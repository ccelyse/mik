@extends('backend.layout.master')

@section('title', 'Accreditx')

@section('content')
    <script src="../../backend/js/sidemenu.js"></script>
<style>
        .card-body {
            flex: 1 1 auto;
            padding: 10px;
        }
        #addModalForm{
            width: 100%;
            padding-bottom: 15px;
            background: #fff;
            margin-bottom: 5px;
            padding: 10px;
        }
        .highcharts-title{
            font-family: Circular !important;
            font-size: 14px !important;
        }
        .highcharts-axis-labels{
            font-family: Circular !important;
            font-size: 12px !important;
        }
        .highcharts-color-0{
            fill: #4e80bd !important;
        }
        .highcharts-color-2{
            fill: #9bbb59 !important;
        }
        #filter_report{
            float: right;
            margin-top: 20px;
        }
    </style>
    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebarmenu">

        </div>

        <div id='content-wrapper' class='d-flex flex-column'>
            <div id='content'>
                <nav class='navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow' id="apptopmenu">

                </nav>
                <div class='container-fluid'>
{{--                    <div class="row">--}}
{{--                        <form action="#" id="addModalForm" enctype="multipart/form-data">--}}
{{--                            <div class="alert alert-success text-center" id="login_error" style="margin-top: 10px;display: none"></div>--}}
{{--                                <div class="row">--}}
{{--                                    <div class='col-lg-6'>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <select class="form-control form-control-user" id="HospitalsId" required>--}}
{{--                                                <option value="">Select ......</option>--}}
{{--                                            </select>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class='col-lg-6'>--}}
{{--                                        <input type='button' class='btn btn-fill btn-success btn-wd' value='Filter' id="filter_report" style="border-radius:0px !important;"/>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            <div class="clearfix"></div>--}}
{{--                        </form>--}}
{{--                            <div id="AllStandards" style="width:100%; height: 400px; margin: 0 auto"></div>--}}
{{--                            <div id="Critical" style="margin-top: 35px !important;width:100%; height: 400px; margin: 0 auto"></div>--}}
{{--                    </div>--}}
                    <div class="row my-3">
                        <div class="col-md-12" style="margin-bottom: 15px">
                            <div class="card shadow">
                                <div class="card-header">
                                    <p>District Hospital Performance by Risk Area</p>
                                </div>
                                <div class="card-body">
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <td class="stripe-table-data">
                                                <p><strong>Hospital</strong></p>
                                            </td>
                                            <td class="stripe-table-data">
                                                <p><strong>Risk Area 1</strong></p>
                                            </td>
                                            <td class="stripe-table-data">
                                                <p><strong>Risk Area 2</strong></p>
                                            </td>
                                            <td class="stripe-table-data">
                                                <p><strong>Risk Area 3</strong></p>
                                            </td>
                                            <td class="stripe-table-data">
                                                <p><strong>Risk Area 4</strong></p>
                                            </td>
                                            <td class="stripe-table-data">
                                                <p><strong>Risk Area 5</strong></p>
                                            </td>
                                            <td class="stripe-table-data">
                                                <p><strong>Average 2017/2018</strong></p>
                                            </td>
                                            <td class="stripe-table-data">
                                                <p><strong>Average 2017/2019</strong></p>
                                            </td>
                                            <td class="stripe-table-data">
                                                <p><strong>Progress</strong></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                <p>Butaro</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                <p>Butaro</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                <p>Butaro</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                <p>Butaro</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                <p>Butaro</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>
                                            <td class="text-center">
                                                <p>80%</p>
                                            </td>

                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('backend.layout.footer')
</div>
</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
<i class="fas fa-angle-up"></i>
</a>

    <script src="../js/jquery.js" type="application/javascript"></script>
    <script src="../../backend/js/custom.js"></script>
    <script src="../../js/initial.min.js"></script>
    <script src="../../actions/dashboard.js"></script>
@endsection
