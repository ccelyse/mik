@extends('backend.layout.master')
@section('title', 'Made in Kigali| Category')
@section('content')

    <script src="../../backend/js/sidemenu.js" defer></script>
    <link href="../../formwizard/css/material-bootstrap-wizard.css" rel="stylesheet" />
    <link href="../../formwizard/css/demo.css" rel="stylesheet" />
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
    <style>
        .moving-tab{
            width: auto !important;
        }
        .roundedTwo {
            width: 28px;
            height: 28px;
            position: relative;
            margin: 20px auto;
            background: #fcfff4;
            background: -webkit-gradient(linear, left top, left bottom, from(#fcfff4), color-stop(40%, #dfe5d7), to(#b3bead));
            background: linear-gradient(to bottom, #fcfff4 0%, #dfe5d7 40%, #b3bead 100%);
            border-radius: 50px;
            box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0, 0, 0, 0.5);
        }
        .roundedTwo label {
            width: 20px;
            height: 20px;
            position: absolute;
            top: 4px;
            left: 4px;
            cursor: pointer;
            background: -webkit-gradient(linear, left top, left bottom, from(#222222), to(#45484d));
            background: linear-gradient(to bottom, #222222 0%, #45484d 100%);
            border-radius: 50px;
            box-shadow: inset 0px 1px 1px rgba(0, 0, 0, 0.5), 0px 1px 0px white;
        }
        .roundedTwo label:after {
            content: '';
            width: 9px;
            height: 5px;
            position: absolute;
            top: 5px;
            left: 4px;
            border: 3px solid #fcfff4;
            border-top: none;
            border-right: none;
            background: transparent;
            opacity: 0;
            -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg);
        }
        .roundedTwo label:hover::after {
            opacity: 0.3;
        }
        .roundedTwo input[type=checkbox] {
            visibility: hidden;
        }
        .roundedTwo input[type=checkbox]:checked + label:after {
            opacity: 1;
        }
        #preview{
            margin-top: 10px;
            width: 100%;
        }
    </style>


    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebarmenu">

        </div>
        <div id='content-wrapper' class='d-flex flex-column'>
            <div id='content'>
                <nav class='navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow' id="apptopmenu">
                </nav>

                <div class='container'>
                        @if (session('success'))
                            <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                {{ session('success') }}
                            </div>
                        @endif
                    <div class="row">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Category</h6>
                            </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <form action="{{url('AddPost')}}" method="post" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class='col-lg-8'>
                                                    <div class='col-lg-12'>
                                                        <div class="form-group">
                                                            <input type="text" class="form-control form-control-user" name="post_title" id="post_title" placeholder="Blog title" required>
                                                        </div>
                                                    </div>
                                                    <div class='col-lg-12'>
                                                        <div class="form-group">
                                                            <textarea id="summernote" name="post_details"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class='col-lg-12'>
                                                        <input type='submit' class='btn btn-fill btn-success btn-wd' value='Publish' id="add_room" style="border-radius:0px !important;">
                                                    </div>
                                                </div>
                                                <div class='col-lg-4'>
                                                    <div class="categories_div">
                                                        <h6 class="m-0 font-weight-bold text-primary">All categories</h6>
                                                        @foreach($listcategory as $cats)
                                                        <div class="form-control" style="background-image: none;">
                                                            <label><input type="checkbox" name="category_id[]" value="{{$cats->id}}" style="margin-right: 10px;">{{$cats->name}}</label>
                                                        </div>
                                                        @endforeach
                                                    </div>
                                                    <div class="categories_div">
                                                        <h6 class="m-0 font-weight-bold text-primary">All tags</h6>
                                                        @foreach($listtags as $cats)
                                                            <div class="form-control" style="background-image: none;">
                                                                <label><input type="checkbox" name="tag_id[]" value="{{$cats->id}}" style="margin-right: 10px;">{{$cats->name}}</label>
                                                            </div>
                                                        @endforeach
                                                    </div>

                                                    <div class="categories_div_image">
                                                        <h6 class="m-0 font-weight-bold text-primary">Set featured image</h6>
                                                        <input type="file" name="post_featured_image" id="filetag">
                                                        <img src="" id="preview">
                                                    </div>


                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </form>

                                    </div>
                            </div>
                        </div>
            </div>
            <!-- end row -->

                </div>
            </div>
        </div>
        <!--  big container -->

    </div>
    @include('backend.layout.footer')

    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
     <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="js/initial.min.js"></script>
    <script type="application/javascript">
        var data = sessionStorage.getItem('accessToken');
        if (!data == data) {
            window.location.href = "/";
        } else {
            $(document).ready(function() {

                $.ajax({
                    type: 'post',
                    url: "../api/auth/me",
                    dataType: 'json',
                    beforeSend : function( xhr ) {
                        xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
                    },
                    success: function (response) {
                        JSON.stringify(response); //to string
                        $('#user_id').val(response.user.id);
                        $(function(){
                            var current = location.pathname;
                            $('#sidebarmenu a').each(function(){
                                var $this = $(this);
                                // if the current path is like this link, make it active
                                if($this.attr('href').indexOf(current) !== -1){
                                    $this.addClass('active');
                                }
                            })
                        });
                        $('.img-profile').initial();
                    }, error: function (xhr, status, error) {
                        if(xhr.status == 401){
                            window.location.href="/";
                        }else{
                            console.log(xhr.responseText);
                        }
                    }
                    //
                });

            });
        }

    </script>
    <script src="../js/summernote.js" type="application/javascript"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="../formwizard/js/jquery.validate.min.js"></script>
    <script src="../backend/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote();
        });
        var fileTag = document.getElementById("filetag"),
            preview = document.getElementById("preview");

        fileTag.addEventListener("change", function() {
            changeImage(this);
        });

        function changeImage(input) {
            var reader;

            if (input.files && input.files[0]) {
                reader = new FileReader();

                reader.onload = function(e) {
                    preview.setAttribute('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>


@endsection
