@extends('backend.layout.master')
@section('title', 'Made in Kigali| Category')
@section('content')

    <script src="../../backend/js/sidemenu.js" defer></script>
    <link href="../../formwizard/css/material-bootstrap-wizard.css" rel="stylesheet" />
    <link href="../../formwizard/css/demo.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/tables/datatable/datatables.min.css">
    <style>
        .moving-tab{
            width: auto !important;
        }
    </style>


    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebarmenu">

        </div>
        <div id='content-wrapper' class='d-flex flex-column'>
            <div id='content'>
                <nav class='navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow' id="apptopmenu">
                </nav>

                <div class='container-fluid'>

                    <div class="row">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Category</h6>
                            </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration">
                                            <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Categories</th>
                                                <th>Tags</th>
                                                <th>Date</th>
                                                <th>Delete</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                           @foreach($all_blog as $data)
                                            <tr>
                                                <td>{{$data->post_title}}</td>
                                                <td>
                                                    <?php
                                                    $show_cats = \App\BlogCategories::where('blog_categories.post_id',$data->id)
                                                        ->join('categories', 'categories.id', '=', 'blog_categories.category_id')
                                                        ->select('blog_categories.*', 'categories.name')
                                                        ->get();
                                                    foreach ($show_cats as $cats){
                                                        echo "$cats->name ,";
                                                    }
                                                    ?>

                                                </td>
                                                <td>
                                                    <?php
                                                    $show_tags = \App\BlogTags::where('blog_tags.post_id',$data->id)
                                                        ->join('tags', 'tags.id', '=', 'blog_tags.tag_id')
                                                        ->select('blog_tags.*', 'tags.name')
                                                        ->get();
                                                    foreach ($show_tags as $tags){
                                                        echo "$tags->name ,";
                                                    }
                                                    ?>
                                                </td>
                                                <td>Published {{$data->created_at}}</td>
                                                <td><a href="{{ route('backend.DeleteCategory',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Delete</a></td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
{{--                            <div class="card-body">--}}
{{--                                <div class="table-responsive">--}}
{{--                                    <table class="table table-striped table-bordered dataex-html5-export table-responsive">--}}
{{--                                        <thead>--}}
{{--                                        <tr>--}}
{{--
{{--                                        </tr>--}}
{{--                                        </thead>--}}
{{--                                        <tbody>--}}
{{--
{{--                                    </table>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </div>
            </div>
            <!-- end row -->

                </div>
            </div>
        </div>
        <!--  big container -->

    </div>
    @include('backend.layout.footer')

    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
     <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script type="application/javascript">
        $('textarea').keypress(function (e) {
            if(e.which == 13)   {
                var control = e.target;
                var controlHeight = $(control).height();
                //add some height to existing height of control, I chose 17 as my line-height was 17 for the control
                $(control).height(controlHeight+17);
            }
        });
        var data = sessionStorage.getItem('accessToken');
        if (!data == data) {
            window.location.href = "/";
        } else {
            $(document).ready(function() {

                $.ajax({
                    type: 'post',
                    url: "../api/auth/me",
                    dataType: 'json',
                    beforeSend : function( xhr ) {
                        xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
                    },
                    success: function (response) {
                        JSON.stringify(response); //to string
                        $('#user_id').val(response.user.id);
                        $(function(){
                            var current = location.pathname;
                            $('#sidebarmenu a').each(function(){
                                var $this = $(this);
                                // if the current path is like this link, make it active
                                if($this.attr('href').indexOf(current) !== -1){
                                    $this.addClass('active');
                                }
                            })
                        });
                        $('.img-profile').initial();
                    }, error: function (xhr, status, error) {
                        if(xhr.status == 401){
                            window.location.href="/";
                        }else{
                            console.log(xhr.responseText);
                        }
                    }
                    //
                });

            });
        }

    </script>

    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>--}}
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>--}}
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/jszip.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/pdfmake.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/vfs_fonts.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.html5.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.print.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.colVis.min.js"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables-extensions/datatable-button/datatable-html5.js"></script>
    <script src="js/initial.min.js"></script>
{{--    <script src="../../actions/Category.js"></script>--}}

@endsection
