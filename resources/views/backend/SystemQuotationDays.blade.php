@extends('backend.layout.master')
@section('title', 'Primate Safaris')
@section('content')

    <script src="../../backend/js/sidemenu.js" defer></script>
    <link href="../../formwizard/css/material-bootstrap-wizard.css" rel="stylesheet" />
    <link href="../../formwizard/css/demo.css" rel="stylesheet" />
    <link href="../../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <style>
        .moving-tab{
            width: auto !important;
        }
    </style>


    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebarmenu">

        </div>
        <div id='content-wrapper' class='d-flex flex-column'>
            <div id='content'>
                <nav class='navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow' id="apptopmenu">

                </nav>

                <div class='container-fluid'>
                    <div class="row" style="margin-bottom: 15px">
                        <div class="alert alert-success text-center" id="delete_error" style="margin-top: 10px;display: none;width: 100%;text-align: center"></div>
                        <div class="col-lg-6">
                            <button type="button" class="btn btn-success edit-modal" data-toggle="modal" data-target="#addCategorization" style="border-radius:0px !important;">
                                <i class="fas fa-plus-circle"></i> Add new day
                            </button>
                        </div>
                        <div class="col-lg-6" id="generate_quotation">

                        </div>
                        <div class="modal fade" id="addCategorization" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document" style="max-width: 60% !important;">
                                <div class="modal-content">
                                    <div class="alert alert-success text-center" id="updating_error" style="text-align:center;margin-top: 10px;display: none;font-size: 13px;"></div>

                                    <div class="modal-header">
                                        {{--
                                        <h5 class="modal-title" id="exampleModalLabel">Edit Hospital / Health Center Categorization</h5>--}}
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                                <form action="#" id="AddQuote_Day" enctype="multipart/form-data">
                                                    <div class="alert alert-success text-center" id="added_error" style="margin-top: 10px;display: none"></div>
                                                    <div class="tab-content">
                                                        <div class="row">

                                                            <div class='col-lg-12' hidden>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control form-control-user" name="user_id" id="user_id" required>
                                                                </div>
                                                            </div>
                                                            <div class='col-lg-12' hidden>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control form-control-user" name="agent_id" id="agent_id" required>
                                                                </div>
                                                            </div>
                                                            <div class='col-lg-12' hidden>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control form-control-user" name="quotation_id" id="quotation_id" required>
                                                                </div>
                                                            </div>

                                                            <div class="multi-field-wrapper col-lg-12">
                                                                <div class="row  multi-field" id="appendform"  style="border: 2px dashed #888787;margin-bottom: 10px;">
                                                                    <div class='col-lg-6'>
                                                                        <div class="form-group">
                                                                            <select class="form-control form-control-user ProductsService" name="product_category_id[]" id="ProductsService" required>
                                                                                <option value="">Select Product Category</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class='col-lg-6'>
                                                                        <div class="form-group">
                                                                            <select class="form-control form-control-user Products_Provider" name="provider_id[]" id="Products_Provider" required>
                                                                                <option value="">Select Provider</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class='col-lg-6'>
                                                                        <div class="form-group">
                                                                            <select class="form-control form-control-user Products_Contract" name="provider_contract_id[]" id="Products_Contract" required>
                                                                                <option value="">Select Contract</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class='col-lg-3' hidden>
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control form-control-user" name="rack_rate[]" id="rack_rate" placeholder="Rack Rate" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class='col-lg-3'>
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control form-control-user" name="unit_cost[]" id="unit_price" placeholder="Unit Cost" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class='col-lg-3'>
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control form-control-user" name="total_price[]" id="sub_total" placeholder="Sub Total" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class='col-lg-12'>
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control form-control-user" name="quantity[]" id="quantity" placeholder="Quantity" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class='col-lg-12'>
                                                                <button type="button" class="btn btn-success btn-circle add-field" id="add" style="margin-top: 10px;"><i class="fas fa-plus-circle"></i> </button>
                                                                <button type='button' class='btn btn-fill btn-success btn-wd' value='Save' id="addquotation_day" style="border-radius:0px !important;float: right">Save</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Quotation</h6>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="provincesTable" width="100%" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th>NO</th>
                                            <th>Day</th>
                                            <th>Agent</th>
                                            <th>Quotation Name</th>
                                            <th>Services</th>
                                            {{--<th>Edit</th>--}}
                                            <th>Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody id="Accreditation_info">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
            </div>
            <!-- end row -->

        </div>
        <!--  big container -->

    </div>
    @include('backend.layout.footer')

    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Page level custom scripts -->
    <script src="../js/demo/datatables-demo.js"></script>
    <script src="../../actions/systemQuotationDays.js"></script>
@endsection
