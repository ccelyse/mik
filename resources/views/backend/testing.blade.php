@extends('backend.layout.master')

@section('title', 'Accreditx')

@section('content')
    <link href="../backend/css/sb-admin-3.min.css" rel="stylesheet">
    <link href="../formwizard1/css/material-bootstrap-wizard.css" rel="stylesheet" />
    <link href="../formwizard1/css/demo.css" rel="stylesheet" />
    <link href="../../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <style>
        .moving-tab{
            width: auto !important;
        }
    </style>

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebarmenu">

        </div>
        <div id='content-wrapper' class='d-flex flex-column'>
            <div id='content'>
                <nav class='navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow' id="apptopmenu">

                </nav>
                <div class='container-fluid'>

                    <div class="row" style="margin-bottom: 15px">
                        @if (session('success'))
                            <div class="alert alert-success" id="success_messages" style="margin-top: 10px;width: 100%;text-align: center;">
                                {{ session('success') }}
                            </div>
                        @endif
                        <div class="col-md-12">
                            <div class="float-right">
                                <button type="button" class="btn btn-success action_btn" data-toggle="modal" data-target="#addCategorization" style="border-radius:0px !important;">
                                    <i class="fas fa-plus-circle"></i> Add new
                                </button>
                            </div>
                        </div>
                        <div class="modal fade" id="addCategorization" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="{{url('UploadExcelHospital')}}" method="post" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class='col-lg-12'>
                                                    <div class="form-group">
                                                        <input type="file" class="form-control form-control-user" name="hospital_excel" required>
                                                    </div>
                                                </div>
                                                <div class='col-lg-12'>
                                                    <input type='submit' class='btn btn-fill btn-success btn-wd' value='Save' style="border-radius:0px !important;" />
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </form>

                                        <!-- wizard container -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class='row'>
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Hospital Information</h6>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table" id="provincesTable" width="100%" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th>NO</th>
                                            <th>Facility Name</th>
                                            <th>Hospital Category</th>
                                            <th>Name of facility Head</th>
                                            <th>Phone of head of facility</th>
                                            <th>Assess</th>
                                            <th>View More</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody id="Hospital_Info">

                                        </tbody>
                                    </table>
                                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document" style="max-width: 900px !important;">
                                            <div class="modal-content">
                                                <div class="alert alert-success text-center" id="updating_error" style="text-align:center;margin-top: 10px;display: none;font-size: 13px;"></div>
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Edit Hospital Information</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="wizard-container">
                                                        <div class="card wizard-card" data-color="green" id="wizardProfile">
                                                            <form class="form-horizontal" action="#" id="editModalForm">
                                                                <div class="alert alert-success text-center" id="login_error" style="margin-top: 10px;display: none"></div>
                                                                <div class="wizard-header">
                                                                    <h3 class="wizard-title">
                                                                        Hospital Information
                                                                    </h3>
                                                                </div>
                                                                <div class="wizard-navigation">
                                                                    <ul>
                                                                        <li><a href="#Admistrative_information_edit" data-toggle="tab">Admistrative information</a></li>
                                                                        <li><a href="#Contact_information_edit" data-toggle="tab">Contact information</a></li>
                                                                        <li><a href="#Availbale_amenities_edit" data-toggle="tab">Available amenities</a></li>
                                                                        <li><a href="#Staff_categories_edit" data-toggle="tab">Staff categories</a></li>
                                                                    </ul>
                                                                </div>

                                                                <div class="tab-content">
                                                                    <div class="tab-pane" id="Admistrative_information_edit">
                                                                        {{--Admistrative information --}}
                                                                        <div class="row">
                                                                            <div class='col-lg-6' hidden>
                                                                                <div class="form-group" >
                                                                                    <input type="text" class="form-control form-control-user" name="id" id="id_edit"  placeholder="Facility Name" required>
                                                                                </div>
                                                                            </div>
                                                                            <div class='col-lg-6'>
                                                                                <div class="form-group">
                                                                                    <select class="form-control form-control-user province_id" name="province_id" id="province_id_edit" required>

                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class='col-lg-6'>
                                                                                <div class="form-group">
                                                                                    <select class="form-control form-control-user district_id"  name="district_id" id="district_id_edit" required>

                                                                                    </select>
                                                                                </div>
                                                                            </div>

                                                                            <div class='col-lg-6'>
                                                                                <div class="form-group">
                                                                                    <input type="text" class="form-control form-control-user" name="facility_name" id="facility_name_edit"  placeholder="Facility Name" required>
                                                                                </div>
                                                                            </div>
                                                                            <div class='col-lg-6'>
                                                                                <div class="form-group">
                                                                                    <select class="form-control form-control-user hospital_cat_id" name="hospital_cat_id" id="hospital_cat_id_edit" required>

                                                                                    </select>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="tab-pane" id="Contact_information_edit">
                                                                        {{--Contact information --}}
                                                                        <div class="row">
                                                                            <div class='col-lg-6'>
                                                                                <div class="form-group">
                                                                                    <input type="number" class="form-control form-control-user" name="facility_phone" id="facility_phone_edit"  placeholder="Facility Phone" required>
                                                                                </div>
                                                                            </div>

                                                                            <div class='col-lg-6'>
                                                                                <div class="form-group">
                                                                                    <input type="email" class="form-control form-control-user" name="facility_email" id="facility_email_edit"  placeholder="Facility Email" required>
                                                                                </div>
                                                                            </div>
                                                                            <div class='col-lg-6'>
                                                                                <div class="form-group">
                                                                                    <input type="text" class="form-control form-control-user" name="name_of_facility_head" id="name_of_facility_head_edit"  placeholder="Name of facility Head" required>
                                                                                </div>
                                                                            </div>
                                                                            <div class='col-lg-6'>
                                                                                <div class="form-group">
                                                                                    <input type="number" class="form-control form-control-user" name="phone_of_head_facility" id="phone_of_head_facility_edit"  placeholder="Phone of head of facility" required>
                                                                                </div>
                                                                            </div>
                                                                            <div class='col-lg-6'>
                                                                                <div class="form-group">
                                                                                    <input type="text" class="form-control form-control-user" name="name_alternative_facility_head" id="name_alternative_facility_head_edit"  placeholder="Name alternate facility Head" required>
                                                                                </div>
                                                                            </div>
                                                                            <div class='col-lg-6'>
                                                                                <div class="form-group">
                                                                                    <input type="number" class="form-control form-control-user" name="phone_alternative_facility_head" id="phone_alternative_facility_head_edit"  placeholder="Phone of alternate head of facility" required>
                                                                                </div>
                                                                            </div>
                                                                            <div class='col-lg-6'>
                                                                                <div class="form-group" id="population_cv">
                                                                                    <input type="text" class="form-control form-control-user district_population" name="population_covered" id="population_covered_edit" required>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="tab-pane" id="Availbale_amenities_edit">
                                                                        <div class="row">
                                                                            <div class='col-lg-6'>
                                                                                <div class="form-group">
                                                                                    <input type="number" class="form-control form-control-user" name="total_beds" id="total_beds_edit"  placeholder="Total Beds" required>
                                                                                </div>
                                                                            </div>
                                                                            <div class='col-lg-6'>
                                                                                <div class="form-group">
                                                                                    <input type="number" class="form-control form-control-user" name="maternity_beds" id="maternity_beds_edit"  placeholder="Maternity Beds" required>
                                                                                </div>
                                                                            </div>
                                                                            <div class='col-lg-6'>
                                                                                <div class="form-group">
                                                                                    <input type="number" class="form-control form-control-user" name="operational_ambulances" id="operational_ambulances_edit"  placeholder="Operational Ambulances" required>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="tab-pane" id="Staff_categories_edit">
                                                                        <div class="row">
                                                                            <div class='col-lg-6'>
                                                                                <div class="form-group">
                                                                                    <input type="number" class="form-control form-control-user" name="general_practitioner" id="general_practitioner_edit"  placeholder="General Practitioner" required>
                                                                                </div>
                                                                            </div>
                                                                            <div class='col-lg-6'>
                                                                                <div class="form-group">
                                                                                    <input type="number" class="form-control form-control-user" name="specialists" id="specialists_edit"  placeholder="Specialists" required>
                                                                                </div>
                                                                            </div>
                                                                            <div class='col-lg-6'>
                                                                                <div class="form-group">
                                                                                    <input type="number" class="form-control form-control-user" name="a1" id="a1_edit"  placeholder="A1" required>
                                                                                </div>
                                                                            </div>
                                                                            <div class='col-lg-6'>
                                                                                <div class="form-group">
                                                                                    <input type="number" class="form-control form-control-user" name="a2" id="a2_edit"  placeholder="A2" required>
                                                                                </div>
                                                                            </div>
                                                                            <div class='col-lg-6'>
                                                                                <div class="form-group">
                                                                                    <input type="number" class="form-control form-control-user" name="mid_wives" id="mid_wives_edit"  placeholder="Mid Wives" required>
                                                                                </div>
                                                                            </div>
                                                                            <div class='col-lg-6'>
                                                                                <div class="form-group">
                                                                                    <input type="number" class="form-control form-control-user" name="lab_techs" id="lab_techs_edit"  placeholder="Lab techs" required>
                                                                                </div>
                                                                            </div>
                                                                            <div class='col-lg-6'>
                                                                                <div class="form-group">
                                                                                    <input type="number" class="form-control form-control-user" name="physio" id="physio_edit"  placeholder="Physio" required>
                                                                                </div>
                                                                            </div>
                                                                            <div class='col-lg-6'>
                                                                                <div class="form-group">
                                                                                    <input type="number" class="form-control form-control-user" name="anesthetists" id="anesthetists_edit"  placeholder="Anesthetists" required>
                                                                                </div>
                                                                            </div>
                                                                            <div class='col-lg-6'>
                                                                                <div class="form-group">
                                                                                    <input type="number" class="form-control form-control-user" name="pharmacists" id="pharmacists_edit"  placeholder="Pharmacists" required>
                                                                                </div>
                                                                            </div>
                                                                            <div class='col-lg-6'>
                                                                                <div class="form-group">
                                                                                    <input type="number" class="form-control form-control-user" name="dentists" id="dentists_edit"  placeholder="Dentists" required>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="wizard-footer">
                                                                    <div class="pull-right">
                                                                        <input type='button' style="float: right;"  class='btn btn-next btn-fill btn-success btn-wd' name='next' value='Next' />
                                                                        {{--<input type='button' style="float: right;"  class='btn btn-finish btn-fill btn-success btn-wd' name='finish' value='Finish' id="save_Health_info" />--}}
                                                                        <button type='button'  style="float: right;"  class='btn btn-finish btn-fill btn-success btn-wd' name='finish' value='Finish' id="update_category">Update changes</button>
                                                                    </div>

                                                                    <div class="pull-left">
                                                                        <input type='button' class='btn btn-previous btn-fill btn-success btn-wd' name='previous' value='Previous' />
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div> <!-- wizard container -->
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- end row -->

                </div> <!--  big container -->

            </div>

            @include('backend.layout.footer')
        </div>
    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <script src="../js/jquery.js" type="application/javascript"></script>

    <script src="../formwizard1/js/jquery.bootstrap.js" type="text/javascript"></script>
    <script src="../formwizard1/js/material-bootstrap-wizard.js"></script>
    <!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
    <script src="../formwizard1/js/jquery.validate.min.js"></script>

    {{--<script src="../backend/vendor/jquery/jquery.min.js"></script>--}}
    <script src="../../backend/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script src="../../backend/js/sb-admin-2.min.js"></script>
    <script src="../../vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="../../vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Page level custom scripts -->
    <script src="../../js/demo/datatables-demo.js"></script>
    <script src="../../actions/hospital.js"></script>
