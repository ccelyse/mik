@extends('backend.layout.master')

@section('title', 'Primate Safaris')

@section('content')
    <script src="../../backend/js/sidemenu.js"></script>
<style>
    .moving-tab{
        width: auto !important;
    }
</style>
<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <div id="sidebarmenu">

    </div>
    <div id='content-wrapper' class='d-flex flex-column'>
        <div id='content'>
            <nav class='navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow' id="apptopmenu">

            </nav>
            <div class='container-fluid'>
                <div class="row" style="margin-bottom: 15px">
                    <div class="alert alert-success text-center" id="updating_error" style="text-align:center;margin-top: 10px;display: none;font-size: 13px;width: 100%;"></div>
                    <div class="col-md-12">
                        <button type="button" class="btn btn-success action_btn" data-toggle="modal" data-target="#addCategorization" style="border-radius:0px !important;">
                            <i class="fas fa-plus-circle"></i> Add new
                        </button>
                    </div>
                    <div class="modal fade" id="addCategorization" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="alert alert-success text-center" id="updating_error" style="text-align:center;margin-top: 10px;display: none;font-size: 13px;"></div>

                                <div class="modal-header">
                                    {{--
                                    <h5 class="modal-title" id="exampleModalLabel">Edit Hospital / Health Center Categorization</h5>--}}
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="#" id="addModalForm" enctype="multipart/form-data">
                                        <div class="alert alert-success text-center" id="login_error" style="margin-top: 10px;display: none"></div>
                                        <div class="wizard-header">
                                            <h3 class="wizard-title">
                                                System User
                                            </h3>
                                        </div>
                                        <div class="tab-content">
                                            <div class="row">
                                                <div class='col-lg-6'>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control form-control-user" name="name" id="name" placeholder="Name" required>
                                                    </div>
                                                </div>
                                                <div class='col-lg-6'>
                                                    <div class="form-group">
                                                        <input type="email" class="form-control form-control-user" name="email" id="email" placeholder="Email" required>
                                                    </div>
                                                </div>
                                                <div class='col-lg-6'>
                                                    <div class="form-group">
                                                        <input type="password" class="form-control form-control-user" name="password" id="password" placeholder="Password" required>
                                                    </div>
                                                </div>
                                                <div class='col-lg-6'>
                                                    <div class="form-group">
                                                        <input type="password" class="form-control form-control-user" name="confirm_password" id="confirm_password" placeholder="Confirm Password" required>
                                                    </div>
                                                    <span class="passworderror" style="color:red"></span>
                                                    <br />
                                                </div>
{{--                                                <div class='col-lg-6'>--}}
{{--                                                    <div class="form-group">--}}
{{--                                                        <select class="form-control form-control-user" id="role_users" required>--}}
{{--                                                            <option value="User Role">User Role</option>--}}
{{--                                                        </select>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class='col-lg-6'>--}}
{{--                                                    <div class="form-group">--}}
{{--                                                        <select class="form-control form-control-user allhospital" name="allhospital" id="allhospital" required>--}}
{{--                                                            <option value="">Select Hospital/Health Center</option>--}}
{{--                                                        </select>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
                                                <div class='col-lg-6'>
                                                    <input type='button' class='btn btn-fill btn-success btn-wd' value='Save' id="create_account" style="border-radius:0px !important;"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">System User</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="provincesTable" width="100%" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>NO</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Created At</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody id="SystemInfo">

                                    </tbody>
                                </table>
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="alert alert-success text-center" id="updating_error" style="text-align:center;margin-top: 10px;display: none;font-size: 13px;"></div>

                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Edit Perform Score</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form class="form-horizontal" action="#" id="editModalFormAccount">
                                                    {{--<div class="alert alert-success text-center" id="login_error" style="margin-top: 10px;display: none"></div>--}}
                                                    <div class="tab-content">
                                                        <div class="row">
                                                            <div class='col-lg-6'>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control form-control-user" name="id" id="id_edit">
                                                                </div>
                                                            </div>
                                                            <div class='col-lg-6'>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control form-control-user" name="name" id="name_edit">
                                                                </div>
                                                            </div>
                                                            <div class='col-lg-6'>
                                                                <div class="form-group">
                                                                    <input type="email" class="form-control form-control-user" name="email" id="email_edit">
                                                                </div>
                                                            </div>

                                                            <div class='col-lg-6'>
                                                                <div class="form-group">
                                                                    <select class="form-control form-control-user User_role" name ="role_user" id="role_users" required>
                                                                        {{--<option value="User Role">User Role</option>--}}
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class='col-lg-6'>
                                                                <div class="form-group">
                                                                    <select class="form-control form-control-user allhospital" name="allhospital" id="hospital_id" required>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class='col-lg-6'>
                                                                <div class="form-group" hidden>
                                                                </div>
                                                            </div>
                                                            <div class='col-lg-6'>
                                                                <input type='button' class='btn btn-fill btn-success btn-wd' value='Save' id="update_account" style="border-radius:0px !important;"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        <!-- end row -->

    </div>
    <!--  big container -->

</div>

@include('backend.layout.footer')
</div>
</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>


<script src="../js/jquery.js" type="application/javascript"></script>
<script src="../../vendor/datatables/jquery.dataTables.min.js"></script>
<script src="../../vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="../../js/demo/datatables-demo.js"></script>
<!-- Page level custom scripts -->
<script src="../../js/initial.min.js"></script>
<script src="../../actions/users.js"></script>
<script src="../../js/demo/datatables-demo.js"></script>

@endsection
