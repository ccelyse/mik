@extends('backend.layout.master')
@section('title', 'Primate Safaris')
@section('content')

    <script src="../../backend/js/sidemenu.js" defer></script>
    <link href="../../formwizard/css/material-bootstrap-wizard.css" rel="stylesheet" />
    <link href="../../formwizard/css/demo.css" rel="stylesheet" />
    <link href="../../vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <style>
        .moving-tab{
            width: auto !important;
        }
    </style>


    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebarmenu">

        </div>
        <div id='content-wrapper' class='d-flex flex-column'>
            <div id='content'>
                <nav class='navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow' id="apptopmenu">

                </nav>

                <div class='container-fluid'>
                    <div class="row" style="margin-bottom: 15px">
                        <div>
                            <button type="button" class="btn btn-success action_btn edit-modal" data-toggle="modal" data-target="#addCategorization" style="border-radius:0px !important;">
                                <i class="fas fa-plus-circle"></i> Add new
                            </button>
                        </div>
                        <div class="modal fade" id="addCategorization" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="alert alert-success text-center" id="updating_error" style="text-align:center;margin-top: 10px;display: none;font-size: 13px;"></div>

                                    <div class="modal-header">
                                        {{--
                                        <h5 class="modal-title" id="exampleModalLabel">Edit Hospital / Health Center Categorization</h5>--}}
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="alert alert-success text-center" id="added_error" style="text-align:center;margin-top: 10px;display: none;font-size: 13px;"></div>
                                                <form action="#" id="AddProducts" enctype="multipart/form-data">
                                                    <div class="alert alert-success text-center" id="login_error" style="margin-top: 10px;display: none"></div>
                                                    <div class="tab-content">
                                                        <div class="row">
                                                            <div class='col-lg-6'>
                                                                <div class="form-group">
                                                                    <select class="form-control form-control-user ProductsService" name="product_category_id" id="ProductsService" required>
                                                                        <option value="">Select Product Category</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class='col-lg-6'>
                                                                <div class="form-group">
                                                                    <select class="form-control form-control-user Products_Provider" name="provider_id" id="Products_Provider" required>
                                                                        <option value="">Select Provider</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class='col-lg-6'>
                                                                <div class="form-group">
                                                                    <select class="form-control form-control-user Products_Contract" name="provider_contract_id" id="Products_Contract" required>
                                                                        <option value="">Select Contract</option>
                                                                    </select>
                                                                </div>
                                                            </div>


                                                            <div class='col-lg-6'>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control form-control-user" name="product_price" id="product_price" placeholder="Rack Rates" required>
                                                                </div>
                                                            </div>
                                                            <div class="multi-field-wrapper col-lg-12">
                                                                <div class="row  multi-field" id="appendform"  style="border: 2px dashed #888787;margin-bottom: 10px;">
                                                                    <div class='col-lg-6'>
                                                                        <div class="form-group">
                                                                            <select class="form-control form-control-user" name="products_agent_id[]" id="AgentShowProducts" required>
                                                                                <option value="">Select Agent</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class='col-lg-6'>
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control form-control-user" name="products_sto_price[]" id="price" placeholder="STO RATES" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class='col-lg-12'>
                                                                <button type="button" class="btn btn-success btn-circle add-field" id="add" style="margin-top: 10px;"><i class="fas fa-plus-circle"></i> </button>
                                                                {{--<button type='button' class='btn btn-fill btn-success btn-wd' value='Save' id="AddRiskLevel" style="border-radius:0px !important;float: right;">Save</button>--}}
                                                                <input type='button' class='btn btn-fill btn-success btn-wd' value='Save' id="add_product" style="border-radius:0px !important;float: right;"/>
                                                            </div>


                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Products</h6>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="provincesTable" width="100%" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th>NO</th>
                                            <th>Provider Name</th>
                                            <th>Product Service</th>
                                            <th>Provider Contract Name</th>
                                            <th>Rack Rates</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody id="Accreditation_info">

                                        </tbody>
                                    </table>

                                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="alert alert-success text-center" id="updating_error_edit" style="text-align:center;margin-top: 10px;display: none;font-size: 13px;"></div>

                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Edit Client</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="#" id="EditProducts" enctype="multipart/form-data">
                                                        <div class="alert alert-success text-center" id="login_error" style="margin-top: 10px;display: none"></div>
                                                        <div class="tab-content">
                                                            <div class="row">
                                                                <div class='col-lg-6'>
                                                                    <div class="form-group">
                                                                        <select class="form-control form-control-user ProductsService" name="product_category_id" id="product_category_id_edit" required>
                                                                            <option value="">Select Product Category</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class='col-lg-6'>
                                                                    <div class="form-group">
                                                                        <select class="form-control form-control-user Products_Provider" name="provider_id" id="provider_id_edit" required>
                                                                            <option value="">Select Provider</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class='col-lg-12'>
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control form-control-user" name="product_price" id="product_price_edit" placeholder="Rack Rates" required>
                                                                    </div>
                                                                </div>
                                                                <div class='col-lg-6' hidden>
                                                                    <div class="form-group" >
                                                                        <input type="text" class="form-control form-control-user" name="id_edit" id="id_edit" required>
                                                                    </div>
                                                                </div>
                                                                <div class='col-lg-6'>
                                                                    <input type='button' class='btn btn-fill btn-success btn-wd' value='Update' id="update_products" style="border-radius:0px !important;"/>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
            </div>
            <!-- end row -->

        </div>
        <!--  big container -->

    </div>
    @include('backend.layout.footer')

    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <script src="../vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Page level custom scripts -->
    <script src="../js/demo/datatables-demo.js"></script>
    <script src="../../js/initial.min.js"></script>
    <script src="../../actions/systemProduct.js"></script>
@endsection
