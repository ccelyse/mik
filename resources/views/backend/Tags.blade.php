@extends('backend.layout.master')
@section('title', 'Made in Kigali| Category')
@section('content')

    <script src="../../backend/js/sidemenu.js" defer></script>
    <link href="../../formwizard/css/material-bootstrap-wizard.css" rel="stylesheet" />
    <link href="../../formwizard/css/demo.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/tables/datatable/datatables.min.css">
    <style>
        .moving-tab{
            width: auto !important;
        }
    </style>


    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebarmenu">

        </div>
        <div id='content-wrapper' class='d-flex flex-column'>
            <div id='content'>
                <nav class='navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow' id="apptopmenu">
                </nav>

                <div class='container-fluid'>

                    <div class="row" style="margin-bottom: 15px">
                        @if (session('success'))
                            <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                {{ session('success') }}
                            </div>
                        @endif
                        <div>
                            <button type="button" class="btn btn-success action_btn edit-modal" data-toggle="modal" data-target="#addCategory" style="border-radius:0px !important;">
                                <i class="fas fa-plus-circle"></i> Add new tag
                            </button>
                        </div>
                        <div class="modal fade" id="addCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document" style="max-width: 60% !important;">
                                <div class="modal-content">
                                    <div class="alert alert-success text-center" id="updating_error" style="text-align:center;margin-top: 10px;display: none;font-size: 13px;"></div>

                                    <div class="modal-header">
                                        {{--
                                        <h5 class="modal-title" id="exampleModalLabel">Edit Hospital / Health Center Categorization</h5>--}}
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="{{url('AddTags')}}" method="post" id="addCategory" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="tab-content">
                                                <div class="row">
                                                    <div class='col-lg-12'>
                                                        <div class="form-group">
                                                            <input type="text" class="form-control form-control-user" name="name" id="name" placeholder="Name" required>
                                                        </div>
                                                    </div>
                                                    <div class='col-lg-12'>
                                                        <div class="form-group">
                                                            <textarea class="form-control form-control-user" name="description" id="description" placeholder="Description" required></textarea>
                                                        </div>
                                                    </div>

                                                    <div class='col-lg-12'>
                                                        <input type='submit' class='btn btn-fill btn-success btn-wd' value='Add new tag' style="border-radius:0px !important;">
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Category</h6>
                            </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Description</th>
                                                <th>Count</th>
                                                <th>Edit</th>
                                                <th>Delete</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                           @foreach($show as $data)
                                            <tr>
                                                <td>{{$data->name}}</td>
                                                <td>{{$data->description}}</td>
                                                <td></td>
                                                <td>
                                                    <div>
                                                        <button type="button" class="btn btn-icon btn-outline-primary" data-toggle="modal" data-target="#addCategory{{$data->id}}">
                                                            Edit
                                                        </button>
                                                    </div>
                                                    <div class="modal fade" id="addCategory{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog" role="document" style="max-width: 60% !important;">
                                                            <div class="modal-content">
                                                                <div class="alert alert-success text-center" id="updating_error" style="text-align:center;margin-top: 10px;display: none;font-size: 13px;"></div>

                                                                <div class="modal-header">

                                                                    <h5 class="modal-title" id="exampleModalLabel">Edit tag</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form action="{{url('UpdateTags')}}" method="post" id="addCategory" enctype="multipart/form-data">
                                                                        {{ csrf_field() }}
                                                                        <div class="tab-content">
                                                                            <div class="row">
                                                                                <div class='col-lg-12'>
                                                                                    <div class="form-group">
                                                                                        <input type="text" class="form-control form-control-user" name="name" id="name" value="{{$data->name}}">
                                                                                        <input type="text" class="form-control form-control-user" name="id" id="id" value="{{$data->id}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class='col-lg-12'>
                                                                                    <div class="form-group">
                                                                                        <textarea class="form-control form-control-user" rows="10" name="description" id="description">{{$data->description}}</textarea>
                                                                                    </div>
                                                                                </div>

                                                                                <div class='col-lg-12'>
                                                                                    <input type='submit' class='btn btn-fill btn-success btn-wd' value='Update tag' style="border-radius:0px !important;">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </td>
                                                <td><a href="{{ route('backend.DeleteTags',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Delete</a></td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
            </div>
            <!-- end row -->

                </div>
            </div>
        </div>
        <!--  big container -->

    </div>
    @include('backend.layout.footer')

    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
     <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script type="application/javascript">
        $('textarea').keypress(function (e) {
            if(e.which == 13)   {
                var control = e.target;
                var controlHeight = $(control).height();
                //add some height to existing height of control, I chose 17 as my line-height was 17 for the control
                $(control).height(controlHeight+17);
            }
        });
    </script>
    <script type="application/javascript">
        var data = sessionStorage.getItem('accessToken');
        if (!data == data) {
            window.location.href = "/";
        } else {
            $(document).ready(function() {

                $.ajax({
                    type: 'post',
                    url: "../api/auth/me",
                    dataType: 'json',
                    beforeSend : function( xhr ) {
                        xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
                    },
                    success: function (response) {
                        JSON.stringify(response); //to string
                        $('#user_id').val(response.user.id);
                        $(function(){
                            var current = location.pathname;
                            $('#sidebarmenu a').each(function(){
                                var $this = $(this);
                                // if the current path is like this link, make it active
                                if($this.attr('href').indexOf(current) !== -1){
                                    $this.addClass('active');
                                }
                            })
                        });
                        $('.img-profile').initial();
                    }, error: function (xhr, status, error) {
                        if(xhr.status == 401){
                            window.location.href="/";
                        }else{
                            console.log(xhr.responseText);
                        }
                    }
                    //
                });

            });
        }

    </script>

    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js"></script>
    <script src="js/initial.min.js"></script>

@endsection
