@extends('frontend.blog.layouts.master')

@section('title', 'Made in kigali')

@section('content')
    <link href="frontend/assets/css/site.css" rel="stylesheet">
    <link href="frontend/assets/css/fonts.css" rel="stylesheet">
    <style>
        h1, h2, h3, h4, h5, h6 {
            font-family: "Futura LT W01 Medium", Arial, Helvetica;
        }
        h2, .h2 {
            font-size: 4.0rem;
            font-weight: normal;
            text-transform: uppercase;
        }
        .inner {
            margin: 0 auto;
            /*max-width: 1230px;*/
            max-width: 100%;
            padding: 0px !important;
            position: relative;
            width: 100%;
        }
        .top_bar_reg{
            color: #936923;
        }
        .notice-bar a {
            display: block;
            text-decoration: none;
            color: #fff;
            padding: 11px;
        }
        .site-header {
            background: #fff;
            padding: 0 15px;
        }
        .site-container {
            background-color: #fff !important;
            color: #232323;
        }
        .site-footer__navigation {
            padding: 77px 15px 50px !important;
        }
        .button--full {
            display: block;
            margin-top: 0px !important;
            width: 100%;
        }
        .site-footer__social {
            text-align: left;
            margin: 0 auto;
            display: inline;
        }
        .site-footer__navigation {
            padding: 15px !important;
        }
        @media only screen and (max-width: 48rem){
            .site-nav-button {
                position: relative;
                top: 25px;
            }
        }
        /* .landing-banner.section.static{position:relative;}  Need to remove*/


        /* .home-landing .section.slide{position:relative;} Need to remove*/

        .landing-banner {
            max-width: 1600px;
        }

        .landing-banner img {
            bottom: -100%;
            height: 107%;
            left: -100%;
            margin: auto;
            max-width: none;
            padding: 0 !important;
            position: absolute;
            right: -100%;
            top: -100%;
            width: auto;
        }

        .landing-banner-text-wrapper {
            position: absolute;
            width: 100%;
            text-align: center;
            z-index: 8;
            top: 15%;
        }

        .landing-banner-text-wrapper h2 {
            padding: 0 0 5px;
            font-size: 5.21rem;
            text-transform: uppercase;
            color: #fff;
            background-color: #000;
            display: inline;
            line-height: 1.58em;
            box-shadow: 15px 0 0 #000, -15px 0 0 #000;
            -webkit-box-decoration-break: clone;
            -o-box-decoration-break: clone;
            box-decoration-break: clone;
        }

        .landing-banner-text-wrapper h2:after {
            border: none;
        }

        .landing-banner-text-wrapper h2.trans {
            background: rgba(255, 255, 255, 0.7);
            color: #000;
            box-shadow: 15px 0 0 rgba(255, 255, 255, 0.7), -15px 0 0 rgba(255, 255, 255, 0.7);
            -webkit-box-decoration-break: clone;
            -o-box-decoration-break: clone;
            box-decoration-break: clone;
        }

        .landing-banner-text-wrapper .landing-bnr-txt {
            width: 70%;
            margin: 0 auto;
        }

        .landing-banner-text-wrapper .landing-bnr-subtxt {
            width: 80%;
            margin: 0 auto;
            color: #fff;
            font-size: 3.11rem;
            line-height: 1.3em;
            padding-bottom: 43px;
        }

        .landing-banner-text-wrapper .landing-bnr-subtxt p {
            color: #fff;
            font-size: 3.15rem;
            padding: 0;
        }

        .landing-bnr-subtxt.trans p {
            background: rgba(255, 255, 255, 0.8);
            color: #000;
            box-shadow: 15px 0 0 rgba(255, 255, 255, 0.8), -15px 0 0 rgba(255, 255, 255, 0.8);
            -webkit-box-decoration-break: clone;
            -o-box-decoration-break: clone;
            box-decoration-break: clone;
            padding: 3px 0 6px;
            display: inline;
            position: relative;
            line-height: /*1.745em*/
                1.780em;
            font-size: 3.15rem;
        }

        .section #inv_playerContainer {
            z-index: 0
        }

        .landing-banner-text-wrapper .playicon {
            text-align: center;
            display: block;
            margin: 0 auto;
            padding-bottom: 20px;
        }

        .landing-banner-text-wrapper .playicon span {
            cursor: pointer;
            margin: 0 auto;
            text-align: center;
            display: inline-block;
            font-size: 5.2rem;
        }

        .ie9 .landing-banner-text-wrapper .playicon span,
        .internetexplorer11 .landing-banner-text-wrapper .playicon span,
        .ie10 .landing-banner-text-wrapper .playicon span {
            display: block;
            overflow: hidden;
        }

        .landing-banner-text-wrapper .playicon a {
            border: none;
            margin-bottom: 25px;
            display: inline-block;
        }

        .landing-banner-text-wrapper .playicon a.rad-video {
            float: none;
        }

        .landing-banner-text-wrapper .playicon a.rad-video::after {
            display: none;
        }


        /*----- Landing Banner End ----------*/
        #contentwrapper h2, #contentwrapper h3, #contentwrapper h4, #contentwrapper h5 {
            clear: both;
        }
        .section h2 {
            margin: 0;
            font-size: 5.2rem;
            text-transform: uppercase;
            padding-bottom: 15px;
        }
        .main-content h2, .main-content h1 {
            margin-bottom: 25px;
            padding: 0 0 23px 0;
            position: relative;
        }
        .main-content h1.pageTitle ~ .intro-text h3:first-child:after { display:none; }
        .main-content h1.pageTitle ~ h2 ~ h3::after { display: none; }
        .main-content h1.pageTitle ~ h2 ~ * ~ h3::after { display: block; }


        html { font-size:62.5%; }

        .heading, h1, h2, h3, h4, h5, .news-item p.Title { padding: 0 0 10px; line-height: 1.2em; }
        body {font-family: "Futura LT W01 Medium", Arial, Helvetica;font-size: 1.8rem;line-height: 1.444em;color: #000;background-color:#f9f9f9;}

        body.no-scroll { /*overflow:hidden;*/ height:100%; width:100%; }
        /*body{-webkit-overflow-scrolling: touch;}*/

        a { color: #000; text-decoration: none; border-bottom: 2px solid #6aac9f; -webkit-transition:all 300ms cubic-bezier(.455,.03,.515,.955); -moz-transition:all 300ms cubic-bezier(.455,.03,.515,.955); -ms-transition:all 300ms cubic-bezier(.455,.03,.515,.955); -o-transition:all 300ms cubic-bezier(.455,.03,.515,.955);  }
        a:hover { text-decoration: none; color: #36acdc; border-bottom: 2px solid #36acdc;  }
        a:focus, a:active { outline: 0; _noFocusLine: expression(this.hideFocus=true); }
        .invisible { position: absolute !important; left: -9999999px; top: auto; width: 1px; height: 1px; overflow: hidden; visibility: visible; }
        img { outline-style: none; outline-width: 0; border: 0 none; max-width: 100%; height: auto; }
        sup { font-size: smaller; font-weight: 700; }
        sub { margin: 2px 0; padding: 2px 0; }
        ul { list-style-image: none; /*list-style-position: inside;*/ list-style-type: none; }
        #contentwrapper ul:not(.contextMenu){ display:table; }

        #contentwrapper table ul{display:inline-block !important;}
        ol { list-style-image: none; list-style-position: outside; list-style-type: decimal; }
        hr { border-style: none none solid; border-width: 0 0 1px; margin: 5px 0; }
        :active { outline-style: none; outline-width: 0; }
        p { padding: 0 0 25px; }
        p.rad-quote, span.rad-quote { font-size:3.2rem; line-height:1.313em; color:#666; position: relative; }
        p.rad-quote::before { content:open-quote; font-size:2.3em; height: 26px; margin: 0 0.22em 0 0; position: relative; top: 0.16em; }
        p.rad-quote::after { content:close-quote; font-size:2.3em; position: relative; line-height: 0; margin-left: 3px; top:0.65em; }

        p#sectionheader { font-size: 1.4em; line-height: 1.2em; }
        .article-title { font-size:3.0rem; }
        .box-links a { font-size:1.6rem; }

        h1, .h1 { font-size: 5.2rem; font-weight:normal; text-transform:uppercase; }
        h2, .h2 {font-size: 4.0rem;font-weight:normal;text-transform:uppercase;}
        h3, .h3 { font-size: 3.5rem; font-weight:normal; text-transform:uppercase; }
        h4, .h4 { font-size: 3.0rem; font-weight:normal; text-transform:uppercase; }
        h5, .h5 { font-size: 2.8rem; font-weight:normal; text-transform:uppercase; }
        h6, .h6 { font-size: 2.6rem; font-weight:normal; text-transform:uppercase; }
        h2 sub, h3 sub, h4 sub,.rad-big sub, #mmenu sub, #sitemapcontainer sub {bottom: 0;font-size: 100%;text-transform: lowercase;}
        hr { display: block; margin-top: 0.5em; margin-bottom: 0.5em; margin-left: auto; margin-right: auto; border-style: inset; border-width: 1px; }
        .uppercase{text-transform:uppercase;}


        /*----    Landing Banner    ----*/

        .landing-banner { position: relative; }
        .landing-banner img { margin:0 auto; }
        .banner-text { position: absolute; top: 64px; width: 100%; text-align: center; }
        .section .banner-text h2 { padding:0; font-size:5.21rem; text-transform:uppercase; color:#fff; background-color:#000; display: inline; line-height:1.48em; box-shadow:15px 0 0 #000, -15px 0 0 #000; -webkit-box-decoration-break:clone; -o-box-decoration-break:clone; box-decoration-break:clone; }
        .main-content .banner-text p { margin-top:14px; font-size: 3.1rem; padding: 0; line-height: 1.238em; text-shadow: 2px 2px 2px rgba(255,255,255,1);  -moz-text-shadow: 2px 2px 2px rgba(255,255,255,1); -webkit-text-shadow: 2px 2px 2px rgba(255,255,255,1);  }

        /*--  Content banner --*/

        .main-content #bannerwrapper img { width:100%; }
        /*--  Green box link style  --*/

        .green-box-link { clear: both; text-align: center; }
        .green-box-link p { display:inline-block; padding:0; font-size:1.6rem; margin:0 5px; text-transform:uppercase; }
        .green-box-link p a { padding:12px 9px 12px 15px; display:inline-block; border:2px solid #6aac9f; background:#FFF; transition:all 0.3s ease 0s; -moz-transition:all 0.3s ease 0s; -webkit-transition:all 0.3s ease 0s; -o-transition:all 0.3s ease 0s; }
        .green-box-link p a span.icon-chevron-right { position: relative; top: 1px; font-weight: normal; font-size: 1.4rem; padding-left: 5px; color:#000; }
        .green-box-link p a:hover { background-color:#000; border:2px solid #000; color:#000; }
        .green-box-link p a:hover span { color:#fff; }
        .home-section.section2 .green-box-link p a:hover { color:transparent; }

        /*--  Green box link animation  --*/

        .green-box-link p a strong { overflow: hidden; font-weight:normal; float: left; }
        .green-box-link p a strong span { position: relative; display: inline-block; -webkit-transition: -webkit-transform 0.3s;	-moz-transition: -moz-transform 0.3s; transition: transform 0.3s;color:#000; }
        .green-box-link p a strong span::before { position: absolute; top: 100%; content: attr(data-hover); color: #fff; -webkit-transform: translate3d(0,0,0); -moz-transform: translate3d(0,0,0);	transform: translate3d(0,0,0); }
        .green-box-link p a:hover strong span{ -webkit-transform: translateY(-100%); -moz-transform: translateY(-100%); transform: translateY(-100%); -ms-transform: translateY(-100%); -o-transform: translateY(-100%); }

        .home-section.section4 .green-box-link p a strong span { line-height:25px; }
    </style>
    <div class="group site-container">
        @include('frontend.blog.layouts.upmenu')
        <main class="group site-content">
            <div class="inner">
                <div class="static section0 landing-banner video-banner section active" style="margin-top: 35px;">

                    <h1 class="invisible">ASOS – We focus on fashion as a force for good, inspiring young people to express their best selves and achieve amazing things. We believe fashion thrives on individuality and should be fun for everyone.</h1>

                    <script type="text/javascript">
                        $j(document).ready(function() {
                            $j("#videoPlayerBanner").InvPlayer();
                        });
                    </script>
                    <div id="videoPlayerBanner" class="inv_playerContainer inv_desktop inv_bannerPlayer inv_playerReady applyhover inv_videoReady inv_videoPlaying" inv_videoid="asos002" inv_playertype="singleplayer" inv_autoplay="true" inv_isbanner="true" inv_loopenabled="true" inv_videodatatype="brightcove">
                        <div class="inv_videoMainContainer inv_mouseLeave noCursor">
                            <div class="inv_videoMainContainer_inner">
                                <div class="inv_videoDisplay">
                                    <div class="inv_videoLoaderArea" style="display: none;"></div>
                                    <div class="inv_overlayPlay"></div>
                                    <div class="inv_videoStillArea" style="display: none;">
                                        <img src="https://f1.media.brightcove.com/7/1555966121001/1555966121001_5209370332001_5209360879001-vs.jpg?pubId=1555966121001&amp;videoId=5209360879001">
                                    </div>
                                    <div class="inv_videoArea" referenceid="asos002"><img src="https://f1.media.brightcove.com/7/1555966121001/1555966121001_5209370332001_5209360879001-vs.jpg?pubId=1555966121001&amp;videoId=5209360879001">
                                        <video id="inv_VideoPlayer_1585082499226" width="100%" height="100%" playsinline="" muted="" loop="loop">
                                            <source src="https://f1.media.brightcove.com/4/1555966121001/1555966121001_5209370355001_5209360879001.mp4?pubId=1555966121001&amp;videoId=5209360879001" type="video/mp4">Your browser does not support the video tag.</video>
                                    </div>
                                    <div class="inv_container" id="inv_container"></div>
                                </div>
                                <div class="inv_vrSphericalControls">
                                    <div class="inv_viewUp inv_moveView" inv-data-direction="up"></div>
                                    <div class="inv_viewLeft inv_moveView" inv-data-direction="left"></div>
                                    <div class="inv_viewRight inv_moveView" inv-data-direction="right"></div>
                                    <div class="inv_viewDown inv_moveView" inv-data-direction="down"></div>
                                </div>
                                <div class="inv_videoControls">
                                    <div class="inv_playheadWellContainer">
                                        <div class="inv_playhead inv_initialPlayhead" style="left: 63.7453%;"></div>
                                        <div class="inv_playheadWellWatched inv_initialPlayheadwell" style="width: 63.7453%;"></div>
                                        <div class="inv_playheadWellBuffered inv_initialPlayheadwell" style="width: 100%;"></div>
                                        <div class="inv_playheadWell"></div>
                                    </div>
                                    <div class="inv_playPauseBtn inv_pauseBtn"></div>
                                    <div class="inv_soundContainer">
                                        <div class="inv_soundOnOffBtn inv_soundOnBtn"></div>
                                    </div>
                                    <div class="inv_startTime">00:13</div>
                                    <div class="inv_seperatorTime">/</div>
                                    <div class="inv_endTime">00:21</div>
                                    <div class="inv_fullscreenBtn"></div>
                                    <div class="inv_externalLink"></div>
                                </div>
                            </div>
                        </div>

                        <ul id="MyMenu" class="contextMenu" style="top: 119px; left: 455px;">
                            <li>
                                <a href="http://www.investis.com" target="_blank">Delivered by Investis // www.investis.com</a>
                            </li>
                            <li>
                                <a style="cursor:default;">Version 2.0</a>
                            </li>
                        </ul>
                    </div>

                    <div class="landing-banner-text-wrapper">
                        <div class="landing-bnr-txt ">
                            <h2 class="">WE ARE AUTHENTIC, BRAVE AND CREATIVE TO OUR CORE</h2>
                        </div>
                        <div class="landing-bnr-subtxt trans">
                            <p>We focus on fashion as a force for good, inspiring young people to express their best selves and achieve amazing things. We believe fashion thrives on individuality and should be fun for everyone.</p>
                        </div>

                        <div class="green-box-link">
                            <p>
                                <a href="/asos-story">
                                    <strong><span data-hover="Discover the ASOS story">Discover the ASOS story</span></strong>
                                    <span class="icon-chevron-right"></span>
                                </a>
                            </p>
                        </div>

                    </div>

                </div>
                {{--                <section id="ArcadiaGroup" class="module" style="background-image:url('frontend/assets/images/180525-arcadia_p8x2483-resize.jpg'); margin-top:0;" data-parallax="scroll" data-image-src="/media/2219/180525-arcadia_p8x2483-resize.jpg">--}}
                {{--                    <div class="stores-coming-soon-holder">--}}
                {{--                        <div class="container">--}}
                {{--                            <div class="col-sm-12" style="margin: 0 auto;display: table;">--}}
                {{--                                <div class="middle-white-small-box">--}}
                {{--                                    <h2 style="text-transform:uppercase;">--}}
                {{--                                       we are authentic , brave and creative to our core--}}
                {{--                                    </h2>--}}
                {{--                                </div>--}}
                {{--                                <div class="middle-white-small-box">--}}
                {{--                                    <p>We focus on fashion as a force for good , inspiring young people to express their selves and achieve amazing things. We believe fashion thrives on individuality and should be fun for everyone</p>--}}
                {{--                                </div>--}}
                {{--                                <div class="middle-white-small-box">--}}
                {{--                                    <button type="submit" class="button" style="margin: 0 auto;display: table;">Discover the asos story</button>--}}
                {{--                                </div>--}}
                {{--                            </div>--}}
                {{--                            <div class="col-sm-6">--}}

                {{--                            </div>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                </section>--}}

                <section class="module-top">
                    <div class="container text-center">
                        <div class="about_us_text">
                            <p>Attracting talented people, developing their skills and helping them understand their own individual style is something we are pretty good at. Our passionate and committed teams keep us at the cutting-edge of high-street retail making this a fast-paced and challenging place to work. It's also hugely rewarding. With so many opportunities to grow professionally, learn new things, build connections we give you the chance to take control or your career and future with Topshop Topman.&nbsp;</p>
                        </div>

                        <h1>
                            OUR TIMELINE
                        </h1>
                        <div class="tabs">

                            <input type="radio" id="tab1" name="tab-control" checked>
                            <input type="radio" id="tab2" name="tab-control">
                            <input type="radio" id="tab3" name="tab-control">
                            <input type="radio" id="tab4" name="tab-control">
                            <input type="radio" id="tab5" name="tab-control">
                            <input type="radio" id="tab6" name="tab-control">
                            <input type="radio" id="tab7" name="tab-control">
                            <ul>
                                <li title="2013">
                                    <label for="tab1" role="button">
                                        <br>
                                        <span>2013</span>
                                    </label>
                                </li>
                                <li title="2014">
                                    <label for="tab2" role="button">
                                        <br><span>2014</span></label>
                                </li>
                                <li title="2015">
                                    <label for="tab3" role="button">
                                        <br><span>2015</span></label>
                                </li>
                                <li title="2016">
                                    <label for="tab4" role="button">
                                        <br><span>2016</span></label>
                                </li>
                                <li title="2017">
                                    <label for="tab5" role="button">
                                        <br><span>2017</span></label>
                                </li>
                                <li title="2018">
                                    <label for="tab6" role="button">
                                        <br><span>2018</span></label>
                                </li>
                                <li title="2019">
                                    <label for="tab7" role="button">
                                        <br><span>2019</span></label>
                                </li>
                            </ul>

                            <div class="slider">
                                <div class="indicator">
                                </div>
                            </div>

                            <div class="content">
                                <section>
                                    <h2>2013</h2>
                                    <div class="col-lg-6">
                                        <img src="frontend/assets/images/aboutpic.jpg">
                                    </div>
                                    <div class="col-lg-6">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem quas adipisci a accusantium eius ut voluptatibus ad impedit nulla, ipsa qui. Quasi temporibus eos commodi aliquid impedit amet, similique nulla.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem quas adipisci a accusantium eius ut voluptatibus ad impedit nulla, ipsa qui. Quasi temporibus eos commodi aliquid impedit amet, similique nulla.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem quas adipisci a accusantium eius ut voluptatibus ad impedit nulla, ipsa qui. Quasi temporibus eos commodi aliquid impedit amet, similique nulla.</p>
                                    </div>
                                </section>

                                <section>
                                    <h2>2014</h2>
                                    <div class="col-lg-6">
                                        <img src="frontend/assets/images/aboutpic.jpg">
                                    </div>
                                    <div class="col-lg-6">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem quas adipisci a accusantium eius ut voluptatibus ad impedit nulla, ipsa qui. Quasi temporibus eos commodi aliquid impedit amet, similique nulla.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem quas adipisci a accusantium eius ut voluptatibus ad impedit nulla, ipsa qui. Quasi temporibus eos commodi aliquid impedit amet, similique nulla.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem quas adipisci a accusantium eius ut voluptatibus ad impedit nulla, ipsa qui. Quasi temporibus eos commodi aliquid impedit amet, similique nulla.</p>
                                    </div>
                                </section>

                                <section>
                                    <h2>2015</h2>
                                    <div class="col-lg-6">
                                        <img src="frontend/assets/images/aboutpic.jpg">
                                    </div>
                                    <div class="col-lg-6">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem quas adipisci a accusantium eius ut voluptatibus ad impedit nulla, ipsa qui. Quasi temporibus eos commodi aliquid impedit amet, similique nulla.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem quas adipisci a accusantium eius ut voluptatibus ad impedit nulla, ipsa qui. Quasi temporibus eos commodi aliquid impedit amet, similique nulla.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem quas adipisci a accusantium eius ut voluptatibus ad impedit nulla, ipsa qui. Quasi temporibus eos commodi aliquid impedit amet, similique nulla.</p>
                                    </div>
                                </section>

                                <section>
                                    <h2>2016</h2>
                                    <div class="col-lg-6">
                                        <img src="frontend/assets/images/aboutpic.jpg">
                                    </div>
                                    <div class="col-lg-6">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem quas adipisci a accusantium eius ut voluptatibus ad impedit nulla, ipsa qui. Quasi temporibus eos commodi aliquid impedit amet, similique nulla.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem quas adipisci a accusantium eius ut voluptatibus ad impedit nulla, ipsa qui. Quasi temporibus eos commodi aliquid impedit amet, similique nulla.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem quas adipisci a accusantium eius ut voluptatibus ad impedit nulla, ipsa qui. Quasi temporibus eos commodi aliquid impedit amet, similique nulla.</p>
                                    </div>
                                </section>

                                <section>
                                    <h2>2017</h2>
                                    <div class="col-lg-6">
                                        <img src="frontend/assets/images/aboutpic.jpg">
                                    </div>
                                    <div class="col-lg-6">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem quas adipisci a accusantium eius ut voluptatibus ad impedit nulla, ipsa qui. Quasi temporibus eos commodi aliquid impedit amet, similique nulla.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem quas adipisci a accusantium eius ut voluptatibus ad impedit nulla, ipsa qui. Quasi temporibus eos commodi aliquid impedit amet, similique nulla.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem quas adipisci a accusantium eius ut voluptatibus ad impedit nulla, ipsa qui. Quasi temporibus eos commodi aliquid impedit amet, similique nulla.</p>
                                    </div>
                                </section>
                                <section>
                                    <h2>2018</h2>
                                    <div class="col-lg-6">
                                        <img src="frontend/assets/images/aboutpic.jpg">
                                    </div>
                                    <div class="col-lg-6">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem quas adipisci a accusantium eius ut voluptatibus ad impedit nulla, ipsa qui. Quasi temporibus eos commodi aliquid impedit amet, similique nulla.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem quas adipisci a accusantium eius ut voluptatibus ad impedit nulla, ipsa qui. Quasi temporibus eos commodi aliquid impedit amet, similique nulla.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem quas adipisci a accusantium eius ut voluptatibus ad impedit nulla, ipsa qui. Quasi temporibus eos commodi aliquid impedit amet, similique nulla.</p>
                                    </div>
                                </section>

                                <section>
                                    <h2>2019</h2>
                                    <div class="col-lg-6">
                                        <img src="frontend/assets/images/aboutpic.jpg">
                                    </div>
                                    <div class="col-lg-6">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem quas adipisci a accusantium eius ut voluptatibus ad impedit nulla, ipsa qui. Quasi temporibus eos commodi aliquid impedit amet, similique nulla.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem quas adipisci a accusantium eius ut voluptatibus ad impedit nulla, ipsa qui. Quasi temporibus eos commodi aliquid impedit amet, similique nulla.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem quas adipisci a accusantium eius ut voluptatibus ad impedit nulla, ipsa qui. Quasi temporibus eos commodi aliquid impedit amet, similique nulla.</p>
                                    </div>
                                </section>

                            </div>
                        </div>
                    </div>
                </section>

                <section class="module" style="background: #e0e0e0">
                    <div class="col-md-6">
                        <div class="diagonal-right-box" style="margin-top: 50px;">
                            <h2>OUR STRATEGY</h2>
                            <button type="submit" class="button" style="font-size: 14px;margin-top: 15px;margin: 0 auto;display: block;">GREAT FASHION AT GREAT PRICE</button>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="diagonal-right-box">
                            <p style="padding-bottom: 40px;color: #9c9c9c;" class="commited_mobile">We are committed to showing the world that all our achievements over the last 16 years were only the beginning.
                                Our unwavering belief in pushing boundaries is powered by deep commitment to new technologies and ambitious goals.
                                And our unique understanding of 20 - something customers around the world is constantly evolving ,while remaining
                                at the heart of everything we do. Our in-house ASOS own label sits alongside an expertly curated selection of third-party
                                brands. We believe that fashion is given meaning by what real people wear - we channel this this to give our customers
                                exactly what they want, at the best price and quality.  </p>
                        </div>
                    </div>
                </section>

                <section class="module" style="background: #000; color: #fff;">
                    <div class="col-md-6">
                        <div class="diagonal-right-box" style="margin-top: 50px;color: #fff;" id="mobile_pi">
                            <p>We use the latest technologies to push the mobile shopping experience in bold new directions.
                                Our customers have high expectations  -we make sure to stay ahead of them. We want to nurture
                                our relationships with each individual customers as much as possible , not just for friction-free
                                shopping but for a lasting relationship built on the things they care about most. We understand our
                                customers because so many of us are our customers. Shopping on ASOS is more than a transactional process
                                -we are relaxant, useful and inspiring voice that speaks through engaging, diverse content.</p>
                        </div>
                    </div>
                    <div class="col-md-6" style="position: relative;bottom: 100px;" id="augumented_div">
                        <div class="vl-one"></div>
                        <div class="diagonal-right-box" style="color: #fff;font-size: 17px !important;border: 1px solid #fff;padding: 10px;" id="mobile_big_text">
                            <h2 style="font-size: 20px !important;color:#9c9c9c;">IT STARTED WITH A SIMPLE IDEA & OUR MISSION REMAINS THE SAME</h2>
                            <h2 style="font-size: 20px !important;">CREATE HIGH QUALITY , WELL-DESIGNED GOODS THAT WE WANTED OURSELVES</h2>
                        </div>
                        <div class="vl-two"></div>
                    </div>

                </section>

                <section style="background: #fafafa">
                    <div class="col-lg-6">
                        <p style="padding: 50px;color: #9c9c9c" id="signup_mobile">SIGN UP TO RECEIVE 10% OFF YOUR FIRST ORDER</p>
                    </div>

                    <div class="col-md-6">
                        <form method="get" class="search" action="#" style="padding: 35px;    margin-right: 75px;">
                            <fieldset>
                                <input type="hidden" value="12556" name="storeId">
                                <input type="hidden" value="33057" name="catalogId">
                                <input type="hidden" value="-1" name="langId">
                                <input type="hidden" value="false" name="viewAllFlag">
                                <input type="hidden" value="277562" name="categoryId">
                                <input type="hidden" value="true" name="interstitial">

                                <div class="col-md-8" style="padding: 0px !important;">
                                    <input type="email" class="field" name="EMAIL" placeholder="Your email address">
                                </div>
                                <div class="col-md-4" style="padding: 0px !important;">
                                    <button type="submit" class="button button--full">Subscribe</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </section>

            </div>
            <!--/ .inner -->

        </main>
        <!--/ .site-content -->

        <!-- Site Footer -->
    @include('frontend.blog.layouts.footer')
    <!--/ .site-footer -->

    </div>

@endsection
