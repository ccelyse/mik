@extends('frontend.blog.layouts.master')

@section('title', 'Made in kigali')

@section('content')
    <link href="frontend/assets/css/site.css" rel="stylesheet">
    <link href="frontend/assets/css/custom.css" rel="stylesheet">
{{--    <link href="frontend/assets/css/_functions.css" rel="stylesheet">--}}
    <style>
        @font-face {
            font-family: 'silver';
            src: url("frontend/assets/css/fonts/silvertonewoodtypedtdextracondosf.eot");
            src: url("frontend/assets/ffonts/silvertonewoodtypedtdextracondosf.woff") format("woff");
            font-weight: 400;
            font-style: normal; }
        @font-face {
            font-family: 'helvetica-pro';
            src: url("frontend/assets/css/fonts/helveticaneueltpro-roman.eot");
            src: url("frontend/assets/css/fonts/helveticaneueltpro-roman.woff") format("woff");
            font-weight: 400;
            font-style: normal; }
        @font-face {
            font-family: 'helvetica-pro';
            src: url("frontend/assets/css/fonts/helveticaneueltpro-hv.eot");
            src: url("frontend/assets/css/fonts/helveticaneueltpro-hv.woff") format("woff");
            font-weight: 700;
            font-style: normal; }
        @font-face {
            font-family: 'knockout';
            src: url("frontend/assets/css/fonts/knockout-htf46-flyweight.eot");
            src: url("frontend/assets/css/fonts/knockout-htf46-flyweight.woff") format("woff");
            font-weight: 500;
            font-style: normal; }
        .inner {
            /*margin: 0 auto;*/
            /*max-width: 1230px;*/
            max-width: 100%;
            padding: 0px !important;
            position: relative;
            width: 100%;
        }
        .top_bar_reg{
            color: #936923;
        }
        .notice-bar a {
            display: block;
            text-decoration: none;
            color: #fff;
            padding: 11px;
        }
        .site-header {
            background: #fff;
            padding: 0 15px;
        }
        .site-container {
            background-color: #fff !important;
            color: #232323;
        }
        .site-footer__navigation {
            padding: 77px 15px 50px !important;
        }
        .col-x2-full h1.from-top {
            font-size: 65px !important;
        }
        .col-x2-full.banner p {
            font-size: 13px !important;
        }
        #banner .inner {
            min-height: 0px !important;
        }
        #banner img {
            width: 100%;
            bottom: 0px;
            /*-o-object-position: center center;*/
            object-position: top;
        }
        @media screen and (max-width: 89rem){
            .col-x2-full .inner {
                padding: 5% !important;
            }
        }
        @media only screen and (max-width: 48rem){
                .col-x2-full.banner p {
                    font-size: 1em;
                    background: transparent !important;
                }
                .col-x2-full.banner h1 {
                    /*color: #000 !important;*/
                }
                .col-x2-full .wrap-text {
                    padding-bottom: 200px;
                }
                .wrap-text h1,h2{
                    color: #fff !important;
                }
                #howwedoit{
                    color: #000 !important;
                }
                #howwedoitsection{
                    width: 100% !important;
                    display: inline-block;
                    position: relative;
                    right: 0px !important;
                }
                #factoryofemotions{
                    width: 100% !important;
                    display: inline-block;
                    position: relative;
                    right: 0px !important;
                }
                #howwedoitsection h1{
                    color: #000 !important;
                }
                #Cobrands h1,h2{
                    color: #000 !important;
                }
                #experience h1{
                    color: #000 !important;
                }
            .site-nav-button {
                position: relative;
                top: 50px;
            }
            }
</style>
    <div class="group site-container">
        @include('frontend.blog.layouts.upmenu')
        <main class="group site-content">
            <div class="inner" style="margin: 0px !important;">
                <div id="banner howwedo" class="section animate">
                    <div class="col-x2-full banner">
                        <div class="wrap-imgcover from-right">
                            <img src="frontend/assets/images/madeinkigalirwanda03.png" alt="">
                        </div>
                        <div class="inner" style="margin: 0px !important;">
                            <div class="wrap-text" style=" margin: 0 auto;display: table;">
                                <h1 class="from-top" style="text-align: center;">`The Africa We want`</h1>
                                <h2 style="text-align: center;font-size: 26px; padding: 10px;">Support The Movement</h2>
                                <a href="#" class="btn-link1" style=" margin: 0 auto;display: inherit;">MORE ABOUT US</a>
                                <div class="from-left" style="margin-top: 20px !important;background: #f5f7f6;padding: 25px;">
                                    <h1 class="from-top" id="howwedoit">HOW WE DO IT</h1>
                                    <p>So we make sure everyone has an equal chance to discover all the amazing things they are capable of. not matter who they are ,where they are from or what looks
                                        they like to boss. We exist to give you the confidence to be whoever you want be.</p>
                                    <p>We partner with groups of artisan women in Rwanda & Ghana to create a range of handcrafted products
                                        that bring beauty into the world through modern design , time honored
                                        technique and genuine artisan skill </p>
                                    <p>We partner with groups of artisan women in Rwanda & Ghana to create a range of handcrafted products
                                        that bring beauty into the world through modern design , time honored
                                        technique and genuine artisan skill </p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div id="apertura" class="section block-txt-img leftside   madewithlove-apertura">
                    <div class="col-x2-full" id="About_Us_">
                        <div class="wrap-imgcover left">
                            <img src="frontend/assets/images/elys.jpg" alt="">
                        </div>

                        <div class="inner" style="margin: 0px !important;">
                            <div class="wrap-text col-lg-12" style="margin-top: 20px !important;background: #f5f7f6;padding: 25px;border-radius: 60px;position: relative;right: 25px;" id="howwedoitsection">
                                <h1 class="title" style="font-size: 45px;">OUR PRODUCTS</h1>
                                <p>We believe in a world where you have total freedom to be you, without
                                judgement. To experiment to express yourself. To be brave and grab life as the
                                as the extraordinary adventure it is. So we make sure everyone has an equal chance to discover
                                all the amazing things they are capable of no matter who they are,where they are from or what looks they like to boss. We exist to give you the confidence to be whoever you want to be.</p>
                                <p>We partner with groups of artisan women in Rwanda & Ghana to create a range of handcrafted products
                                    that bring beauty into the world through modern design , time honored
                                    technique and genuine artisan skill </p>
                            </div>
                            <div class="wrap-text col-lg-12" style="margin-top: 20px !important;background: #439cd7;padding: 25px;position: relative;right: 25px;" id="factoryofemotions">
                                <h1 class="title" style="font-size: 45px;">FACTORY OF EMOTIONS</h1>
                                <p>What identifies us is our aim to establish beneficial relationships what the people associated with Desigual and what we are always
                                looking for a different way to do fashion and to do things in general . We truly care about certain issues and we make an effort about them
                                every single day. One of them is the way our actions impact on our surroundings </p>
                                <p>Since then we have been working with a special focus on the suppliers on our supply chain
                                , a key issues for us. our garments feature a multitude of details and undergo numerous  processors
                                and for this reason it is important to collaborate with highly specialized and trusted supplier with whom we have
                                established a very close relationship. Most of all they have to share our same values for this reason
                                any supplier who impends to establish a commercial relationship with Desigual must comply with close responsibility
                                requirements in relationship the working conditions in their factories and undergo Homologation in relation to the
                                working conditions in their in relation to the working conditions in their factories and undergo Homologation.
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <div id="apertura" class="section block-txt-img leftside   madewithlove-apertura">
                    <div class="col-x2-full" id="experience_">
                        <div class="col-lg-6">
                            <div class="wrap-text col-lg-12" style="margin-top: 20px !important;padding: 25px;border-radius: 60px;" id="experience">
                                <h1 class="title" style="font-size: 45px;">THE MIK EXPERIENCE</h1>
                                <p>Free delivery and returns We continually strive to make your experience with us as seamless as possible.
                                In real speak , that's all about our free delivery and returns it's one of your favorite ASOS things (and we could not agree more).Student discount (10% off until you graduate)
                                Being a student can mean you are low on funds, but don't worry. we got you. We give students 10% off all the way through to graduation. your budget should not stop you being you.</p>
                            </div>
                            <img src="frontend/assets/images/madeinkigali04.png" alt="">
                        </div>
                        <div class="col-lg-6">
                            <div class="inner" style="margin: 0px !important;float: none;width: 100%;">
                                <div class="wrap-text col-lg-12" style="margin-top: 20px !important;padding: 25px;border-radius: 60px;position: relative;right: 25px;" id="Cobrands">
                                    <h1 class="title" style="font-size: 45px;">THE MIK & CO BRANDS</h1>
                                    <p>ASOS DESIGN</p>
                                    <p>Giving you the confidence to express your individuality. ASOS DESIGN interprets major trends,adding that next-level
                                    ASOS Representing in our size ranges ASOS curve. We've got all the stuff you need to invent a style that's all your... making everyday ,night and everything in between as extraordinary
                                    as you are making everyday ,night and everything in between as extraordinary as you are.</p><br>
                                    <p>ASOS EDITION</p>
                                    <p>is designed for the most memorable moments of your life so you can turn and stand out, that's as unique as you. In our size ranges and including our beautiful ASOS EDITION Wedding
                                    Collection ,we'll have you dancing before the party;s even</p><br>
                                    <p>ASOS WHITE</p>
                                    <p>Backing oversized fits with a minimal clean aesthetic. ASOS WHITE is here to elevate your everyday.
                                        Believing investment pieces don't have to compromise on individuality. it created understated with
                                        a modern twist that you will be proud to wear -not to mention look damn cool in.</p><br>

                                    <p>ASOS MADE IN KENYA</p>
                                    <p>No longer a choice between conscience and self-expression ,we believe fashion has the power to build futures. That's why our exclusive ASOS MADE IN KENYA collection
                                    works with SOKO Kenya to improve the lives of local communities by effecting skills
                                    and support to drive sustainable development. Does good looks good.</p>

                                    <h2 style="text-align: center;font-size: 26px; padding: 10px;">`Made to get it right`</h2>
                                    <a href="#" class="btn-link1" style="margin: 0 auto;display: table;">DISCOVER THE MIK STORY</a>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
                <section style="background: #fafafa" id="footer_menu">
                    <div class="col-lg-6">
                        <p style="padding: 50px;color: #9c9c9c" id="signup_mobile">SIGN UP TO RECEIVE 10% OFF YOUR FIRST ORDER</p>
                    </div>

                    <div class="col-md-6">
                        <form method="get" class="search" action="#" style="padding: 35px;    margin-right: 75px;">
                            <fieldset>
                                <input type="hidden" value="12556" name="storeId">
                                <input type="hidden" value="33057" name="catalogId">
                                <input type="hidden" value="-1" name="langId">
                                <input type="hidden" value="false" name="viewAllFlag">
                                <input type="hidden" value="277562" name="categoryId">
                                <input type="hidden" value="true" name="interstitial">

                                <div class="col-md-8" style="padding: 0px !important;">
                                    <input type="email" class="field" name="EMAIL" placeholder="Your email address">
                                </div>
                                <div class="col-md-4" style="padding: 0px !important;">
                                    <button type="submit" class="button button--full">Subscribe</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </section>
            </div>
            <!--/ .inner -->

        </main>
        <!--/ .site-content -->

        <!-- Site Footer -->
    @include('frontend.blog.layouts.footer')
    <!--/ .site-footer -->

    </div>

@endsection
