@extends('frontend.blog.layouts.master')

@section('title', 'Made in kigali')

@section('content')
    <link href="frontend/assets/css/site.css" rel="stylesheet">
    <link href="frontend/assets/css/custom.css" rel="stylesheet">
{{--    <link href="frontend/assets/css/_functions.css" rel="stylesheet">--}}
    <style>
        @font-face {
            font-family: 'silver';
            src: url("frontend/assets/css/fonts/silvertonewoodtypedtdextracondosf.eot");
            src: url("frontend/assets/ffonts/silvertonewoodtypedtdextracondosf.woff") format("woff");
            font-weight: 400;
            font-style: normal; }
        @font-face {
            font-family: 'helvetica-pro';
            src: url("frontend/assets/css/fonts/helveticaneueltpro-roman.eot");
            src: url("frontend/assets/css/fonts/helveticaneueltpro-roman.woff") format("woff");
            font-weight: 400;
            font-style: normal; }
        @font-face {
            font-family: 'helvetica-pro';
            src: url("frontend/assets/css/fonts/helveticaneueltpro-hv.eot");
            src: url("frontend/assets/css/fonts/helveticaneueltpro-hv.woff") format("woff");
            font-weight: 700;
            font-style: normal; }
        @font-face {
            font-family: 'knockout';
            src: url("frontend/assets/css/fonts/knockout-htf46-flyweight.eot");
            src: url("frontend/assets/css/fonts/knockout-htf46-flyweight.woff") format("woff");
            font-weight: 500;
            font-style: normal; }
        .inner {
            /*margin: 0 auto;*/
            /*max-width: 1230px;*/
            max-width: 100%;
            padding: 0px !important;
            position: relative;
            width: 100%;
        }
        .top_bar_reg{
            color: #936923;
        }
        .notice-bar a {
            display: block;
            text-decoration: none;
            color: #fff;
            padding: 11px;
        }
        .site-header {
            background: #fff;
            padding: 0 15px;
        }
        .site-container {
            background-color: #fff !important;
            color: #232323;
        }
        .site-footer__navigation {
            padding: 77px 15px 50px !important;
        }
        @media screen and (min-width: 1900px){
            .col-x2-full .inner {
                padding-left: 15% !important;
            }
        }
        .homemulticolumn {
            padding-top: 15px;
            padding-bottom: 20px;
            padding: 100px;
        }
    </style>
    <div class="group site-container">
        @include('frontend.blog.layouts.upmenu')
        <main class="group site-content">
            <div class="inner" style="margin: 0px !important;">
                <div id="banner" class="section animate">
                    <div class="col-x2-full banner">
                        <div class="wrap-imgcover from-right">
                            <img src="frontend/assets/images/madeinkigali01.png" alt="">
                        </div>
                        <div class="inner" style="margin: 0px !important;">
                            <div class="wrap-text">
                                <h1 class="from-top">Atypical since 1984</h1>
                                <div class="from-left"><p>We are a global brand with over 3,700 employees of 100 different nationalities. We invite people to express their authenticity through unique clothing.</p>
                                </div>
                                <a href="{{url('HowWeDoIt')}}" class="btn-link1">ABOUT US</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="section text-multicolums homemulticolumn" style="width: 100%;float: left;background: #f8f8f6">
                    <div class="large-row">
                        <div class="cols-x-5 gutter-x1">
                            <div class="block">
                                <div class="wrap-text">
                                    <h3>About Us</h3>
                                    <p>We believe in word where you have total freedom to be you without judgement.</p>
                                    <a href="{{url('HowWeDoIt')}}#howwedo" class="btn-link1">WHO WE ARE</a>
                                </div>
                            </div>
                            <div class="block">
                                <div class="wrap-text">
                                    <h3>Our Products</h3>
                                    <p>When it comes to sourcing the perfect places,we don't just focus on commercial criteria
                                    - we put ethical trading sustainable sourcing and animal welfare at the heart of how we work.</p>
                                    <a href="{{url('HowWeDoIt')}}#About_Us_" class="btn-link1">WHAT WE DO</a>

                                </div>
                            </div>
                            <div class="block">
                                <div class="wrap-text">
                                    <h3>A factory of emotions</h3>
                                    <p>We truly make an effort to do everything faultlessly. This is why we are extremely demanding with ourselves by making sure all our supplies our production process to be ethical and responsible</p>
                                    <a href="{{url('HowWeDoIt')}}#About_Us_" class="btn-link1" >PRODUCTION REQUIREMENTS</a>

                                </div>
                            </div>
                            <div class="block">
                                <div class="wrap-text">
                                    <h3>The MIK Experience</h3>
                                    <p>We never settle. We have on always testing ,`always in beta` philosophy , constantly improving to make it all just that bit better every day.</p>
                                    <a href="{{url('HowWeDoIt')}}#experience_" class="btn-link1">WHAT WE GUARANTEE</a>

                                </div>
                            </div>
                            <div class="block">
                                <div class="wrap-text">
                                    <h3>The MIK & Co Brands</h3>
                                    <p>We don't do fashion like anyone else does fashion. Our MIK & Co Brands created by our design team , look between the lines to bring you the freshest clothing , shoes ,accessories and gifts.</p>
                                    <a href="{{url('HowWeDoIt')}}#experience_" class="btn-link1" >OUR PROJECTS</a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="livedata" class="section infodatos scrolldetector livedatahome animate">
                    <div class="row">
                        <div class="cols-gutter-40px">
                            <div class="wide40">

                            </div>
                            <div class="wrapinfo wide60">
                                <div class="group cabecera_textos">
                                    <h2 class="title from-left">
                                        LIFE &amp; FACTS          </h2>
                                    <div class="text">
                                        We are a multi-category and multi-channel company. We design, distribute and sell 6 product categories through 10 channels in about 90 countries. Check this out          </div>

                                </div>

                                <div class="group categoria_distribucion">
                                    <div class="imagen">
                                        <img src="https://corporate.desigual.com/wp-content/themes/desigual/imgs/bolso.png" alt="">
                                    </div>
                                    <div class="data">
                                        <div class="from-top-move">
                                            <div class="num animate-num-slow" data-numero="6">8</div>
                                        </div>

                                        <div class="data-label label-pop1">
                                            PRODUCT CATEGORIES
                                        </div>
                                        <div class="pop pop1">
                                            Our 8 product categories
                                            <ul>
                                                <li>Woman</li>
                                                <li>Accessories</li>
                                                <li>Men</li>
                                                <li>Kids</li>
                                                <li>Living</li>
                                                <li>Beauty</li>
                                                <li>Arts</li>
                                                <li>Food</li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="data">
                                        <div class="from-top-move">
                                            <div class="num animate-num-slow" data-numero="10">5</div>
                                        </div>

                                        <div class="data-label label-pop2">
                                            DISTRIBUTION CHANNELS            </div>
                                        <div class="pop pop2">
                                            Our 5 distribution channels:
                                            <ul>
                                                <li>Single brand stores</li>
                                                <li>Multi-brand stores</li>
                                                <li>MIK authorised</li>
                                                <li>Department stores authorised</li>
                                                <li>Our own online store</li>
                                            </ul>


                                        </div>
                                    </div>
                                </div>

                                <div class="group facturacion_empleados">
                                    <div class="data">
                                        <div class="from-top-move">
                                            <div class="num animate-num-slow" data-numero="3700">5</div>
                                        </div>
                                        <div class="data-label">
                                            EMPLOYEES
                                        </div>
                                    </div>

                                    <div class="data">
                                        <div class="from-top-move">
                                            <div class="num animate-num-slow" data-numero="3700">7</div>
                                        </div>
                                        <div class="data-label">
                                            EMPLOYEES
                                        </div>
                                    </div>
                                </div>
                                <div class="group facturacion_textos">
                                    <div class="data">
                                        <div class="from-top-move">
                                            <div class="num animate-num-slow" data-numero="3700">70%</div>
                                        </div>
                                        <div class="data-label">
                                            EMPLOYEES
                                        </div>
                                    </div>

                                    <div class="data">
                                        <div class="from-top-move">
                                            <div class="num animate-num-slow" data-numero="3700">1200</div>
                                        </div>
                                        <div class="data-label">
                                            EMPLOYEES
                                        </div>
                                    </div>
                                </div>

                                <div class="group mapa">
                                    <div class="data">
                                        <div class="map">
                                            <img src="https://corporate.desigual.com/wp-content/themes/desigual/imgs/mapamundihome.jpg" alt="" style="width: 147px;">
                                        </div>
                                    </div>

                                    <div class="data">
                                        <div class="from-top-move">
                                            <div class="num animate-num-slow" data-numero="90">7</div>
                                        </div>

                                        <div class="data-label">
                                            MARKETS            </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div id="newsroom" class="section scrolldetector beige animate">
                    <div class="row">
                        <div class="head-section">
                            <div class="group">
                                <h2 class="title ">
                                    THE EDIT
                                </h2>
                                <div class="text ">
                                    Get your MIK from the trends you need to know from MIK
                                </div>
                            </div>
                            <a class="btn-link1" href="#">
                                ACCESS THE EDIT</a>

                        </div>

                    </div>
                </div>

            </div>
            <!--/ .inner -->

        </main>
        <!--/ .site-content -->

        <!-- Site Footer -->
        <script type='text/javascript' src='https://corporate.desigual.com/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
        <script type='text/javascript' src='https://corporate.desigual.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
        <script type='text/javascript' src='https://corporate.desigual.com/wp-content/themes/desigual/vendor/ofi.min.js?ver=20171215'></script>
        <script type='text/javascript' src='https://corporate.desigual.com/wp-content/themes/desigual/vendor/jquery.waypoints.min.js?ver=20171220'></script>
        <script type='text/javascript' src='https://corporate.desigual.com/wp-content/themes/desigual/js/jquery.matchHeight-min.js?ver=20171215'></script>
        <script type='text/javascript' src='https://corporate.desigual.com/wp-content/themes/desigual/js/jquery.animateNumbers.min.js?ver=20171215'></script>
        <script type='text/javascript' src='https://corporate.desigual.com/wp-content/themes/desigual/js/owl.carousel.min.js?ver=20171215'></script>
        <script type='text/javascript' src='https://corporate.desigual.com/wp-content/themes/desigual/js/functions.js?ver=20171215'></script>
    @include('frontend.blog.layouts.footer')
    <!--/ .site-footer -->

    </div>

@endsection
