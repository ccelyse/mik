@extends('frontend.blog.layouts.master')

@section('title', 'Made in kigali')

@section('content')
    <link href="frontend/assets/css/site.css" rel="stylesheet">
    <link href="frontend/assets/css/custom.css" rel="stylesheet">
{{--    <link href="frontend/assets/css/_functions.css" rel="stylesheet">--}}
    <style>
        @import url("https://fonts.googleapis.com/css?family=Overpass:300,400,700,900&display=swap");
        @font-face {
            font-family: 'silver';
            src: url("frontend/assets/css/fonts/silvertonewoodtypedtdextracondosf.eot");
            src: url("frontend/assets/ffonts/silvertonewoodtypedtdextracondosf.woff") format("woff");
            font-weight: 400;
            font-style: normal; }
        @font-face {
            font-family: 'helvetica-pro';
            src: url("frontend/assets/css/fonts/helveticaneueltpro-roman.eot");
            src: url("frontend/assets/css/fonts/helveticaneueltpro-roman.woff") format("woff");
            font-weight: 400;
            font-style: normal; }
        @font-face {
            font-family: 'helvetica-pro';
            src: url("frontend/assets/css/fonts/helveticaneueltpro-hv.eot");
            src: url("frontend/assets/css/fonts/helveticaneueltpro-hv.woff") format("woff");
            font-weight: 700;
            font-style: normal; }
        @font-face {
            font-family: 'knockout';
            src: url("frontend/assets/css/fonts/knockout-htf46-flyweight.eot");
            src: url("frontend/assets/css/fonts/knockout-htf46-flyweight.woff") format("woff");
            font-weight: 500;
            font-style: normal; }
        .inner {
            /*margin: 0 auto;*/
            /*max-width: 1230px;*/
            max-width: 100%;
            padding: 0px !important;
            position: relative;
            width: 100%;
        }
        .top_bar_reg{
            color: #936923;
        }
        .notice-bar a {
            display: block;
            text-decoration: none;
            color: #fff;
            padding: 11px;
        }
        .site-header {
            background: #fff;
            padding: 0 15px;
        }
        .site-container {
            background-color: #fafafa !important;
            color: #232323;
        }
        .col-x2-full h1.from-top {
            color: #901b22 !important;
            /*font-size: 135px;*/
            font-size: 4.68em;
        }
        .site-footer__navigation {
            padding: 77px 15px 50px !important;
        }
        .col-x2-full.banner p {
            font-size: 16px;
            line-height: 1.5;
            margin-top: 10px;
            font: normal 16px Overpass, sans-serif;
        }
        @media only screen and (max-width: 48rem){
            .col-x2-full.banner p {
                background: transparent !important;
                color: #fff;
            }
            .col-x2-full .wrap-text {
                display: table-cell;
                vertical-align: middle;
                /*padding: 200px 0;*/
            }
            .col-x2-full.banner .inner {
                padding-top: 0px !important;
            }
        }
        .comingsoon_button{
            margin: 0 auto;
            display: table;
        }
        .col-x2-full .btn-link1 {
            margin-top: 14px;
            margin: 22px;
        }
        .btn-link1, [type="submit"] {
            padding: 12px 22px;
        }
        .col-x2-full h1.from-top {
            font: normal 5em Overpass, sans-serif;
        }
    </style>
    <div class="group site-container">
        <header class="group site-header test">
            <div class="inner">
                <div role="banner" class="site-header__section site-header__section--branding">
                    <h1 class="site-header__logo" style="margin:0px !important;">
                        <a href="{{'/'}}" title="Made in Kigali">
                            <img src="frontend/assets/images/LOGOO.png" style="width: 90px;padding-top: 10px;">
                            <span class="visuallyhidden test">Made in Kigali</span></a>
                    </h1>
                </div>
            </div>

        </header>

        <main class="group site-content">
            <div class="inner" style="margin: 0px !important;">
                <div id="banner" class="section animate mobile_comingsoon">
                    <div class="col-x2-full banner">
                        <div class="inner" style="margin: 0px !important;">
                            <div class="wrap-text">
                                <h1 class="from-top">Coming Soon</h1>
                                <div class="from-left">
                                    <p>We are working really hard on creating something fantastic and are pretty interested to  see what you think! <br>
                                        It's nothing revolutionary,but will fix somethings that just aren't working as well as they should. <br>
                                        In the meantime follow our social media feeds or click the buttons bellow for more information about MIK.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="banner" class="section animate">
                    <div class="col-x2-full banner">
                        <div class="wrap-imgcover from-right">
                            <img src="frontend/assets/images/coming3.jpg" alt="">
                        </div>
                        <div class="inner" style="margin: 0px !important;" id="second_inner_mobile">
                            <div class="wrap-text">
                                <h1 class="from-top" id="comingsoon_title">Coming Soon</h1>
                                <div class="from-left" id="comingsoon_p">
                                    <p>We are working really hard on creating something fantastic and are pretty interested to  see what you think! <br>
                                        It's nothing revolutionary,but will fix somethings that just aren't working as well as they should. <br>
                                        In the meantime follow our social media feeds or click the buttons bellow for more information about MIK.</p>
                                </div>
                                <div class="comingsoon_button">
                                    <a href="{{url('AboutUs')}}" class="btn-link1">ABOUT US</a>
                                    <a href="{{url('WhoWeAre')}}" class="btn-link1">WHO WE ARE</a>
                                    <a href="{{url('BlogPage')}}" class="btn-link1">BLOG</a>
                                </div>

                                <div class="footer_corporativo" style="padding-top: 10px">
                                    <form role="form" name="contactForm" class="contactForm ng-pristine ng-invalid ng-invalid-required" id="rn_QuestionSubmit" method="post" action="#">
                                        <div class="col-lg-8 form-group" style="padding: 0px !important;">
                                            <div id="rn_TextInput_4" class="rn_TextInput rn_Input">
                                                <input type="text" name="" class="rn_Text" placeholder="We'll be here soon, subscribe to be notified" maxlength="80" required="">
                                            </div>
                                        </div>
                                        <div class="col-lg-4 form-group" style="padding: 0px !important;">
                                            <div id="rn_FormSubmit_15" class="rn_FormSubmit">
                                                <div id="rn_FormSubmit_15_ErrorLocation" aria-live="assertive"></div>
                                                <input type="submit" id="rn_FormSubmit_15_Button" value="Subscribe">
                                            </div>
                                        </div>
                                    </form>
                                    <ul class="countdown_li">
                                        <li><span id="days"></span>days</li>
                                        <li><span id="hours"></span>Hours</li>
                                        <li><span id="minutes"></span>Minutes</li>
                                        <li><span id="seconds"></span>Seconds</li>
                                    </ul>

                                    <div class="center_footer">
                                        <div class="row">
                                            <div class="menu-redes">
                                                <div class="menu-menu-footer-redes-en-container">
                                                    <ul id="menu-Footer-redes" class="menu">
                                                        <li id="menu-item-1368" class="twitter menu-item menu-item-type-custom menu-item-object-custom menu-item-1368"><a target="_blank" href="#">TWITTER</a></li>
                                                        <li id="menu-item-1369" class="facebook menu-item menu-item-type-custom menu-item-object-custom menu-item-1369"><a target="_blank" href="#">FACEBOOK</a></li>
                                                        <li id="menu-item-1370" class="instagram menu-item menu-item-type-custom menu-item-object-custom menu-item-1370"><a target="_blank" href="#">INSTAGRAM</a></li>
                                                        <li id="menu-item-1371" class="google menu-item menu-item-type-custom menu-item-object-custom menu-item-1371"><a target="_blank" href="#">GOOGLE</a></li>
                                                        <li id="menu-item-1372" class="pinterest menu-item menu-item-type-custom menu-item-object-custom menu-item-1372"><a target="_blank" href="#">PINTEREST</a></li>
                                                        <li id="menu-item-1373" class="youtube menu-item menu-item-type-custom menu-item-object-custom menu-item-1373"><a target="_blank" href="#">YOUTUBE</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
<script type="application/javascript">
    const second = 1000,
        minute = second * 60,
        hour = minute * 60,
        day = hour * 24;

    let countDown = new Date('april 30, 2020 00:00:00').getTime(),
        x = setInterval(function() {

            let now = new Date().getTime(),
                distance = countDown - now;

            document.getElementById('days').innerText = Math.floor(distance / (day)),
                document.getElementById('hours').innerText = Math.floor((distance % (day)) / (hour)),
                document.getElementById('minutes').innerText = Math.floor((distance % (hour)) / (minute)),
                document.getElementById('seconds').innerText = Math.floor((distance % (minute)) / second);

            //do something later when date is reached
            //if (distance < 0) {
            //  clearInterval(x);
            //  'IT'S MY BIRTHDAY!;
            //}

        }, second)
</script>
@endsection
