<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">

    <!-- Page Title-->
    <title>Made in Kigali| Coming soon </title>

    <!-- Meta Tags-->
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Elysee Confince">

    <!-- Viewport Meta-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <!-- Template Favicon & Icons Start -->
    <link rel="shortcut icon" sizes="16x16 24x24 32x32 48x48 64x64 96x96" href="comingsoon/img/favicon.jpeg" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="57x57" href="comingsoon/img/favicon.jpeg">
    <link rel="apple-touch-icon" sizes="60x60" href="comingsoon/img/favicon.jpeg">
    <link rel="apple-touch-icon" sizes="72x72" href="comingsoon/img/favicon.jpeg">
    <link rel="apple-touch-icon" sizes="76x76" href="comingsoon/img/favicon.jpeg">
    <link rel="apple-touch-icon" sizes="114x114" href="comingsoon/img/favicon.jpeg">
    <link rel="apple-touch-icon" sizes="120x120" href="comingsoon/img/favicon.jpeg">
    <link rel="apple-touch-icon" sizes="144x144" href="comingsoon/img/favicon.jpeg">
    <link rel="apple-touch-icon" sizes="152x152" href="comingsoon/img/favicon.jpeg">
    <link rel="apple-touch-icon" sizes="180x180" href="comingsoon/favicon.jpeg">
    <link rel="icon" type="image/png" sizes="192x192" href="comingsoon/img/favicon.jpeg">
    <!-- <link rel="manifest" href="comingsoon/img/favicon/site.webmanifest"> -->
    <link rel="mask-icon" href="comingsoon/img/favicon/safari-pinned-tab.svg" color="#25237e">
    <meta name="msapplication-config" content="comingsoon/img/favicon/browserconfig.xml">
    <meta name="msapplication-TileColor" content="#f95b5b">
    <!-- Template Favicon & Icons End-->

    <!-- Facebook Metadata Start -->
    <meta property="og:image:height" content="300">
    <meta property="og:image:width" content="573">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:url" content="http://mixdesign.club/themeforest/peachy/">
    <meta property="og:image" content="http://mixdesign.club/themeforest/peachy/img/favicon/og-image.jpg">
    <!-- Facebook Metadata End -->

    <!-- Template Styles Start -->
    <link rel="stylesheet" type="text/css" href="comingsoon/css/plugins.css">
    <link rel="stylesheet" type="text/css" href="comingsoon/css/loaders/loader.css">
    <link rel="stylesheet" type="text/css" href="comingsoon/css/main.css">
    <!-- Template Styles End-->

    <!-- Custom Browser Color Start-->
    <meta name="theme-color" content="#f95b5b">
    <meta name="msapplication-navbutton-color" content="#f95b5b">
    <meta name="apple-mobile-web-app-status-bar-style" content="#f95b5b">
    <!-- Custom Browser Color End-->

</head>

<body class="overflow-hidden">

<!-- Old Browsers Support Start-->
<!--[if lt IE 9]>
<script src="comingsoon/js/libs/es5-shim.min.js"></script>
<script src="comingsoon/js/libs/html5shiv.min.js"></script>
<script src="comingsoon/js/libs/html5shiv-printshiv.min.js"></script>
<script src="comingsoon/js/libs/respond.min.js"></script>
<![endif]-->
<!-- Old Browsers Support End-->

<!-- Loader Start -->
<div class="loader">
    <div class="loader-slide-left"></div>
    <div class="loader-slide-right"></div>
    <div class="loader-content">
        <div class="loader-logo slideInDown">
            <!-- Your Logo Here -->
            <img src="comingsoon/img/favicon.jpeg" style="width: 100px">
        </div>
        <div class="loader-caption slideInUp">
          <span class="loading-dots">
            <span class="dot"></span>
            <span class="dot"></span>
            <span class="dot"></span>
          </span>
        </div>
    </div>
</div>
<!-- Loader End-->

<!-- Custom HTML Start-->

<!-- Menu Trigger Start -->
<div class="menu-btn is-visible">
    <a href="index-split-slider.html#0" id="menu-trigger" class="menu-trigger"></a>
</div>
<!-- Menu Trigger End -->

<!-- Navigation Start -->
<nav id="menu" class="menu">

    <!-- Menu Container Start -->
    <div class="menu__container fullheight">

        <!-- Menu Close Button Start -->
        <div class="menu__controls">
            <a href="index-split-slider.html#0" id="menu-close" class="menu-close dark"></a>
        </div>
        <!-- Menu Close Button End -->

        <!-- Menu Content Start -->
        <div class="menu__content">
            <!-- Menu caption -->
            <p class="menu__caption">menu</p>
            <!-- Main menu -->
            <ul class="menu__navigation">
                <li>
                    <a href="{{'AboutUs'}}">About Us</a>
                </li>
                <li>
                    <a href="#">Who we are</a>
                </li>
                <li>
                    <a href="{{'BlogPage'}}">Blog</a>
                </li>
                <li>
                    <a href="{{'/'}}#0" id="contact-trigger">Contact</a>
                </li>
            </ul>
        </div>
        <!-- Menu Content End -->

    </div>
    <!-- Menu Container End -->

</nav>
<!-- Navigation End -->

<!-- Main Screen Section Start -->
<section id="main" class="main-section">

    <!-- Logo Block Start -->
    <div class="main-section__logo">
        <img src="comingsoon/img/LOGOO.png" alt="Peachy - Bright Coming Soon & Landing Page Template">
    </div>
    <!-- Logo Block End -->

    <!-- Main Section Content Start -->
    <div class="main-section__content">
        <div class="container-fluid p-0 fullheight">
            <div class="row no-gutters fullheight">

                <!-- Main Section Intro Start -->
                <div class="col-12 col-xl-6 main-section__intro">
                    <div class="intro-content">

                        <!-- Headline Start -->
                        <div id="headline" class="headline">
                            <h1>Coming<span>soon</span></h1>
                            <p class="headline__text">We are working really hard on creating something fantastic and are pretty interested to  see what you think! <br>
                                It's nothing revolutionary,but will fix somethings that just aren't working as well as they should. <br>
                                In the meantime follow our social media feeds or click the buttons bellow for more information about MIK.
                            </p>
                            <div class="headline__btnholder">
                                <a href="{{'AboutUs'}}" class="btn btn-dark">About Us</a>
                                <a href="{{url('WhoWeAre')}}" class="btn btn-dark">Who are we</a>
                                <a href="{{'BlogPage'}}" class="btn btn-dark">Blog</a>
                            </div>
                        </div>
                        <!-- Headline End -->

                        <!-- Socials Start -->
                        <div class="socials socials-text socials-bottom socials-dark">
                            <ul>
                                <li>
                                    <a href="https://www.facebook.com/" target="_blank">Facebook</a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/" target="_blank">Instagram</a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/" target="_blank">Twitter</a>
                                </li>
                            </ul>
                        </div>
                        <!-- Socials End -->

                    </div>
                </div>
                <!-- Main Section Intro End -->

                <!-- Main Section Media Start -->
                <div class="col-12 col-xl-6 main-section__media">
                    <div class="media-content">

                        <!-- Main Section Slider Start-->
                        <div class="media-slider">
                            <div class="swiper-container">
                                <!-- Additional required wrapper -->
                                <div class="swiper-wrapper">
                                    <!-- Slides -->
                                    <div class="swiper-slide swiper-slide-1"></div>
{{--                                    <div class="swiper-slide swiper-slide-2"></div>--}}
{{--                                    <div class="swiper-slide swiper-slide-3"></div>--}}
                                </div>
                            </div>
                            <!-- Control arrows -->
                            <div class="slider-controls">
                                <div class="swiper-button-next light">
                                    <span></span>
                                </div>
                                <div class="swiper-button-prev light">
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        <!-- Main Section Slider End-->

                    </div>
                </div>
                <!-- Main Section Media End -->

                <!-- Main Section Aside Start -->
                <div class="main-section__aside"></div>
                <!-- Main Section Aside End -->

            </div>
        </div>
    </div>
    <!-- Main Section Content End -->

</section>
<!-- Main Screen Section End -->

<!-- Contact Section Start -->
<section id="contact" class="content-section">
    <div class="container-fluid p-0 fullheight">
        <div class="row no-gutters fullheight">

            <!-- Section Info Start -->
            <div class="col-12 col-xl-6 content-section__info">
                <div class="scroll">

                    <!-- Section Info Blocks Container Start -->
                    <div class="blocks-container">

                        <!-- Section Controls Start -->
                        <div class="section-controls">
                            <a href="index-split-slider.html#0" id="contact-close" class="section-close"></a>
                        </div>
                        <!-- Section Controls End -->

                        <!-- Section Title Start -->
                        <div class="content-block">
                            <div class="section-title">
                                <h2>Welcome to our<br>new <span>Office</span></h2>
                                <p class="section-title__text">Our website is under construction but we are ready to go! You can call us or leave a request here. We are always glad to see you in our office from
                                    <span>9:00</span> to <span>18:00</span>.</p>
                            </div>
                        </div>
                        <!-- Section Title End -->

                        <!-- Section Content Block Contact Data  Start -->
                        <div class="content-block contact-data">
                            <div class="container-fluid">
                                <div class="row">
                                    <!-- Contact data single item -->
                                    <div class="col-12 col-sm-6 contact-data__item">
                                        <h5>Location</h5>
                                        <p class="small-text">
                                            Kigali Heights
                                            <br>Kigali ,Rwanda
                                        </p>
                                    </div>
                                    <!-- Contact data single item -->
                                    <div class="col-12 col-sm-6 contact-data__item">
                                        <h5>Follow us</h5>
                                        <ul>
                                            <li>
                                                <a href="https://www.facebook.com/" target="_blank">Facebook</a>
                                            </li>
                                            <li>
                                                <a href="https://www.instagram.com/" target="_blank">Instagram</a>
                                            </li>
                                            <li>
                                                <a href="https://twitter.com/" target="_blank">Twitter</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <!-- Contact data single item -->
                                    <div class="col-12 col-sm-6 contact-data__item">
                                        <h5>Phone</h5>
                                        <p class="small-text">
                                            <a href="tel:+12127089400">+250 782 384 772</a>
                                        </p>
                                    </div>
                                    <!-- Contact data single item -->
                                    <div class="col-12 col-sm-6 contact-data__item">
                                        <h5>Email</h5>
                                        <p class="small-text">
                                            <a href="#">info@madeinkigali.com</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Section Content Block - Contact Data - End -->

                        <!-- Section Content Block Contact Buttons Start -->
                        <div class="content-block contact-buttons">
                            <a href="index-split-slider.html#" id="letstalk-trigger" class="btn btn-dark">Let's talk</a>
                        </div>
                        <!-- Section Content Block Contact Buttons End -->

                    </div>
                    <!-- Section Info Blocks Container End -->

                </div>
            </div>
            <!-- Section Info End -->

            <!-- Section Media Start -->
            <div class="col-12 col-xl-6 content-section__media">
                <div class="static fullheight">

                    <!-- Section Media Container Start -->
                    <div class="media-container fullheight">

                        <!-- Google Map Start -->
                        <div class="map">
                            <div id="google-map">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15950.002161067952!2d30.0931309!3d-1.9530718!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xda6c2b3cabfec246!2sKigali%20Heights!5e0!3m2!1sen!2srw!4v1583840931769!5m2!1sen!2srw" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                                <div id="zoom-in">
                                    <span class="btn-icon fas fa-plus"></span>
                                </div>
                                <div id="zoom-out">
                                    <span class="btn-icon fas fa-minus"></span>
                                </div>
                            </div>
                        </div>
                        <!-- Google Map End -->

                    </div>
                    <!-- Section Media Container End -->

                </div>
            </div>
            <!-- Section Media End -->

        </div>
    </div>
</section>
<!-- Contact Section End -->

<!-- Let's Talk Popup Start -->
<div class="popup letstalk">
    <div class="popup__container">

        <!-- Popup Controls Start -->
        <div class="popup__controls">
            <a href="index-split-slider.html#0" id="letstalk-close" class="popup-close"></a>
        </div>
        <!-- Popup Controls End-->

        <!-- Popup Content Start -->
        <div class="popup__content">
            <div class="container-fluid p-0">
                <div class="row no-gutters flex-xl-row-reverse">
                    <div class="col-12 col-xl-6">

                        <!-- Popup Content Block Start -->
                        <div class="content-block">

                            <!-- Popup Title Start -->
                            <div class="popup-title">
                                <p class="popup-title__title anim-obj">Stay in&#8194;<span>touch</span></p>
                                <p class="popup-title__text small-text anim-obj">Want to know more about us, tell us about your project or just to say hello?
                                    Drop us a line and we will get back as soon as possible.</p>
                            </div>
                            <!-- Popup Title End -->

                            <!-- Form Container Start -->
                            <div class="form-container">

                                <!-- Let's Talk Form Reply Group Start -->
                                <div class="reply-group">
                                    <p class="reply-group__title">Done!</p>
                                    <p class="reply-group__text small-text">Thanks for your message. We will get back as soon as possible.</p>
                                </div>
                                <!-- Let's Talk Form Reply Group End -->

                                <!-- Let's Talk Form Start -->
                                <form class="form letstalk-form form-dark" id="letstalk-form">
                                    <!-- Hidden Required Fields -->
                                    <input type="hidden" name="project_name" value="Peachy - Bright Coming Soon & Landing Page Template">
                                    <input type="hidden" name="admin_email" value="support@mixdesign.club">
                                    <input type="hidden" name="form_subject" value="Let's Talk Form Message">
                                    <!-- END Hidden Required Fields-->
                                    <input type="text" name="Name" placeholder="Your Name*" required>
                                    <input type="email" name="E-mail" placeholder="Email Adress*" required>
                                    <textarea name="Message" placeholder="A Few Words*" required></textarea>
                                    <span class="inputs-description">*Required fields</span>
                                    <button class="btn btn-dark anim-obj">
                                        <span class="btn-caption">Send</span>
                                    </button>
                                </form>
                                <!-- Let's Talk Form End  -->

                            </div>
                            <!-- Form Container End -->

                        </div>
                        <!-- Popup Content Block End -->

                    </div>
                    <div class="col-12 col-xl-6">

                        <!-- Popup Image Block Start -->
                        <div class="popup-image popup-image-2"></div>
                        <!-- Popup Image Block End -->

                    </div>
                </div>
            </div>
        </div>
        <!-- Popup Content End -->

    </div>
</div>
<!-- Let's Talk Popup End -->

<!-- Custom HTML End -->

<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

    <!-- Background of PhotoSwipe.
    It's a separate element, as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
        <!-- don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Share"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div>

    </div>

</div>

<!-- Load Scripts Start-->
<script src="comingsoon/js/libs.min.js"></script>
<script src="comingsoon/js/gallery-init.js"></script>
<script src="comingsoon/js/peachy-custom.js"></script>
<script src="comingsoon/js/maps/google-map.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzp2JuNLNAX0b0LL-MinLjicuHJ7czm-w"></script>
<!-- Load Scripts End-->

</body>

</html>
