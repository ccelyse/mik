@extends('frontend.blog.layouts.master')

@section('title', '5 Trending Shoes You Need In Your Wardrobe This Spring | Made in kigali')

@section('content')

    <div class="group site-container">
        <!-- /.site-header -->
    @include('frontend.blog.layouts.upmenu')
    <!-- /.site-header -->

        <main class="group site-content">
            <div class="inner">

                <div class="primary" role="main">

                    <div class="group post-gallery">
                        <div>
                            <div class="post-gallery__item">
                                <a href="frontend/assets/images/HERO-1.jpg" class="post-gallery__link post-gallery__link--single" data-js-lightbox>
                                    <img class="post-gallery__media" src="frontend/assets/images/375w/HERO-1-270x203.jpg" srcset="frontend/assets/images/375w/HERO-1-270x203.jpg 375w, frontend/assets/images/600w/HERO-1-400x300.jpg 600w, frontend/assets/images/820w/HERO-1-820x615.jpg 820w" alt="" height="615" width="820"></a>
                            </div>

                        </div>
                    </div>

                    <div id="post-67202" class="group content-block post-67202 post type-post status-publish format-standard has-post-thumbnail category-featured category-style category-this-and-that">

                        <a href="#" class="tag entry-tag" title="View all posts in Featured">Featured</a>
                        <h1 class="entry-title">5 Trending Shoes You Need In Your Wardrobe This Spring</h1>

                        <div class="entry-meta">
                            2nd March 2020 </div>

                        <div class="entry-share">
                            <ul class="social-list">
                                <li class="social-list__item">
                                    <a href="#" class="social-icon">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                </li>
                                <li class="social-list__item">
                                    <a href="" class="social-icon">
                                        <i class="fab fa-facebook"></i>
                                    </a>
                                </li>
                                <li class="social-list__item">
                                    <a href="" class="social-icon">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                                <li class="social-list__item">
                                    <a href="" class="social-icon">
                                        <i class="fab fa-youtube"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="entry-content">
                            <p>Spring is less than three weeks away &#8211; so if you&#8217;re planning a spring clean of your wardrobe ahead of the new season, it&#8217;s only right you then replenish it with some new shoes. Luckily, we&#8217;ve already got <em>all</em> the pairs that will be trending once temperatures rise, so you can be fully prepared. From bright blue boots to flip-flop heels, discover the shoes you need and get ready to step into spring on the right foot&#8230;</p>
                            <p><span style="font-weight: 900;">1. All the blues</span></p>
                            <div class="curalate-widget" style="max-width: 1012px; margin: auto; min-width: 240px; min-height: 240px;">
                                <div class="curalate-widget-container" style="position: relative; width: 100%; height: 0; border-radius: 3px; padding-bottom: 75.09881422924902%;">
                                    <iframe class="curalate-widget-iframe" style="z-index: 1; border-radius: 3px; position: absolute; width: 100%; height: 100%; left: 0; top: 0;" title="Curalate Reveal: Interactive image with tagged items" src="https://r.curalate.com/v2/widget/u3vLpObeTtWopOYlRt0Y0TgTOUxukGev?replaceTabs=false&amp;spatialTagIcon=tag&amp;autoShowProducts=false&amp;shopButtonText=View Details" width="1012" height="760" frameborder="0" scrolling="no"></iframe><img class="curalate-widget-image" style="position: relative !important; z-index: 0 !important; width: 100%; border-radius: 3px;" src="https://d28m5bx785ox17.cloudfront.net/v1/img/5paSZjxUxSRxZOuX_ym_KekQjkRy4lmv09sBNfVXyC0=/d/l" alt="" aria-hidden="true" />
                                    <script class="curalate-widget-script" async="" defer="" charset="utf-8" src="https://d30bopbxapq94k.cloudfront.net/js/curalate-widget-client-all-v3.min.js"></script>
                                </div>
                            </div>
                            <p>We&#8217;ve been seeing blue shoes on all the street style stars this fashion month, so take notes. Often styled with more neutral outfits and a matching blue bag, the bold accessory is a must-have in your spring wardrobe.</p>
                            <p><span style="font-weight: 900;">2. Flip flop heels</span></p>
                            <div class="curalate-widget" style="max-width: 1012px; margin: auto; min-width: 240px; min-height: 240px;">
                                <div class="curalate-widget-container" style="position: relative; width: 100%; height: 0; border-radius: 3px; padding-bottom: 75.09881422924902%;">
                                    <iframe class="curalate-widget-iframe" style="z-index: 1; border-radius: 3px; position: absolute; width: 100%; height: 100%; left: 0; top: 0;" title="Curalate Reveal: Interactive image with tagged items" src="https://r.curalate.com/v2/widget/JnNMLsTsv9X28byd7Y8yiwi1xYTE4c7f?replaceTabs=false&amp;spatialTagIcon=tag&amp;autoShowProducts=false&amp;shopButtonText=View Details" width="1012" height="760" frameborder="0" scrolling="no"></iframe><img class="curalate-widget-image" style="position: relative !important; z-index: 0 !important; width: 100%; border-radius: 3px;" src="https://d28m5bx785ox17.cloudfront.net/v1/img/SOpPkiIj2pG-tehLtEDQh1AAPisd2h24ShrzNGvBEF0=/d/l" alt="" aria-hidden="true" />
                                    <script class="curalate-widget-script" async="" defer="" charset="utf-8" src="https://d30bopbxapq94k.cloudfront.net/js/curalate-widget-client-all-v3.min.js"></script>
                                </div>
                            </div>
                            <p>With an added heel and on-trend square toe shape, the humble flip flop has just received the ultimate revamp for 2020. Now no longer a poolside exclusive, it&#8217;ll accompany you to anything from garden parties to brunches and even the office.</p>
                            <p><span style="font-weight: 900;">3. Many mules </span></p>
                            <div class="curalate-widget" style="max-width: 1012px; margin: auto; min-width: 240px; min-height: 240px;">
                                <div class="curalate-widget-container" style="position: relative; width: 100%; height: 0; border-radius: 3px; padding-bottom: 75.09881422924902%;">
                                    <iframe class="curalate-widget-iframe" style="z-index: 1; border-radius: 3px; position: absolute; width: 100%; height: 100%; left: 0; top: 0;" title="Curalate Reveal: Interactive image with tagged items" src="https://r.curalate.com/v2/widget/L7NsYtwgyJByJdCY3dh3ncFd3Lvmoqcz?replaceTabs=false&amp;spatialTagIcon=tag&amp;autoShowProducts=false&amp;shopButtonText=View Details" width="1012" height="760" frameborder="0" scrolling="no"></iframe><img class="curalate-widget-image" style="position: relative !important; z-index: 0 !important; width: 100%; border-radius: 3px;" src="https://d28m5bx785ox17.cloudfront.net/v1/img/Ha39Q_BzAEFj4aC8_RVsLr44xX2NIOUtS03Ck2jdVlc=/d/l" alt="" aria-hidden="true" />
                                    <script class="curalate-widget-script" async="" defer="" charset="utf-8" src="https://d30bopbxapq94k.cloudfront.net/js/curalate-widget-client-all-v3.min.js"></script>
                                </div>
                            </div>
                            <p>You probably already knew about this one, but mules are here to stay. Go for closed-toe options for the office, stiletto styles for the dancefloor and buttermilk shades to match all of your spring wedding looks.</p>
                            <p><span style="font-weight: 900;">4. Princess slippers</span></p>
                            <div class="curalate-widget" style="max-width: 1012px; margin: auto; min-width: 240px; min-height: 240px;">
                                <div class="curalate-widget-container" style="position: relative; width: 100%; height: 0; border-radius: 3px; padding-bottom: 75.09881422924902%;">
                                    <iframe class="curalate-widget-iframe" style="z-index: 1; border-radius: 3px; position: absolute; width: 100%; height: 100%; left: 0; top: 0;" title="Curalate Reveal: Interactive image with tagged items" src="https://r.curalate.com/v2/widget/mkZwbQfxEuhn2dDGZVxDovw7DFQzcBOR?replaceTabs=false&amp;spatialTagIcon=tag&amp;autoShowProducts=false&amp;shopButtonText=View Details" width="1012" height="760" frameborder="0" scrolling="no"></iframe><img class="curalate-widget-image" style="position: relative !important; z-index: 0 !important; width: 100%; border-radius: 3px;" src="https://d28m5bx785ox17.cloudfront.net/v1/img/6_vkOXs0V-8PdbvqbQc9ldri-CRDG2RYFEVePd5wztA=/d/l" alt="" aria-hidden="true" />
                                    <script class="curalate-widget-script" async="" defer="" charset="utf-8" src="https://d30bopbxapq94k.cloudfront.net/js/curalate-widget-client-all-v3.min.js"></script>
                                </div>
                            </div>
                            <p>Always dreamt of owning a pair of shoes that look like princess slippers? This season, it&#8217;ll finally come true as mules featuring ruffles, iridescent finishes and a lot of pink are everywhere right now.</p>
                            <p><span style="font-weight: 900;">5. Lace up </span></p>
                            <div class="curalate-widget" style="max-width: 1012px; margin: auto; min-width: 240px; min-height: 240px;">
                                <div class="curalate-widget-container" style="position: relative; width: 100%; height: 0; border-radius: 3px; padding-bottom: 75.09881422924902%;">
                                    <iframe class="curalate-widget-iframe" style="z-index: 1; border-radius: 3px; position: absolute; width: 100%; height: 100%; left: 0; top: 0;" title="Curalate Reveal: Interactive image with tagged items" src="https://r.curalate.com/v2/widget/pqEtPcvPuMiAFAHldxDUvz1WUrJYgmgV?replaceTabs=false&amp;spatialTagIcon=tag&amp;autoShowProducts=false&amp;shopButtonText=View Details" width="1012" height="760" frameborder="0" scrolling="no"></iframe><img class="curalate-widget-image" style="position: relative !important; z-index: 0 !important; width: 100%; border-radius: 3px;" src="https://d28m5bx785ox17.cloudfront.net/v1/img/ZyiUE3uDnOJR820ln-TDp8R9n4fURIsySy6a8lqHml0=/d/l" alt="" aria-hidden="true" />
                                    <script class="curalate-widget-script" async="" defer="" charset="utf-8" src="https://d30bopbxapq94k.cloudfront.net/js/curalate-widget-client-all-v3.min.js"></script>
                                </div>
                            </div>
                            <p>Lace-up heels never go out of style and we can see why: they don&#8217;t just remind us of ballet shoes, they also make heels feel a little more secure. To make them feel fresh for 2020, simply wrap the ties around the bottom of your trousers.</p>
                            <p>&nbsp;</p>
                            <p><em>Want to know what to wear with your new shoes? Here are<a href="https://www.topshop.com/blog/2020/02/5-ways-to-style-oversized-shirts-for-every-occasion"> 5 Ways To Style Oversized Shirts For Every Occasion&#8230; </a></em></p>
                            <p>&nbsp;</p>
                            <p>Hero Image Credit: <a href="https://www.instagram.com/p/B8zOSaipE0q/?utm_source=ig_web_copy_link">@sineadcrowe </a></p>
                            <p>&nbsp;</p>
                        </div>

                    </div>

                    <div class="group post-navigation">
                        <a href="#" rel="prev" class="post-navigation__button post-navigation__button--previous"><span>Previous Article</span> <span class="visuallyhidden">The Netflix Releases To Watch This March 2020</span></a> <a href="#" rel="next" class="post-navigation__button post-navigation__button--next"><span>Next Article</span> <span class="visuallyhidden">How To Style The Topshop Collection Everyone Is Talking About</span></a> </div>

                    <div class="group mini-posts">
                        <div class="inner">
                            <h2 class="mini-posts__top-title">Recent Posts</h2>
                            <div class="mini-posts__list" data-js-carousel data-js-carousel-items="3">
                                <div class="mini-posts__list-item">
                                    <a href="#" class="tag mini-posts__tag" title="View all posts in Culture">Culture</a>
                                    <a href="#" class="mini-posts__media-link">
                                        <img src="frontend/assets/images/HERO-1-214x157.jpg" alt="How To Approach Spring Cleaning According To Your Star Sign" class="mini-posts__media" height="157" width="214">
                                    </a>
                                    <h3 class="mini-posts__title">
                                    <a href="#">How To Approach Spring Cleaning According To Your Star Sign</a></h3>
                                </div>
                                <div class="mini-posts__list-item">
                                    <a href="#" class="tag mini-posts__tag" title="View all posts in Featured">Featured</a>
                                    <a href="#" class="mini-posts__media-link">
                                    <img src="frontend/assets/images/HERO-1-214x157.jpg" alt="5 Dress Trends That Are So Good You&#8217;ll Want Them All" class="mini-posts__media" height="157" width="214"></a>
                                    <h3 class="mini-posts__title">
                                    <a href="#">5 Dress Trends That Are So Good You&#8217;ll Want Them All</a>
                                    </h3>
                                </div>
                                <div class="mini-posts__list-item">
                                    <a href="#" class="tag mini-posts__tag" title="View all posts in Featured">Featured</a>
                                    <a href="#" class="mini-posts__media-link">
                                    <img src="frontend/assets/images/HERO-1-214x157.jpg" alt="7 Full Outfits You Can Buy On Sale For Under £100" class="mini-posts__media" height="157" width="214"></a>
                                    <h3 class="mini-posts__title">
                                    <a href="#">7 Full Outfits You Can Buy On Sale For Under £100</a>
                                    </h3>
                                </div>
                                <div class="mini-posts__list-item">
                                    <a href="#" class="tag mini-posts__tag" title="View all posts in Culture">Culture</a>
                                    <a href="#" class="mini-posts__media-link">
                                        <img src="frontend/assets/images/HERO-1-214x157.jpg" alt="7 Of The Best Celebrity 21st Birthday Parties Ever" class="mini-posts__media" height="157" width="214"></a>
                                    <h3 class="mini-posts__title">
                                    <a href="#">7 Of The Best Celebrity 21st Birthday Parties Ever</a>
                                    </h3>
                                </div>
                                <div class="mini-posts__list-item">
                                    <a href="#" class="tag mini-posts__tag" title="View all posts in Culture">Culture</a>
                                    <a href="#" class="mini-posts__media-link">
                                    <img src="frontend/assets/images/HERO-1-214x157.jpg" alt="Meet The Women Behind Topshop: Our Most Inspiring Colleagues Talk Female Icons, Advice They&#8217;d Give Their Younger Self And What&#8217;s In Their Snack Drawer" class="mini-posts__media" height="157" width="214"></a>
                                    <h3 class="mini-posts__title">
                                    <a href="#">Meet The Women Behind Topshop: Our Most Inspiring Colleagues Talk Female Icons, Advice They&#8217;d Give Their Younger Self And What&#8217;s In Their Snack Drawer</a>
                                    </h3>
                                </div>
                                <div class="mini-posts__list-item">
                                    <a href="#" class="tag mini-posts__tag" title="View all posts in Featured">Featured</a>
                                    <a href="#" class="mini-posts__media-link">
                                    <img src="frontend/assets/images/HERO-1-214x157.jpg" alt="5 Easy Street Style Looks You Can Actually Recreate And Wear Everyday" class="mini-posts__media" height="157" width="214"></a>
                                    <h3 class="mini-posts__title">
                                    <a href="frontend/assets/images/HERO-1-214x157.jpg">5 Easy Street Style Looks You Can Actually Recreate And Wear Everyday</a>
                                    </h3>
                                </div>
                                <div class="mini-posts__list-item">
                                    <a href="#" class="tag mini-posts__tag" title="View all posts in Featured">Featured</a>
                                    <a href="#" class="mini-posts__media-link">
                                    <img src="frontend/assets/images/HERO-1-214x157.jpg" alt="The New Book You Should Read Based On Your Star Sign" class="mini-posts__media" height="157" width="214">
                                    </a>
                                    <h3 class="mini-posts__title">
                                    <a href="#">The New Book You Should Read Based On Your Star Sign</a>
                                    </h3>
                                </div>
                                <div class="mini-posts__list-item">
                                    <a href="#" class="tag mini-posts__tag" title="View all posts in Style">Style</a>
                                    <a href="#" class="mini-posts__media-link">
                                    <img src="frontend/assets/images/HERO-1-214x157.jpg" alt="5 Simple Ways To Transition Your Winter Wardrobe Pieces To Spring" class="mini-posts__media" height="157" width="214">
                                    </a>
                                    <h3 class="mini-posts__title">
                                    <a href="#">5 Simple Ways To Transition Your Winter Wardrobe Pieces To Spring</a>
                                    </h3>
                                </div>
                                <div class="mini-posts__list-item">
                                    <a href="#" class="tag mini-posts__tag" title="View all posts in Cinema &amp; TV">Cinema &amp; TV</a>
                                    <a href="#" class="mini-posts__media-link">
                                    <img src="frontend/assets/images/HERO-1-214x157.jpg" alt="An Exclusive Look Behind The Scenes Of One Of 2020&#8217;s Most Haunting Films" class="mini-posts__media" height="157" width="214">
                                    </a>
                                    <h3 class="mini-posts__title">
                                    <a href="#">An Exclusive Look Behind The Scenes Of One Of 2020&#8217;s Most Haunting Films</a>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="group ad-block">
                        <a href="https://www.topshop.com/en/tsuk/category/jeans-6877054">
                            <img src="frontend/assets/images/375w/FOOTER_INBETWEEN_1-400x182.jpg" alt="" srcset="frontend/assets/images/375w/FOOTER_INBETWEEN_1-400x182.jpg 400w, frontend/assets/images/820w/FOOTER_INBETWEEN_1.jpg 820w" height="373" width="820">
                        </a>
                    </div>

                </div>

                <div class="secondary" role="complementary">

                    <div class="content-block content-block--sidebar">
                        <h2 class="widget-title">Stay In The Know</h2>
                        <p>Enter your email address to receive the MadeinKigali newsletter</p>
                        <form method="get" class="search" action="//www.topshop.com/webapp/wcs/stores/servlet/CatalogNavigationSearchResultCmd">
                            <fieldset>
                                <input type="hidden" value="12556" name="storeId">
                                <input type="hidden" value="33057" name="catalogId">
                                <input type="hidden" value="-1" name="langId">
                                <input type="hidden" value="false" name="viewAllFlag">
                                <input type="hidden" value="277562" name="categoryId">
                                <input type="hidden" value="true" name="interstitial">
                                <input type="email" class="field" name="EMAIL" placeholder="Your email address">
                                <button type="submit" class="button button--full">Subscribe</button>
                            </fieldset>
                        </form>
                    </div>

                    <div class="widget-block">
                        <h2 class="widget-title"><span class="widget-title__line">About Us</span></h2>
                        <p>Welcome to the MadeinKigali blog – a place to read about everything inspiring us from the latest fashion news to styling tips and our cultural tip-offs.</p>
                    </div>

                    <div class="widget-block">
                        <h2 class="widget-title"><span class="widget-title__line">Follow Us</span></h2>
                        <ul class="social-list">
                            <li class="social-list__item">
                                <a href="https://instagram.com/topshop" class="social-icon">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                            <li class="social-list__item">
                                <a href="" class="social-icon">
                                    <i class="fab fa-facebook"></i>
                                </a>
                            </li>
                            <li class="social-list__item">
                                <a href="" class="social-icon">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li class="social-list__item">
                                <a href="" class="social-icon">
                                    <i class="fab fa-youtube"></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="ad-block ad-block--sidebar">
                        <a href="#"><img src="frontend/assets/images/SIDEBAR_1-327x216.jpg" alt="" height="218" width="330"></a>
                    </div>

                    <div class="widget-block">
                        <h2 class="widget-title"><span class="widget-title__line">Search</span></h2>
                        <form method="get" class="search" action="/blog/">
                            <fieldset>
                                <label for="s609" class="visuallyhidden">Search this site</label>
                                <div class="group search__field">
                                    <input type="search" class="field" name="s" id="s609" placeholder="Search&hellip;">
                                    <button type="submit" name="submit" class="search-button ir">Search</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>

                    <div class="widget-block">
                        <h2 class="widget-title"><span class="widget-title__line">Tags</span></h2>
                        <ul class="inline-list">
                            <li><a href="/blog/beauty" rel="tag" class="tag">Beauty</a></li>
                            <li><a href="/blog/culture" rel="tag" class="tag">Culture</a></li>
                            <li><a href="/blog/featured" rel="tag" class="tag">Featured</a></li>
                            <li><a href="/blog/music" rel="tag" class="tag">Music</a></li>
                            <li><a href="/blog/news" rel="tag" class="tag">News</a></li>
                            <li><a href="/blog/style" rel="tag" class="tag">Style</a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </main>
        <!--/ .site-content -->

        <!-- Site Footer -->
    @include('frontend.blog.layouts.footer')
    <!--/ .site-footer -->

    </div>

@endsection
