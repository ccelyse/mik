<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <script>
        document.documentElement.className = document.documentElement.className.replace(/\bno-js\b/, 'js');
    </script>
    <!--[if lt IE 9]>
    <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script> <![endif]-->
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <meta name="viewport" content="initial-scale=1.0001">
    <link rel="stylesheet" href="frontend/assets/css/style.min.css">
    <link href="frontend/assets/css/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <meta name="description" content="" />
    <link rel="shortcut icon" href="frontend/assets/images/favicon.jpeg">
    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
    <meta property="og:locale" content="en_GB" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Made in Kigali Blog" />
    <meta property="og:description" content="" />
    <meta property="og:site_name" content="Made in Kigali Blog" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="" />
    <meta name="twitter:title" content="Made in Kigali Blog" />


    <link rel='dns-prefetch' href='https://ajax.googleapis.com' />
    <link rel='dns-prefetch' href='https://s.w.org' />
    <link rel='stylesheet' id='wp-block-library-css' href='frontend/assets/css/dist/block-library/style.min.css' type='text/css' media='all' />
    <style>
        form .message-textarea {
            display: none !important;
        }
    </style>
</head>
<body>

@yield('content')

<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js'></script>
<script type='text/javascript' src='frontend/assets/js/topshop-blog-2015.min.js'></script>
<script type='text/javascript' src='frontend/assets/js/wp-embed.min.js'></script>

<script type="text/javascript">
    var _cf = _cf || [];
    _cf.push(['_setFsp', true]);
    _cf.push(['_setBm', true]);
    _cf.push(['_setAu', '/public/899a2505c171961ec32004636e4c1']);
</script>
<script type="text/javascript" src="../public/899a2505c171961ec32004636e4c1"></script>
</body>

</html>
