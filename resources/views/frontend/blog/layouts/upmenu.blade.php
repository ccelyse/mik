{{--<div class="group notice-bar">--}}
{{--    <img src="frontend/assets/images/BLOGHEADER.png" style="width: 100%;">--}}
{{--</div>--}}
<div class="group notice-bar">
    <div class="inner">
        <a href="#"><span class="top_bar_reg">Register now to get </span>10% off <span class="top_bar_reg">your first order with code</span> <b>NEWCUSTOMER10</b></a>
    </div>
</div>
<header class="group site-header test">
    <div class="inner">

        <div role="banner" class="site-header__section site-header__section--branding">
            <h1 class="site-header__logo">
                <a href="{{'/'}}" title="Made in Kigali">
                    <img src="frontend/assets/images/LOGOO.png" style="width: 90px;padding-top: 10px;">
                    <span class="visuallyhidden test">Made in Kigali</span></a>
            </h1>
        </div>
        <!-- /logo -->

        <a href="{{'/'}}" class="site-nav-button" data-js-toggle-nav title="Show/hide menu">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 28" width="14px" height="12.25px">
                <style type="text/css">
                    .menu-line {
                        fill: none;
                        stroke: #000000;
                        stroke-width: 4;
                        stroke-linecap: round;
                        stroke-linejoin: round;
                    }
                </style>
                <line class="menu-line" x1="2" y1="26" x2="30" y2="26" />
                <line class="menu-line" x1="2" y1="14" x2="30" y2="14" />
                <line class="menu-line" x1="30" y1="2" x2="2" y2="2" />
            </svg>
            <span>Menu</span></a>

        <nav class="site-header__section site-header__section--nav">

            <a href="{{'/'}}" class="site-nav-button-close" data-js-toggle-nav title="Hide menu">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" height="20px" width="20px" aria-label="Close" fill="white">
                    <title>Close</title>
                    <path d="M18.8,16L31.4,3.4c0.8-0.8,0.8-2,0-2.8c-0.8-0.8-2-0.8-2.8,0L16,13.2L3.4,0.6c-0.8-0.8-2-0.8-2.8,0c-0.8,0.8-0.8,2,0,2.8 L13.2,16L0.6,28.6c-0.8,0.8-0.8,2,0,2.8C1,31.8,1.5,32,2,32c0.5,0,1-0.2,1.4-0.6L16,18.8l12.6,12.6C29,31.8,29.5,32,30,32 s1-0.2,1.4-0.6c0.8-0.8,0.8-2,0-2.8L18.8,16z" />
                </svg>
            </a>

            <ul id="menu-primary" class="site-nav">
                <li id="menu-item-28803" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-28803 site-nav__item"><a href="#" aria-current="page" class="site-nav__link">Fashion</a></li>
                <li id="menu-item-28579" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-28579 site-nav__item"><a href="#" class="site-nav__link">Illustration</a></li>
                <li id="menu-item-28573" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-28573 site-nav__item"><a href="#" class="site-nav__link">Beauty</a></li>
                <li id="menu-item-28580" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-28580 site-nav__item"><a href="#" class="site-nav__link">Culture</a></li>
                <li id="menu-item-28578" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-28578 site-nav__item"><a href="#" class="site-nav__link">Beauty</a></li>
                <li id="menu-item-53629" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-53629 site-nav__item"><a href="#" class="site-nav__link">Shop Madeinkigali</a></li>
            </ul>
            <div class="site-nav__search">
                <a href="{{'/'}}" class="site-nav__search-button" data-js-toggle-search>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 45 45" height="19.5px" width="19.5px" aria-label="Search">
                        <title>Search</title>
                        <path d="M43.1,39.9l-8.3-8.3c2.6-3.3,4.2-7.5,4.2-12.1C39,8.7,30.3,0,19.5,0S0,8.7,0,19.5S8.7,39,19.5,39 c4,0,7.8-1.2,10.9-3.3l8.5,8.5c0.6,0.6,1.4,0.9,2.1,0.9c0.8,0,1.5-0.3,2.1-0.9C44.3,43,44.3,41.1,43.1,39.9z M4,19.5 C4,11,11,4,19.5,4C28,4,35,11,35,19.5c0,4.3-1.7,8.1-4.5,10.9c-0.2,0.1-0.4,0.3-0.6,0.5c-0.1,0.1-0.2,0.3-0.3,0.4 c-2.7,2.3-6.2,3.7-10.1,3.7C11,35,4,28,4,19.5z" />
                    </svg>
                </a>
                <form method="get" class="search" action="/blog/">
                    <fieldset>
                        <label for="s801" class="visuallyhidden">Search this site</label>
                        <div class="group search__field">
                            <input type="search" class="field" name="s" id="s801" placeholder="Search&hellip;">
                            <button type="submit" name="submit" class="search-button ir">Search</button>
                        </div>
                    </fieldset>
                </form>
            </div>

        </nav>
        <!-- /nav -->

    </div>

</header>
