<footer class="group site-footer">
    <div class="group site-footer__legal-links">
        <div class="inner">

            <ul id="menu-legal-links" class="site-footer__nav">
                <li id="menu-item-28806" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28806 site-footer__nav__item"><a href="#" class="site-footer__nav__link">Help</a></li>
                <li id="menu-item-50694" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-50694 site-footer__nav__item"><a href="#" class="site-footer__nav__link">Delivery & Returns</a></li>
                <li id="menu-item-28807" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-28807 site-footer__nav__item"><a href="#" class="site-footer__nav__link">Card & Offers</a></li>
                <li id="menu-item-28808" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-28808 site-footer__nav__item"><a href="#" class="site-footer__nav__link">Company</a></li>
                <li id="menu-item-28808" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-28808 site-footer__nav__item"><a href="#" class="site-footer__nav__link">Contact</a></li>
            </ul>
        </div>
        <!--/ .inner -->
    </div>
    <div class="group site-footer__navigation">
        <div class="inner">
            <div class="site-footer__social">
                {{--                <h2 class="site-footer__social__title">Madeinkigali Social</h2>--}}
                <ul class="social-list">
                    <li class="social-list__item">
                        <a href="#" class="social-icon">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>
                    <li class="social-list__item">
                        <a href="#" class="social-icon">
                            <i class="fab fa-facebook"></i>
                        </a>
                    </li>
                    <li class="social-list__item">
                        <a href="#" class="social-icon">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                    <li class="social-list__item social-list__item--hide-on-mobile social-list__item--hide-in-sidebar">
                        <a href="#" class="social-icon">
                            <i class="fab fa-youtube"></i>
                        </a>
                    </li>

                </ul>
            </div>
            {{--            <div class="site-footer__navigation__column-one" style="">--}}
            {{--                <h2 class="site-footer__navigation__title"><a href="#">Go To Madeinkigali.com</a></h2>--}}

            {{--                <ul id="menu-topshop-com-links" class="topshop-nav">--}}
            {{--                    <li id="menu-item-28560" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-28560 topshop-nav__item"><a href="#" class="topshop-nav__link">New In</a></li>--}}
            {{--                    <li id="menu-item-28561" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-28561 topshop-nav__item"><a href="#" class="topshop-nav__link">Clothing</a></li>--}}
            {{--                    <li id="menu-item-28562" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-28562 topshop-nav__item"><a href="#" class="topshop-nav__link">Shoes</a></li>--}}
            {{--                    <li id="menu-item-28563" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-28563 topshop-nav__item"><a href="#" class="topshop-nav__link">Bags &#038; Accessories</a></li>--}}
            {{--                    <li id="menu-item-28564" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-28564 topshop-nav__item"><a href="#" class="topshop-nav__link">Beauty</a></li>--}}
            {{--                    <li id="menu-item-28565" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-28565 topshop-nav__item"><a href="#" class="topshop-nav__link">Magazine</a></li>--}}
            {{--                </ul>--}}
            {{--                <div class="site-footer__social">--}}
            {{--                    <h2 class="site-footer__social__title">Madeinkigali Social</h2>--}}
            {{--                    <ul class="social-list">--}}
            {{--                        <li class="social-list__item">--}}
            {{--                            <a href="#" class="social-icon">--}}
            {{--                                <i class="fab fa-instagram"></i>--}}
            {{--                            </a>--}}
            {{--                        </li>--}}
            {{--                        <li class="social-list__item">--}}
            {{--                            <a href="#" class="social-icon">--}}
            {{--                                <i class="fab fa-facebook"></i>--}}
            {{--                            </a>--}}
            {{--                        </li>--}}
            {{--                        <li class="social-list__item">--}}
            {{--                            <a href="#" class="social-icon">--}}
            {{--                                <i class="fab fa-twitter"></i>--}}
            {{--                            </a>--}}
            {{--                        </li>--}}
            {{--                        <li class="social-list__item social-list__item--hide-on-mobile social-list__item--hide-in-sidebar">--}}
            {{--                            <a href="#" class="social-icon">--}}
            {{--                                <i class="fab fa-youtube"></i>--}}
            {{--                            </a>--}}
            {{--                        </li>--}}

            {{--                    </ul>--}}
            {{--                </div>--}}

            {{--            </div>--}}
            {{--            <div class="site-footer__navigation__column-two">--}}

            {{--                <div class="topshop-apps" style="">--}}


            {{--                    <div class="topshop-apps__store-locator">--}}
            {{--                        <a href="#">--}}
            {{--                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 38 49.1" width="25px" height="36px">--}}
            {{--                                <path d="M19,0C8.5,0,0,8.3,0,18.8C0,18.9,0,19,0,19h0c0,3,0.5,4.7,1.3,6.6c0.6,1.6,1.4,3.2,2.4,4.6c1.4,2,3,4.1,4.7,6.2 C13.5,42.7,19,49.1,19,49.1s5.5-6.4,10.6-12.7c1.7-2.1,3.3-4.1,4.7-6.1c1-1.3,1.8-3.1,2.4-4.7C37.5,23.7,38,22,38,19h0 c0,0,0,0,0-0.2C38,8.4,29.5,0,19,0z M19,25.1c-3.3,0-6-2.7-6-6s2.7-6,6-6c3.3,0,6,2.7,6,6S22.3,25.1,19,25.1z" />--}}
            {{--                            </svg>--}}
            {{--                            <span class="topshop-apps__supporting">Find Your <br>Nearest Store</span></a>--}}
            {{--                    </div>--}}

            {{--                    <div class="topshop-apps__newsletter">--}}
            {{--                        <a href="#">--}}
            {{--                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 99 75" width="46px" height="35px">--}}
            {{--                                <style type="text/css">--}}
            {{--                                    .svg-n-0 {--}}
            {{--                                        fill: none;--}}
            {{--                                        stroke: #221F1F;--}}
            {{--                                        stroke-width: 4;--}}
            {{--                                        stroke-linecap: round;--}}
            {{--                                        stroke-linejoin: round;--}}
            {{--                                    }--}}

            {{--                                    .svg-n-1 {--}}
            {{--                                        fill: #221F1F;--}}
            {{--                                    }--}}

            {{--                                    .svg-n-2 {--}}
            {{--                                        fill: #FFFFFF;--}}
            {{--                                    }--}}
            {{--                                </style>--}}
            {{--                                <g>--}}
            {{--                                    <rect x="2.5" y="15" class="svg-n-0" width="82" height="58" />--}}
            {{--                                </g>--}}
            {{--                                <polyline class="svg-n-0" points="5.1,15 43.5,53.4 81.9,15 " />--}}
            {{--                                <line class="svg-n-0" x1="54.2" y1="44.6" x2="84.1" y2="72.7" />--}}
            {{--                                <line class="svg-n-0" x1="2.9" y1="72.7" x2="32.8" y2="44.6" />--}}
            {{--                                <circle class="svg-n-1" cx="84" cy="14.5" r="14.5" />--}}
            {{--                                <g>--}}
            {{--                                    <path class="svg-n-2" d="M83.7,21.2h-1.9l4.3-10.9h-5.3V8.7h7.3V10L83.7,21.2z" />--}}
            {{--                                </g>--}}
            {{--                            </svg>--}}
            {{--                            <span class="topshop-apps__supporting">Stay In<br> The Know</span></a>--}}
            {{--                    </div>--}}

            {{--                </div>--}}
            {{--            </div>--}}

        </div>
        <!--/ .inner -->
    </div>
</footer>
