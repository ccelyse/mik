<div class="group categoria_distribucion">
    <div class="imagen">
        <img src="https://corporate.desigual.com/wp-content/themes/desigual/imgs/bolso.png" alt="">
    </div>
    <div class="data">
        <div class="from-top-move">
            <div class="num animate-num-slow" data-numero="6">6</div>
        </div>

        <div class="data-label label-pop1">
            PRODUCT CATEGORIES            </div>
        <div class="pop pop1">
            Our 6 product categories
            <ul>
                <li>Woman</li>
                <li>Shoes</li>
                <li>Accesories</li>
                <li>Sports</li>
                <li>Man</li>
                <li>Kids</li>
            </ul>
        </div>
    </div>

    <div class="data">
        <div class="from-top-move">
            <div class="num animate-num-slow" data-numero="10">10</div>
        </div>

        <div class="data-label label-pop2">
            DISTRIBUTION CHANNELS            </div>
        <div class="pop pop2">
            Our 10 distribution channels:
            <ul>
                <li>Retail Season</li>
                <li>Retail Outlet</li>
                <li>Franchises</li>
                <li>D-Shops</li>
                <li>Flash Sales</li>
                <li>Desigual.com</li>
                <li>E-Tailers</li>
                <li>Department Stores</li>
                <li>Wholesale (including Shoes and Sport)</li>
                <li>Travel Retail</li>
            </ul>


        </div>
    </div>
</div>
