@extends('frontend.blog.layouts.master')

@section('title', 'Made in kigali')

@section('content')

    <div class="group site-container">
        <!-- /.site-header -->
        @include('frontend.blog.layouts.upmenu')
        <!-- /.site-header -->

        <main class="group site-content">
            <div class="inner">

                <div class="group hero-slider" data-js-carousel data-js-carousel-autoplay="5000">
                    <div class="item hero-slider__slide">
                        <a href="{{'BlogPost'}}">
                            <picture data-js-slide>
                                <!--[if IE 9]><video style="display: none;"><![endif]-->
                                <source media="(min-width: 600px)" srcset="frontend/assets/images/slider/SLIDER.jpg">
                                <source srcset="frontend/assets/images/placeholder/slider.png">
                                <!--[if IE 9]></video><![endif]-->
                                <img srcset="frontend/assets/images/placeholder/slider.png" alt="" class="slide__media" width="1200" height="546">
                            </picture>
                        </a>

                        <div class="slide__supporting">
                            <h2 class="slide__title">The Netflix Releases To Watch This March 2020</h2>
                            <div class="slide__copy">
                                <p class="slide__more">
                                    <a href="{{'BlogPost'}}"
                                       class="button button--read-more">Continue Reading
                                        <span class="visuallyhidden">The Netflix Releases To Watch This March 2020</span>
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="item hero-slider__slide">
                        <a href="#">
                            <picture data-js-slide>
                                <!--[if IE 9]><video style="display: none;"><![endif]-->
                                <source media="(min-width: 600px)" srcset="frontend/assets/images/slider/IDOL.jpg">
                                <source srcset="frontend/assets/images/placeholder/slider.png">
                                <!--[if IE 9]></video><![endif]-->
                                <img srcset="frontend/assets/images/placeholder/slider.png" alt="" class="slide__media" width="1200" height="546">
                            </picture>
                        </a>

                        <div class="slide__supporting">
                            <h2 class="slide__title">Behind The Scenes: Get The First Look At MadeinKigali’s IDOL Spring Summer 2020 Collection</h2>
                            <div class="slide__copy">
                                <p class="slide__more">
                                    <a href="{{'BlogPost'}}" class="button button--read-more">Continue Reading<span class="visuallyhidden">
                                            Behind The Scenes: Get The First Look At MadeinKigali’s IDOL Spring Summer 2020 Collection
                                        </span>
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="item hero-slider__slide">
                        <a href="#">
                            <picture data-js-slide>
                                <!--[if IE 9]><video style="display: none;"><![endif]-->
                                <source media="(min-width: 600px)" srcset="frontend/assets/images/slider/NYC.jpg">
                                <source srcset="frontend/assets/images/placeholder/slider.png">
                                <!--[if IE 9]></video><![endif]-->
                                <img srcset="frontend/assets/images/placeholder/slider.png" alt="" class="slide__media" width="1200" height="546">
                            </picture>
                        </a>

                        <div class="slide__supporting">
                            <h2 class="slide__title">The Coolest Places To Eat, Sleep And Hang Out In New York This 2020</h2>
                            <div class="slide__copy">
                                <p class="slide__more">
                                    <a href="{{'BlogPost'}}" class="button button--read-more">Continue Reading<span class="visuallyhidden">
                                            The Coolest Places To Eat, Sleep And Hang Out In New York This 2020
                                        </span>
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="primary" role="main">

                    <div class="group post-gallery">

                        <div class="post-gallery__item">
                            <a href="#" class="post-gallery__link">
                                <img class="post-gallery__media" src="frontend/assets/images/375w/HERO-3-270x203.jpg" srcset="frontend/assets/images/375w/HERO-3-400x300.jpg 375w, frontend/assets/images/600w/HERO-3-400x300.jpg 600w, frontend/assets/images/820w/HERO-3-820x615.jpg 820w" alt="" height="615" width="820">
                            </a>
                        </div>
                    </div>

                    <div id="post-67249" class="group content-block post-67249 post type-post status-publish format-standard has-post-thumbnail category-cinema-tv category-featured">

                        <a href="#" class="tag entry-tag" title="View all posts in Cinema &amp; TV">Cinema &amp; TV</a>
                        <h2 class="entry-title">
                            <a href="{{'BlogPost'}}" title="" rel="bookmark">
                                An Exclusive Look Behind The Scenes Of One Of 2020&#8217;s Most Haunting Films
                            </a>
                        </h2>

                        <div class="entry-meta">
                            4th March 2020 </div>

                        <div class="entry-content">
                            <p>A plant that has the power to make you happy? In Little Joe, scientist Alice Woodard (Emily Beecham) seems to have created just that &#8211; but she quickly discovered that it also bears an unpredictable threat&#8230; What makes this whole scenario that extra bit eerie are the bold colours, the haunting cinematography and the unusually &hellip;</p>
                            <p><a href="{{'BlogPost'}}" class="button button--read-more">Continue reading <span class="visuallyhidden">
                                        An Exclusive Look Behind The Scenes Of One Of 2020&#8217;s Most Haunting Films
                                    </span>
                                </a>
                            </p>
                        </div>

                    </div>

                    <div class="group mini-posts">
                        <div class="inner">
                            <h2 class="mini-posts__top-title">Trending Stories</h2>
                            <div class="mini-posts__list" data-js-carousel data-js-carousel-items="3">
                                <div class="mini-posts__list-item">
                                    <a href="#" class="tag mini-posts__tag" title="View all posts in Cinema &amp; TV">Cinema &amp; TV</a>
                                    <a href={{'BlogPost'}}#" class="mini-posts__media-link">
                                        <img src="frontend/assets/images/HERO-1-214x157.jpg" alt="The Netflix Releases To Watch This January 2020" class="mini-posts__media" height="157" width="214"></a>
                                    <h3 class="mini-posts__title">
                                        <a href="{{'BlogPost'}}">
                                            The Netflix Releases To Watch This January 2020</a>
                                    </h3>
                                </div>
                                <div class="mini-posts__list-item">
                                    <a href="#" class="tag mini-posts__tag" title="View all posts in Featured">Featured</a>
                                    <a href="{{'BlogPost'}}" class="mini-posts__media-link">
                                        <img src="frontend/assets/images/1012X760PX-209x157.jpg" alt="5 Simple Things To Do To Start The New Year In The Right Way" class="mini-posts__media" height="157" width="214"></a>
                                    <h3 class="mini-posts__title">
                                        <a href="{{'BlogPost'}}">
                                            5 Simple Things To Do To Start The New Year In The Right Way
                                        </a>
                                    </h3>
                                </div>
                                <div class="mini-posts__list-item">
                                    <a href="#" class="tag mini-posts__tag" title="View all posts in Award Season">Award Season</a>
                                    <a href="{{'BlogPost'}}" class="mini-posts__media-link">
                                        <img src="frontend/assets/images/0HERO-209x157.jpg" alt="How, When And Where To Watch The Oscar Nominated Films Of 2020" class="mini-posts__media" height="157" width="214"></a>
                                    <h3 class="mini-posts__title">
                                        <a href="{{'BlogPost'}}">
                                            How, When And Where To Watch The Oscar Nominated Films Of 2020
                                        </a>
                                    </h3>
                                </div>
                                <div class="mini-posts__list-item">
                                    <a href="#" class="tag mini-posts__tag" title="View all posts in Featured">Featured</a>
                                    <a href="{{'BlogPost'}}" class="mini-posts__media-link">
                                        <img src="frontend/assets/images/HERO-1-214x157.jpg" alt="5 Pieces You Need To Nail The Off-Duty Street Style Look" class="mini-posts__media" height="157" width="214"></a>
                                    <h3 class="mini-posts__title">
                                        <a href="{{'BlogPost'}}">
                                            5 Pieces You Need To Nail The Off-Duty Street Style Look
                                        </a>
                                    </h3>
                                </div>
                                <div class="mini-posts__list-item">
                                    <a href="#" class="tag mini-posts__tag" title="View all posts in City Guide">City Guide</a>
                                    <a href="{{'BlogPost'}}" class="mini-posts__media-link">
                                        <img src="frontend/assets/images/hero2-214x157.jpg" alt="13 Cultural Events To Look Forward To In 2020 If You Love Fashion" class="mini-posts__media" height="157" width="214"></a>
                                    <h3 class="mini-posts__title">
                                        <a href="{{'BlogPost'}}">
                                            13 Cultural Events To Look Forward To In 2020 If You Love Fashion
                                        </a>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="group post-gallery">

                        <div class="post-gallery__item">
                            <a href="#" class="post-gallery__link">
                                <img class="post-gallery__media" src="frontend/assets/images/375w/HERO-3-270x203.jpg" srcset="frontend/assets/images/375w/HERO-3-400x300.jpg 375w, frontend/assets/images/600w/HERO-3-400x300.jpg 600w, frontend/assets/images/820w/HERO-3-820x615.jpg 820w" alt="" height="615" width="820">
                            </a>
                        </div>
                    </div>

                    <div id="post-67261" class="group content-block post-67261 post type-post status-publish format-standard has-post-thumbnail category-featured category-style category-this-and-that">

                        <a href="#" class="tag entry-tag" title="View all posts in Featured">Featured</a>
                        <h2 class="entry-title">
                            <a href="{{'BlogPost'}}" title="" rel="bookmark">How To Style The MadeinKigali Collection Everyone Is Talking About
                            </a>
                        </h2>

                        <div class="entry-meta">
                            3rd March 2020
                        </div>

                        <div class="entry-content">
                            <p>If you hadn&#8217;t already heard, MadeinKigali IDOL just returned with a brand-new drop for Spring Summer 2020 and it&#8217;s the collection everyone wants to get their hands on. Inspired by the 1996 film Romeo and Juliet, it focuses on sheer fabrics, intricate prints and a lighter colour palette whilst still maintaining IDOL&#8217;s signature rock and &hellip;</p>
                            <p>
                                <a href="{{'BlogPost'}}" class="button button--read-more">Continue reading <span class="visuallyhidden">
                                        How To Style The Topshop Collection Everyone Is Talking About
                                    </span>
                                </a>
                            </p>
                        </div>

                    </div>

                    <div class="group post-gallery">

                        <div class="post-gallery__item">
                            <a href="#" class="post-gallery__link">
                                <img class="post-gallery__media" src="frontend/assets/images/375w/HERO-3-270x203.jpg" srcset="frontend/assets/images/375w/HERO-3-400x300.jpg 375w, frontend/assets/images/600w/HERO-3-400x300.jpg 600w, frontend/assets/images/820w/HERO-3-820x615.jpg 820w" alt="" height="615" width="820"></a>
                        </div>
                    </div>

                    <div id="post-67202" class="group content-block post-67202 post type-post status-publish format-standard has-post-thumbnail category-featured category-style category-this-and-that">

                        <a href="#" class="tag entry-tag" title="View all posts in Featured">Featured</a>
                        <h2 class="entry-title"><a href="{{'BlogPost'}}" title="" rel="bookmark">5 Trending Shoes You Need In Your Wardrobe This Spring</a></h2>

                        <div class="entry-meta">
                            2nd March 2020 </div>

                        <div class="entry-content">
                            <p>Spring is less than three weeks away &#8211; so if you&#8217;re planning a spring clean of your wardrobe ahead of the new season, it&#8217;s only right you then replenish it with some new shoes. Luckily, we&#8217;ve already got all the pairs that will be trending once temperatures rise, so you can be fully prepared. From &hellip;</p>
                            <p><a href="{{'BlogPost'}}" class="button button--read-more">Continue reading <span class="visuallyhidden">5 Trending Shoes You Need In Your Wardrobe This Spring</span></a></p>
                        </div>

                    </div>

                    <div class="group ad-block">
                        <a href="https://www.topshop.com/en/tsuk/category/jeans-6877054">
                            <img src="frontend/assets/images/375w/FOOTER_INBETWEEN_1-400x182.jpg" alt="" srcset="frontend/assets/images/375w/FOOTER_INBETWEEN_1-400x182.jpg 400w, frontend/assets/images/820w/FOOTER_INBETWEEN_1.jpg 820w" height="373" width="820">
                        </a>
                    </div>

                    <div class="group post-gallery">

                        <div class="post-gallery__item">
                            <a href="#" class="post-gallery__link">
                                <img class="post-gallery__media" src="frontend/assets/images/375w/HERO-3-270x203.jpg" srcset="frontend/assets/images/375w/HERO-3-400x300.jpg 375w, frontend/assets/images/600w/HERO-3-400x300.jpg 600w, frontend/assets/images/820w/HERO-3-820x615.jpg 820w" alt="" height="615" width="820"></a>
                        </div>
                    </div>

                    <div id="post-67204" class="group content-block post-67204 post type-post status-publish format-standard has-post-thumbnail category-cinema-tv category-featured">

                        <a href="#" class="tag entry-tag" title="View all posts in Cinema &amp; TV">Cinema &amp; TV</a>
                        <h2 class="entry-title"><a href="{{'BlogPost'}}" title="" rel="bookmark">The Netflix Releases To Watch This March 2020</a></h2>

                        <div class="entry-meta">
                        </div>

                        <div class="entry-content">
                            <p>While the weather is still being as unpredictable as it is right now, having a whole host of Netflix shows lined up to watch on a day stuck indoors is essential. Luckily, this March 2020 is so packed with new releases, you&#8217;ll never run out of new content to tune into. Want to know what&#8217;s &hellip;</p>
                            <p><a href="{{'BlogPost'}}" class="button button--read-more">Continue reading <span class="visuallyhidden">The Netflix Releases To Watch This March 2020</span></a></p>
                        </div>

                    </div>

                    <div class="group post-gallery">

                        <div class="post-gallery__item">
                            <a href="#" class="post-gallery__link">
                                <img class="post-gallery__media" src="frontend/assets/images/375w/HERO-3-270x203.jpg" srcset="frontend/assets/images/375w/HERO-3-400x300.jpg 375w, frontend/assets/images/600w/HERO-3-400x300.jpg 600w, frontend/assets/images/820w/HERO-3-820x615.jpg 820w" alt="" height="615" width="820"></a>
                        </div>
                    </div>

                    <div id="post-66952" class="group content-block post-66952 post type-post status-publish format-standard has-post-thumbnail category-featured category-style category-this-and-that">

                        <a href="#" class="tag entry-tag" title="View all posts in Featured">Featured</a>
                        <h2 class="entry-title">
                            <a href="{{'BlogPost'}}" title="" rel="bookmark">
                                Behind The Scenes: Get The First Look At MadeinKigali’s IDOL Spring Summer 2020 Collection
                            </a>
                        </h2>

                        <div class="entry-meta">
                            29th February 2020 </div>

                        <div class="entry-content">
                            <p>IDOL, Topshop&#8217;s most in-demand collection, is back with a capsule to kick-start your wardrobe for Spring Summer 2020. Defined by IDOL’s signature rock and roll edge, the collection focusses on high-low dressing, fresh colourways and exciting unique prints. Want to know more? We caught up with our Design Director Anthony Cuthbertson to find out more &hellip;</p>
                            <p><a href="{{'BlogPost'}}" class="button button--read-more">Continue reading <span class="visuallyhidden">Behind The Scenes: Get The First Look At MadeinKigali’s IDOL Spring Summer 2020 Collection</span></a></p>
                        </div>

                    </div>

                    <div class="post-navigation">
                        <a href="#" class="post-navigation__button post-navigation__button--next">Older Articles</a> </div>
                    <div class="group ad-block">
                        <a href="#"><img src="images/2020/02/FOOTER_INBETWEEN_2-400x182.jpg" alt="" srcset="/blog/images/2020/02/FOOTER_INBETWEEN_2-400x182.jpg 400w, /blog/images/2020/02/FOOTER_INBETWEEN_2.jpg 820w" height="373" width="820"></a>
                    </div>
                </div>

                <div class="secondary" role="complementary">

                    <div class="content-block content-block--sidebar">
                        <h2 class="widget-title">Stay In The Know</h2>
                        <p>Enter your email address to receive the MadeinKigali newsletter</p>
                        <form method="get" class="search" action="#">
                            <fieldset>
                                <input type="hidden" value="12556" name="storeId">
                                <input type="hidden" value="33057" name="catalogId">
                                <input type="hidden" value="-1" name="langId">
                                <input type="hidden" value="false" name="viewAllFlag">
                                <input type="hidden" value="277562" name="categoryId">
                                <input type="hidden" value="true" name="interstitial">
                                <input type="email" class="field" name="EMAIL" placeholder="Your email address">
                                <button type="submit" class="button button--full">Subscribe</button>
                            </fieldset>
                        </form>
                    </div>

                    <div class="widget-block">
                        <h2 class="widget-title"><span class="widget-title__line">About Us</span></h2>
                        <p>Welcome to the Made in Kigali blog – a place to read about everything inspiring us from the latest fashion news to styling tips and our cultural tip-offs.</p>
                    </div>

                    <div class="widget-block">
                        <h2 class="widget-title"><span class="widget-title__line">Follow Us</span></h2>
                        <ul class="social-list">
                            <li class="social-list__item">
                                <a href="" class="social-icon">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                            <li class="social-list__item">
                                <a href="" class="social-icon">
                                    <i class="fab fa-facebook"></i>
                                </a>
                            </li>
                            <li class="social-list__item">
                                <a href="" class="social-icon">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li class="social-list__item">
                                <a href="" class="social-icon">
                                    <i class="fab fa-youtube"></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="ad-block ad-block--sidebar">
                        <a href="#"><img src="frontend/assets/images/SIDEBAR_1-327x216.jpg" alt="" height="218" width="330"></a>
                    </div>

                    <div class="widget-block">
                        <h2 class="widget-title"><span class="widget-title__line">Search</span></h2>
                        <form method="get" class="search" action="/blog/">
                            <fieldset>
                                <label for="s359" class="visuallyhidden">Search this site</label>
                                <div class="group search__field">
                                    <input type="search" class="field" name="s" id="s359" placeholder="Search&hellip;">
                                    <button type="submit" name="submit" class="search-button ir">Search</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>

                    <div class="widget-block">
                        <h2 class="widget-title"><span class="widget-title__line">Tags</span></h2>
                        <ul class="inline-list">
                            <li><a href="#" rel="tag" class="tag">Beauty</a></li>
                            <li><a href="#" rel="tag" class="tag">Culture</a></li>
                            <li><a href="#" rel="tag" class="tag">Featured</a></li>
                            <li><a href="#" rel="tag" class="tag">Music</a></li>
                            <li><a href="#" rel="tag" class="tag">News</a></li>
                            <li><a href="#" rel="tag" class="tag">Style</a></li>
                        </ul>
                    </div>

                </div>
            </div>
            <!--/ .inner -->

        </main>
        <!--/ .site-content -->

        <!-- Site Footer -->
    @include('frontend.blog.layouts.footer')
        <!--/ .site-footer -->

    </div>

@endsection
