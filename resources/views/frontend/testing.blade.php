<!DOCTYPE html>
<html lang="en" xml:lang="en" xmlns="https://www.w3.org/1999/xhtml">

<head>
    <!-- PMISEUW2WB15 -->
    <title>
        Made in kigali
    </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="shortcut icon" type="image/x-icon" href="~/media/70D6BBBB9CA24868BDFF16ADAE303A09.ico" />
    <!-- MetaFields -->

    <meta name="description" content="WE ARE AUTHENTIC, BRAVE AND CREATIVE TO OUR CORE" />
    <meta name="google-site-verification" content="4BN95Yi0GGB__9aEjpCCklVxDRtoz-1XbFe75yIeUHs" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <meta property="og:type" content="Website" />
    <meta property="og:url" content="https://www.asosplc.com/" />
    <meta property="og:title" content="Home" />
    <meta property="og:image" content="https://www.asosplc.com/~/media/Images/A/Asos-V2/logo/asos-plc-square-logo.jpg" />
    <link rel="canonical" href="index.html" />

    <script type="text/javascript" language="javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" language="javascript">
        $j = jQuery.noConflict();
        autoCompleteDomain = 'www.asosplc.com';
    </script>
    <script type="text/javascript" language="javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.0/jquery-ui.min.js"></script>
    <!-- Stylesheets -->

    <link href="frontend/assets/stylesheets/fonts-revision=78c97a34-c68c-4e9a-ae8e-465d3bb900df.css" rel="stylesheet" type="text/css" media="All" />
    <link href="frontend/assets/stylesheets/bootstrap-revision=40ff2d83-5bd7-42ce-9ec7-f852fbb2a8bc.css" rel="stylesheet" type="text/css" media="All" />
    <link href="frontend/assets/stylesheets/main-revision=6a41621f-fe95-4fbb-88c4-94440e928b10.css" rel="stylesheet" type="text/css" media="All" />
    <link href="frontend/assets/stylesheets/careers-revision=94e6c96d-288f-47ac-9d97-aaf6b4b860e0.css" rel="stylesheet" type="text/css" />
    <link href="frontend/assets/stylesheets/main-other-revision=9fbd5e73-5074-4d9a-ac52-05c78a2bb24a.css" rel="stylesheet" type="text/css" media="All" />
    <link href="frontend/assets/stylesheets/menu-revision=67ebcf47-9481-4316-8e28-471f6f240ea8.css" rel="stylesheet" type="text/css" media="All" />
    <link href="frontend/assets/stylesheets/mmenu-revision=858a93d0-9d67-4e3f-9a30-7ec5d22169d1.css" rel="stylesheet" type="text/css" media="All" />
    <link href="frontend/assets/stylesheets/sub-content-page-revision=5d228dbb-fb8a-4714-9759-45fc97a65f9b.css" rel="stylesheet" type="text/css" media="All" />
    <link href="frontend/assets/stylesheets/media-queries-revision=e430c762-76ec-4072-ab29-770166535d2b.css" rel="stylesheet" type="text/css" media="All" />
    <link href="frontend/assets/stylesheets/media-queries-other-revision=2169a956-d971-409a-b88b-0bcd7cab340b.css" rel="stylesheet" type="text/css" />
    <link href="frontend/assets/stylesheets/sub-content-page-media-queries-revision=e7848250-dc88-44a4-a30d-38c0dc4b9c5c.css" rel="stylesheet" type="text/css" media="All" />
    <link href="frontend/assets/stylesheets/jquery-ui-revision=44d88bb4-47b4-4c2e-bbe5-bd1a8ee6ac78.css" rel="stylesheet" type="text/css" media="All" />
    <link href="frontend/assets/stylesheets/slick-revision=8cac4851-68d2-4c36-b685-517a353a6a1e.css" rel="stylesheet" type="text/css" media="All" />
    <link href="frontend/assets/stylesheets/print-revision=a626a453-4acc-41df-9d6f-7f87b54e17e0.css" rel="stylesheet" type="text/css" media="Print" />
    <link href="frontend/assets/stylesheets/jmcrollbar-revision=3db960ec-74b7-4faa-9f5a-3507b3c85081.css" rel="stylesheet" type="text/css" media="All" />

{{--    <link href="frontend/assets/stylesheets/videoplayer.css" rel="stylesheet" type="text/css" media="All" />--}}

<!-- Javascripts -->

    <!-- New Breadcrumb -->
</head>

<body id="ip3-Homepage" class="unknown0 ip3-normal mis-production ip3-level0 nojs">

<script language="javascript" type="text/javascript">
    <!--
    //Added for IE7 compatibility issue.
    var bodyItem = document.getElementsByTagName("body")[0];
    var value = "";
    value = bodyItem.attributes['class'].nodeValue;
    bodyItem.className = value.replace("nojs", "hasjs");
    //document.getElementsByTagName("body")[0].setAttribute("class", document.getElementsByTagName("body")[0].getAttribute("class").replace("nojs", "hasjs"));
    -->
</script>

<form name="MainForm" method="post" action="/" id="MainForm">
    <div>
        <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="0YLnBBpalcyQvqr6fq97v+3JG+gMrE6ajBT7bkRpsG1J/eI+LhU8T6qtNoZprG4yoJKMHSwXmzKGHreIZHvWkahLl4+YbEg5fsIpAeUeZ84gVwuDOA4ROcCzWAnCU4OPpLlVO7Iu4hvjdHwV1I1dCutr0b8HAXSpATy435UvwFflAmduKba//qT137aGlVFJ4OWPDDPBqcpS77KFbAV7pULzhPvfOGd/R1bdYTguHFp/YGZofBRqrp71Idmkra+zyudxt4VFgLEeSBXq/ARk6TPLYmLFFwXKH+0k5QBWGVbSgvldTGlY52hUvdhXTqgz3ql/W8+CXIgYiMsJ1gdUo2Syxn+ZpzKbm+qpejtso+BmjsUpvnFtb8ZVpy96GuOA3ymvorOU6o4Dlj4BthNQlxYfXxxg4epZsPcHPnBSXi8RUE5JPbWh/hYrUlQVC5KUgVeqbEGUTqq2OaPBqvEz/K7nLBPWqWgHljW0ZfKErs9NHmckF96iD5g8CJBBUFDVnjssWtaXT+s1azJAr9WoawAiWfN2SOl0XhowKLZCtnoFKj+oZ5qVS59QyoqbCqBgciPZOm5mnPHxqyZNfEDPMz8hx69F4wTRpGOp9GafWXT4Cm+mkfWi0xpSyFY+2znN0F46ceRXiRjnQtNGnCdD7wyEEy8=" />
    </div>

    <script type="text/javascript">
        //<![CDATA[
        function checkValidSearchString(textboxid, defaultvalue, validationMsg) {
            var isValid = !((document.getElementById(textboxid).value == defaultvalue) || (document.getElementById(textboxid).value == ''));
            if (isValid)
                return true;
            else {
                alert(validationMsg);
                return false;
            }
        } //]]>
    </script>

    <div>

        <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="6B260122" />
        <input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
        <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="PxxqB99dWNVsb3SCG3sq8QGZ/+UJQWJyoUv3fsNvpqJ/rcLAJ/gtZEyN0IksQkgkIo/JtTmh6RnNWHDtR0EEyPSggnR+UEgi+Ei/Zw5rO4FvEbiD57IF5e0LX7rJM9V/dRDJMy8PhfadgVjf7QwlMj0muK0yKLU9rO8VJcF3uNpD+bznyj0IFbOvJJzrn1AJFBNgnA==" />
    </div>

    <!--Only for V2 Site B:3130-->

    <!-- New Breadcrumb -->

    <div id="MainWrapper" class="container-fluid has-parallax">

        <!--skipsearch-->
        <div class="header-wrapper">
            <div class="invisible">
                <a href="index.html#maincontent">
                    skip to main content
                </a>
            </div>

            <div id="header" class="header-row">
                <div class="header-inner row">
                    <div id="logo" class="col-xs-5 col-sm-2 col-md-3 col-md-offset-1">

                        <img src="frontend/assets/images/LOGOO.png" alt="ASOS plc" width="178" height="52" />

                    </div>
                    <!--Logo ends-->
                    <div id="header-right" class="col-xs-12 col-sm-10 col-md-7">
                        <div id="right-hdr-top">
                            <div class="header-ticker">
                                <iframe style="visibility: hidden;" onload="this.style.visibility='visible';" width="100%" id="ExternalWebContentExternalIFrameTicker" src="https://irs.tools.investis.com/clients/uk/asos/Ticker/Ticker.aspx?culture=en-GB"></iframe>
                            </div>
                            <div class="hdr-top-link">
                                <ul>
                                    <li><a href="contacts.html">Contacts</a></li>
                                </ul>
                            </div>
                            <div id="searchbox">
                                <div class="srch-inner">

                                    <div id="searchTextboxContainer">
                                        <script type="text/javascript">
                                            //<![CDATA[
                                            function FilterIP3SearchInput(event) {
                                                var keyCode = ('which' in event) ? event.which : event.keyCode;
                                                isNumericOrWithShift = (keyCode == 48 || keyCode == 57 || keyCode == 56 || keyCode == 188 || keyCode == 190) || (keyCode >= 96 && keyCode <= 105);
                                                isSymbol = (keyCode == 219 || keyCode == 221 || keyCode == 220);
                                                isWithoutShift = (keyCode == 191);
                                                modifiers = (event.shiftKey);
                                                if ((isNumericOrWithShift && modifiers) || isSymbol || (!modifiers && isWithoutShift)) {
                                                    return false;
                                                } else {
                                                    return true;
                                                }
                                            }
                                            //]]>
                                        </script>
                                        <input name="body_0$main_0$ctl03$ctl01$searchTextbox" type="text" value="Search" id="searchTextbox" onkeydown="return FilterIP3SearchInput(event);" onclick="if (this.value == &#39;Search&#39;){this.value=&#39;&#39;;}" onblur="if (this.value == &#39;&#39;){this.value=&#39;Search&#39;;}" />
                                    </div>
                                    <div id="SearchButtonContainer">
                                        <input type="submit" name="body_0$main_0$ctl03$ctl02$ctl00" value="GO" onclick="return checkValidSearchString(&#39;searchTextbox&#39;,&#39;Search&#39;,&#39;Please enter valid search keyword&#39;);" id="searchButton" />
                                    </div>

                                </div>
                                <div class="srch-btn-inner">
                                    <span class="icon-search-ico"></span>
                                </div>
                            </div>
                        </div>

                        <div class="right-hdr-bottom main-nav">
                            <ul id="section-Homepage-level1">
                                <li id="nav-investors-level1" class="level1 haschildren first"><a href="#"><span>New In</span></a></li>
                                <li id="nav-media-level1" class="level1 haschildren"><a href="#"><span>Fabric</span></a></li>
                                <li id="nav-corporate-responsibility-level1" class="level1 haschildren"><a href="#"><span>Made to Measure</span></a></li>
                                <li id="nav-careers-level1" class="level1 last"><a href="#"><span>Wholes</span></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-xs-12 mobiSearchWrapper">
                        <div class="mobi-menu-search ">
                            <div class="hambg menu-btn"><a href="index.html#mmenu"><span></span><span></span><span></span></a></div>
                            <div class="headsearch"><a href="javascript:;"><span class="icon-search-ico"></span></a></div>
                        </div>

                    </div>
                    <div class="searchbox-mobi-wrapper visible-xs col-xs-12">
                        <div id="searchbox-mobi">
                            <div class="srch-inner">

                                <div id="searchTextboxContainerMobi">
                                    <script type="text/javascript">
                                        //<![CDATA[
                                        function FilterIP3SearchInput(event) {
                                            var keyCode = ('which' in event) ? event.which : event.keyCode;
                                            isNumericOrWithShift = (keyCode == 48 || keyCode == 57 || keyCode == 56 || keyCode == 188 || keyCode == 190) || (keyCode >= 96 && keyCode <= 105);
                                            isSymbol = (keyCode == 219 || keyCode == 221 || keyCode == 220);
                                            isWithoutShift = (keyCode == 191);
                                            modifiers = (event.shiftKey);
                                            if ((isNumericOrWithShift && modifiers) || isSymbol || (!modifiers && isWithoutShift)) {
                                                return false;
                                            } else {
                                                return true;
                                            }
                                        }
                                        //]]>
                                    </script>
                                    <input name="body_0$main_0$ctl05$ctl01$searchTextboxMobi" type="text" value="Search" id="searchTextboxMobi" onkeydown="return FilterIP3SearchInput(event);" onclick="if (this.value == &#39;Search&#39;){this.value=&#39;&#39;;}" onblur="if (this.value == &#39;&#39;){this.value=&#39;Search&#39;;}" />
                                </div>
                                <div id="SearchButtonContainerMobi">
                                    <input type="submit" name="body_0$main_0$ctl05$ctl02$ctl00" value="GO" onclick="return checkValidSearchString(&#39;searchTextboxMobi&#39;,&#39;Search&#39;,&#39;Please enter valid search keyword&#39;);" id="searchButtonMobi" />
                                </div>

                            </div>

                        </div>
                    </div>

                    <div id="mmenu" class="mmenu hide-mobimenu">
                        <div class="mmenu-list">
                            <ul id="mmenumain-section-Homepage-level1">
                                <li id="mmenumain-nav-Homepage-level1" class="level1 current first"><span>Home</span></li>
                                <li id="mmenumain-nav-investors-level1" class="level1 haschildren"><a href="investors.html"><span>Investors</span></a>
                                    <ul id="mmenumain-section-investors-level2">
                                        <li id="mmenumain-nav-our-opportunity-level2" class="level2 first"><a href="investors/our-opportunity.html"><span>Our Opportunity</span></a></li>
                                        <li id="mmenumain-nav-our-business-model-level2" class="level2"><a href="investors/our-business-model.html"><span>Business Model</span></a></li>
                                        <li id="mmenumain-nav-leadership-corporate-governance-level2" class="level2"><a href="investors/leadership-corporate-governance.html"><span>Leadership & Corporate Governance</span></a></li>
                                        <li id="mmenumain-nav-latest-results-level2" class="level2"><a href="investors/latest-results.html"><span>Latest Results</span></a></li>
                                        <li id="mmenumain-nav-2019-year-in-review-level2" class="level2 haschildren"><a href="investors/2019-year-in-review.html"><span>2019 in Review</span></a>
                                            <ul id="mmenumain-section-2019-year-in-review-level3">
                                                <li id="mmenumain-nav-financial-and-operational-highlights-level3" class="level3 first"><a href="investors/2019-year-in-review/financial-and-operational-highlights.html"><span>Financial & Operational Highlights</span></a></li>
                                                <li id="mmenumain-nav-chairmans-statement-level3" class="level3"><a href="investors/2019-year-in-review/chairmans-statement.html"><span>Chair’s Statement</span></a></li>
                                                <li id="mmenumain-nav-year-in-review-level3" class="level3"><a href="investors/2019-year-in-review/year-in-review.html"><span>Year in Review</span></a></li>
                                                <li id="mmenumain-nav-kpis-level3" class="level3"><a href="investors/2019-year-in-review/kpis.html"><span>KPI<sub>s</sub></span></a></li>
                                                <li id="mmenumain-nav-financial-review-level3" class="level3"><a href="investors/2019-year-in-review/financial-review.html"><span>Financial Review</span></a></li>
                                                <li id="mmenumain-nav-risk-level3" class="level3 last"><a href="investors/2019-year-in-review/risk.html"><span>Risk</span></a></li>
                                            </ul>
                                        </li>
                                        <li id="mmenumain-nav-reports-and-presentations-level2" class="level2"><a href="investors/reports-and-presentations/2020.html"><span>Reports & Presentations</span></a></li>
                                        <li id="mmenumain-nav-key-financial-information-level2" class="level2"><a href="investors/key-financial-information.html"><span>Key Financial Information</span></a></li>
                                        <li id="mmenumain-nav-financial-calendar-level2" class="level2"><a href="investors/financial-calendar.html"><span>Financial Calendar</span></a></li>
                                        <li id="mmenumain-nav-regulatory-news-level2" class="level2"><a href="investors/regulatory-news.html"><span>Regulatory News</span></a></li>
                                        <li id="mmenumain-nav-share-price-level2" class="level2 haschildren"><a href="investors/share-price.html"><span>Share Price</span></a>
                                            <ul id="mmenumain-section-share-price-level3">
                                                <li id="mmenumain-nav-share-price-chart-level3" class="level3 first last"><a href="investors/share-price/share-price-chart.html"><span>Share Price Chart</span></a></li>
                                            </ul>
                                        </li>
                                        <li id="mmenumain-nav-consensus-level2" class="level2"><a href="investors/consensus.html"><span>Consensus</span></a></li>
                                        <li id="mmenumain-nav-shareholder-information-level2" class="level2 haschildren last"><a href="investors/shareholder-information/agm/2019.html"><span>Shareholder Information</span></a>
                                            <ul id="mmenumain-section-shareholder-information-level3">
                                                <li id="mmenumain-nav-agm-level3" class="level3 first"><a href="investors/shareholder-information/agm/2019.html"><span>AGM</span></a></li>
                                                <li id="mmenumain-nav-aim-rule-26-level3" class="level3"><a href="investors/shareholder-information/aim-rule-26.html"><span>AIM Rule 26</span></a></li>
                                                <li id="mmenumain-nav-major-shareholders-level3" class="level3"><a href="investors/shareholder-information/major-shareholders.html"><span>Major Shareholders</span></a></li>
                                                <li id="mmenumain-nav-company-documents-level3" class="level3"><a href="investors/shareholder-information/company-documents.html"><span>Company Documents</span></a></li>
                                                <li id="mmenumain-nav-glossary-level3" class="level3"><a href="investors/shareholder-information/glossary.html"><span>Glossary</span></a></li>
                                                <li id="mmenumain-nav-analyst-coverage-level3" class="level3"><a href="investors/shareholder-information/analyst-coverage.html"><span>Analyst Coverage</span></a></li>
                                                <li id="mmenumain-nav-advisors-level3" class="level3 last"><a href="investors/shareholder-information/advisors.html"><span>Advisors</span></a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li id="mmenumain-nav-media-level1" class="level1 haschildren"><a href="media.html"><span>Media</span></a>
                                    <ul id="mmenumain-section-media-level2">
                                        <li id="mmenumain-nav-global-contacts-level2" class="level2 first"><a href="media/global-contacts.html"><span>Global Email Contacts</span></a></li>
                                        <li id="mmenumain-nav-media-gallery-level2" class="level2"><a href="media/media-gallery.html"><span>Media Gallery</span></a></li>
                                        <li id="mmenumain-nav-news-level2" class="level2"><a href="media/news.html"><span>News</span></a></li>
                                        <li id="mmenumain-nav-social-media-level2" class="level2 last"><a href="media/social-media.html"><span>Social Media</span></a></li>
                                    </ul>
                                </li>
                                <li id="mmenumain-nav-corporate-responsibility-level1" class="level1 haschildren"><a href="corporate-responsibility.html"><span>Corporate Responsibility</span></a>
                                    <ul id="mmenumain-section-corporate-responsibility-level2">
                                        <li id="mmenumain-nav-fashion-with-integrity-level2" class="level2 haschildren first"><a href="corporate-responsibility/fashion-with-integrity.html"><span>Fashion With Integrity</span></a>
                                            <ul id="mmenumain-section-fashion-with-integrity-level3">
                                                <li id="mmenumain-nav-stakeholder-engagement-level3" class="level3 first"><a href="corporate-responsibility/fashion-with-integrity/stakeholder-engagement.html"><span>Stakeholder Engagement</span></a></li>
                                                <li id="mmenumain-nav-un-global-compact-level3" class="level3"><a href="corporate-responsibility/fashion-with-integrity/un-global-compact.html"><span>UN Global Compact</span></a></li>
                                                <li id="mmenumain-nav-business-integrity-level3" class="level3 last"><a href="corporate-responsibility/fashion-with-integrity/business-integrity.html"><span>Business Integrity</span></a></li>
                                            </ul>
                                        </li>
                                        <li id="mmenumain-nav-our-products-level2" class="level2 haschildren"><a href="corporate-responsibility/our-products.html"><span>Our Products</span></a>
                                            <ul id="mmenumain-section-our-products-level3">
                                                <li id="mmenumain-nav-sustainable-sourcing-definitions-level3" class="level3 first"><a href="corporate-responsibility/our-products/sustainable-sourcing-definitions.html"><span>Sustainable Sourcing Definitions</span></a></li>
                                                <li id="mmenumain-nav-our-supply-chain-level3" class="level3"><a href="corporate-responsibility/our-products/our-supply-chain.html"><span>Our Supply Chain</span></a></li>
                                                <li id="mmenumain-nav-ethical-trade-programme-level3" class="level3"><a href="corporate-responsibility/our-products/ethical-trade-programme.html"><span>Ethical Trade Programme</span></a></li>
                                                <li id="mmenumain-nav-ethical-trade-partnerships-level3" class="level3"><a href="corporate-responsibility/our-products/ethical-trade-partnerships.html"><span>Ethical Trade Partnerships</span></a></li>
                                                <li id="mmenumain-nav-sustainable-sourcing-programme-level3" class="level3"><a href="corporate-responsibility/our-products/sustainable-sourcing-programme.html"><span>Sustainable Sourcing Programme</span></a></li>
                                                <li id="mmenumain-nav-sustainable-sourcing-partnerships-level3" class="level3"><a href="corporate-responsibility/our-products/sustainable-sourcing-partnerships.html"><span>Sustainable Sourcing Partnerships</span></a></li>
                                                <li id="mmenumain-nav-responsible-edit-level3" class="level3"><a href="corporate-responsibility/our-products/responsible-edit.html"><span>Responsible Edit</span></a></li>
                                                <li id="mmenumain-nav-animal-welfare-level3" class="level3"><a href="corporate-responsibility/our-products/animal-welfare.html"><span>Animal Derived Materials Policy</span></a></li>
                                                <li id="mmenumain-nav-third-party-brands-programme-level3" class="level3 last"><a href="corporate-responsibility/our-products/third-party-brands-programme.html"><span>Third-Party Brands Programme</span></a></li>
                                            </ul>
                                        </li>
                                        <li id="mmenumain-nav-our-business-level2" class="level2 haschildren"><a href="corporate-responsibility/our-business.html"><span>Our Business</span></a>
                                            <ul id="mmenumain-section-our-business-level3">
                                                <li id="mmenumain-nav-carbon-and-energy-level3" class="level3 first"><a href="corporate-responsibility/our-business/carbon-and-energy.html"><span>Carbon & Energy</span></a></li>
                                                <li id="mmenumain-nav-packaging-and-waste-level3" class="level3 last"><a href="corporate-responsibility/our-business/packaging-and-waste.html"><span>Packaging & Waste</span></a></li>
                                            </ul>
                                        </li>
                                        <li id="mmenumain-nav-our-customers-level2" class="level2 haschildren"><a href="corporate-responsibility/our-customers.html"><span>Our Customers</span></a>
                                            <ul id="mmenumain-section-our-customers-level3">
                                                <li id="mmenumain-nav-customer-care-level3" class="level3 first"><a href="corporate-responsibility/our-customers/customer-care.html"><span>Customer Care</span></a></li>
                                                <li id="mmenumain-nav-inclusion-level3" class="level3"><a href="corporate-responsibility/our-customers/inclusion.html"><span>Inclusion</span></a></li>
                                                <li id="mmenumain-nav-mind-and-body-level3" class="level3 last"><a href="corporate-responsibility/our-customers/mind-and-body.html"><span>Mind & Body</span></a></li>
                                            </ul>
                                        </li>
                                        <li id="mmenumain-nav-our-community-level2" class="level2 haschildren"><a href="corporate-responsibility/our-community.html"><span>Our Community</span></a>
                                            <ul id="mmenumain-section-our-community-level3">
                                                <li id="mmenumain-nav-employees-in-the-community-level3" class="level3 first"><a href="corporate-responsibility/our-community/employees-in-the-community.html"><span>Employees in the Community</span></a></li>
                                                <li id="mmenumain-nav-community-programmes-level3" class="level3 last"><a href="corporate-responsibility/our-community/community-programmes.html"><span>Community Programmes</span></a></li>
                                            </ul>
                                        </li>
                                        <li id="mmenumain-nav-performance-and-commitments-level2" class="level2"><a href="corporate-responsibility/performance-and-commitments.html"><span>Performance & Commitments</span></a></li>
                                        <li id="mmenumain-nav-reporting-and-policies-level2" class="level2 last"><a href="corporate-responsibility/reporting-and-policies.html"><span>Reporting & Policies</span></a></li>
                                    </ul>
                                </li>
                                <li id="mmenumain-nav-careers-level1" class="level1 last"><a href="careers.html"><span>Careers</span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="mobimenuclose"><a href="javascript:;" class="mmenu-close"><span></span><span></span></a></div>
                </div>
            </div>

            <div id="dummy-header-height"></div>

            <!-- 2nd level Navigation Starts -->
            <div id="subNav">
                <div class="main-sub-menu-row row">
                    <div class="main-sub-nav col-md-offset-1 col-md-10 col-xs-12">
                        <ul id="section-Homepage-level2">
                            <li id="nav-investors-level2" class="level2 haschildren first"><a href="investors.html"><span>Investors</span></a>
                                <ul id="section-investors-level3">
                                    <li id="nav-our-opportunity-level3" class="level3 first"><a href="investors/our-opportunity.html"><span>Our Opportunity</span></a></li>
                                    <li id="nav-our-business-model-level3" class="level3"><a href="investors/our-business-model.html"><span>Business Model</span></a></li>
                                    <li id="nav-leadership-corporate-governance-level3" class="level3"><a href="investors/leadership-corporate-governance.html"><span>Leadership & Corporate Governance</span></a></li>
                                    <li id="nav-latest-results-level3" class="level3"><a href="investors/latest-results.html"><span>Latest Results</span></a></li>
                                    <li id="nav-2019-year-in-review-level3" class="level3 haschildren"><a href="investors/2019-year-in-review.html"><span>2019 in Review</span></a></li>
                                    <li id="nav-reports-and-presentations-level3" class="level3"><a href="investors/reports-and-presentations/2020.html"><span>Reports & Presentations</span></a></li>
                                    <li id="nav-key-financial-information-level3" class="level3"><a href="investors/key-financial-information.html"><span>Key Financial Information</span></a></li>
                                    <li id="nav-financial-calendar-level3" class="level3"><a href="investors/financial-calendar.html"><span>Financial Calendar</span></a></li>
                                    <li id="nav-regulatory-news-level3" class="level3"><a href="investors/regulatory-news.html"><span>Regulatory News</span></a></li>
                                    <li id="nav-share-price-level3" class="level3 haschildren"><a href="investors/share-price.html"><span>Share Price</span></a></li>
                                    <li id="nav-consensus-level3" class="level3"><a href="investors/consensus.html"><span>Consensus</span></a></li>
                                    <li id="nav-shareholder-information-level3" class="level3 haschildren last"><a href="investors/shareholder-information/agm/2019.html"><span>Shareholder Information</span></a></li>
                                </ul>
                            </li>
                            <li id="nav-media-level2" class="level2 haschildren"><a href="media.html"><span>Media</span></a>
                                <ul id="section-media-level3">
                                    <li id="nav-global-contacts-level3" class="level3 first"><a href="media/global-contacts.html"><span>Global Email Contacts</span></a></li>
                                    <li id="nav-media-gallery-level3" class="level3"><a href="media/media-gallery.html"><span>Media Gallery</span></a></li>
                                    <li id="nav-news-level3" class="level3"><a href="media/news.html"><span>News</span></a></li>
                                    <li id="nav-social-media-level3" class="level3 last"><a href="media/social-media.html"><span>Social Media</span></a></li>
                                </ul>
                            </li>
                            <li id="nav-corporate-responsibility-level2" class="level2 haschildren"><a href="corporate-responsibility.html"><span>Corporate Responsibility</span></a>
                                <ul id="section-corporate-responsibility-level3">
                                    <li id="nav-fashion-with-integrity-level3" class="level3 haschildren first"><a href="corporate-responsibility/fashion-with-integrity.html"><span>Fashion With Integrity</span></a></li>
                                    <li id="nav-our-products-level3" class="level3 haschildren"><a href="corporate-responsibility/our-products.html"><span>Our Products</span></a></li>
                                    <li id="nav-our-business-level3" class="level3 haschildren"><a href="corporate-responsibility/our-business.html"><span>Our Business</span></a></li>
                                    <li id="nav-our-customers-level3" class="level3 haschildren"><a href="corporate-responsibility/our-customers.html"><span>Our Customers</span></a></li>
                                    <li id="nav-our-community-level3" class="level3 haschildren"><a href="corporate-responsibility/our-community.html"><span>Our Community</span></a></li>
                                    <li id="nav-performance-and-commitments-level3" class="level3"><a href="corporate-responsibility/performance-and-commitments.html"><span>Performance & Commitments</span></a></li>
                                    <li id="nav-reporting-and-policies-level3" class="level3 last"><a href="corporate-responsibility/reporting-and-policies.html"><span>Reporting & Policies</span></a></li>
                                </ul>
                            </li>
                            <li id="nav-careers-level2" class="level2 last"><a href="careers.html"><span>Careers</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- 2nd level Navigation Ends -->

            <!--  Content banner Starts  -->

            <!--skipsearch-->
            <div class="row banner-row">
                <div id="bannerwrapper" class="col-xs-12">
                    <div id="bannerContainer" class="BannerContainer">
                        <img id="bannerImage" class="BannerImage" src="~/media/Images/A/Asos-V2/banners/investors-banner-.jpg" alt="" style="border-width:0px;" /></div>
                </div>
            </div>
            <!--/skipsearch-->
            <!--  Content banner ends  -->

        </div>
        <!--/skipsearch-->
        <!--  Video overlay starts  -->
        <div class="vid-outer">
            <div class="video-container">
                <div class="vid-inr">
                    <div class="place-holder"></div>

                </div>
            </div>
        </div>
        <!--  Video overlay ends  -->

        <!--  Case study overlay starts  -->

        <div id="cs-overlay-outer" class="overlay-wrapper cs-overlay">
            <div class="signpost-overlay overlay">
                <div id="cs-overlay-inner" class="signpost-overlay-inn sp-content row"></div>
            </div>
            <div class="video-overlay overlay">
                <div class="trust-video-container">
                    <div class="cs-place-holder"></div>
                </div>
            </div>
            <div class="image-gallery-overlay overlay"></div>
            <div class="overlay-controls">
                <span class="icon-cross"></span>
                <div class="inner-sp-tabs">
                    <span class="icon-cs-page-ico active"></span>
                    <span class="icon-movie-ico"></span>
                    <span class="icon-camera-ico"></span>
                </div>
            </div>
        </div>

        <!--  Case study overlay ends  -->

        <div id="contentwrapper" class="main-content home-landing parallax-page">

            <!-- For Skip to Main Content Only -->
            <a class="hide" name="maincontent" id="mainContent"></a>

            <div class="homecontentwrapper">

                <div id="slidesContainer">

                    <div class="leadership-outer">
                        <div class="leadership-container">

                            <div class="leadership-vid-wrp bod-overlay">
                                <div class="vid-inr">

                                    <div class="place-holder"></div>
                                </div>
                            </div>

                            <div class="overlay-controls">
                                <span class="icon-cross bod-close"></span>

                            </div>
                        </div>
                    </div>

                    <!--Section 0 Start-->

                    <!--skipsearch-->

                    <div class="static section0 landing-banner video-banner section active">

                        <h1 class="invisible">ASOS &ndash; We focus on fashion as a force for good, inspiring young people to express their best selves and achieve amazing things. We believe fashion thrives on individuality and should be fun for everyone.</h1>

                        <script type="text/javascript">
                            $j(document).ready(function() {
                                $j("#videoPlayerBanner").InvPlayer();
                            });
                        </script>
                        <div id="videoPlayerBanner" class="inv_playerContainer inv_desktop inv_bannerPlayer inv_playerReady applyhover inv_videoReady inv_videoPlaying" inv_videoid="asos002" inv_playertype="singleplayer" inv_autoplay="true" inv_isbanner="true" inv_loopenabled="true" inv_videodatatype="brightcove"><div class="inv_videoMainContainer inv_mouseLeave noCursor">
                                <div class="inv_videoMainContainer_inner">
                                    <div class="inv_videoDisplay">
                                        <div class="inv_videoLoaderArea" style="display: none;"></div>
                                        <div class="inv_overlayPlay inv_overlayPlayClicked"></div>
                                        <div class="inv_videoStillArea" style="display: none;">
                                            <img src="https://f1.media.brightcove.com/7/1555966121001/1555966121001_5209370332001_5209360879001-vs.jpg?pubId=1555966121001&amp;videoId=5209360879001">
                                        </div>
                                        <div class="inv_videoArea" referenceid="asos002"><img src="https://f1.media.brightcove.com/7/1555966121001/1555966121001_5209370332001_5209360879001-vs.jpg?pubId=1555966121001&amp;videoId=5209360879001"><video id="inv_VideoPlayer_1585088075590" width="100%" height="100%" playsinline="" muted="" loop="loop"><source src="https://f1.media.brightcove.com/4/1555966121001/1555966121001_5209370355001_5209360879001.mp4?pubId=1555966121001&amp;videoId=5209360879001" type="video/mp4">Your browser does not support the video tag.</video></div>
                                        <div class="inv_container" id="inv_container"></div>
                                    </div>
                                    <div class="inv_vrSphericalControls">
                                        <div class="inv_viewUp inv_moveView" inv-data-direction="up"></div>
                                        <div class="inv_viewLeft inv_moveView" inv-data-direction="left"></div>
                                        <div class="inv_viewRight inv_moveView" inv-data-direction="right"></div>
                                        <div class="inv_viewDown inv_moveView" inv-data-direction="down"></div>
                                    </div>
                                    <div class="inv_videoControls">
                                        <div class="inv_playheadWellContainer">
                                            <div class="inv_playhead inv_initialPlayhead" style="left: 7.15947%;"></div>
                                            <div class="inv_playheadWellWatched inv_initialPlayheadwell" style="width: 7.15947%;"></div>
                                            <div class="inv_playheadWellBuffered inv_initialPlayheadwell" style="width: 100%;"></div>
                                            <div class="inv_playheadWell"></div>
                                        </div>
                                        <div class="inv_playPauseBtn inv_pauseBtn"></div>
                                        <div class="inv_soundContainer">
                                            <div class="inv_soundOnOffBtn inv_soundOnBtn"></div>
                                        </div>
                                        <div class="inv_startTime">00:01</div>
                                        <div class="inv_seperatorTime">/</div>
                                        <div class="inv_endTime">00:21</div>
                                        <div class="inv_fullscreenBtn"></div>
                                        <div class="inv_externalLink"></div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="landing-banner-text-wrapper">
                            <div class="landing-bnr-txt ">
                                <h2 class="">WE ARE AUTHENTIC, BRAVE AND CREATIVE TO OUR CORE</h2>
                            </div>
                            <div class="landing-bnr-subtxt trans">
                                <p>We focus on fashion as a force for good, inspiring young people to express their best selves and achieve amazing things. We believe fashion thrives on individuality and should be fun for everyone.</p>
                            </div>

                            <div class="green-box-link">
                                <p>
                                    <a href="asos-story.html">
                                        <strong><span data-hover="Discover the ASOS story">Discover the ASOS story</span></strong>
                                        <span class="icon-chevron-right"></span>
                                    </a>
                                </p>
                            </div>

                        </div>

                    </div>

                    <!--/skipsearch-->
                    <!--Section 0 End -->

                    <!--Section 1 Start -->
                    <div class="section1 slide vslide section home-section" data-dataId="1">
                        <div class="row">
                            <div class="col-xs-12 col-sm-offset-1 col-sm-10">
                                <div class="financial-highlights-box">

                                    <h3 class="rad-center">Our mission is to become the world’s number-one solution for cooperatives and  fashion-loving Designers around africa by driving long-lasting positive change and improving living conditions by investing in people, communities and innovative ideas.</h3>
                                </div>
                            </div>

                        </div>

                    </div>
                    <!--Section 1 End -->
                </div>

            </div>
        </div>
        <!--/skipsearch-->
    </div>

    <div id="__JSONMENU" style="display: none;"></div>
</form>
{{--<script type="text/javascript" src="https://viz.tools.investis.com/video/videoPlayer-v.2.0-latest/asos/js/videoMainConfig.js"></script>--}}
<script type="text/javascript" src="frontend/assets/javascripts/videoMainConfig.js"></script>
<script type="text/javascript" src="frontend/assets/javascripts/main-revision=67a3049e-11f1-4d3c-b577-252b9af52c4f.js"></script>
<script type="text/javascript" src="frontend/assets/javascripts/frame-manager-revision=e45b23de-0e69-4343-b1cd-dea97dac9cec.js"></script>
<script type="text/javascript" src="frontend/assets/javascripts/slick-revision=29c732f2-a375-4866-8464-fa93821280e2.js"></script>
<script type="text/javascript" src="frontend/assets/javascripts/ResourceHandlermerge-0=%7B33F48B7D-9EE4-42E0-A09D-3AE0E66ED8D2%7D&1=%7B164D5C12-A28E-483D-BB94-676877C34E74%7D&3=%7B8C175307-AC9D-44FC-82D9-269E2CCE7593%7D&4=%7B49DAFCBD-0621-4178-B021-653A35306DA2%7D&5=%7BD2398592-45A7-4841-B8CB-CD179284A9FC%7D&6=%7BA558AC-10fabb9746dc2dfb.js"></script>

</body>

</html>
