<?php

namespace App;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Laratrust\Models\LaratrustRole;
use Illuminate\Database\Eloquent\Model;
use Laratrust\Traits\LaratrustUserTrait;

class Role extends LaratrustRole
{
    protected $fillable = [
        'name','display_name','description'
        ];

}
