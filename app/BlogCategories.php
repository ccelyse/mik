<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogCategories extends Model
{
    protected $table = "blog_categories";
    protected $fillable = ['id','post_id','category_id'];
}
