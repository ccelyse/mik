<?php

namespace App\Http\Controllers;

use App\AssessmentFindingsRecomds;
use Illuminate\Http\Request;

class AssessmentFindingsRecomdsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AssessmentFindingsRecomds  $assessmentFindingsRecomds
     * @return \Illuminate\Http\Response
     */
    public function show(AssessmentFindingsRecomds $assessmentFindingsRecomds)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AssessmentFindingsRecomds  $assessmentFindingsRecomds
     * @return \Illuminate\Http\Response
     */
    public function edit(AssessmentFindingsRecomds $assessmentFindingsRecomds)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AssessmentFindingsRecomds  $assessmentFindingsRecomds
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssessmentFindingsRecomds $assessmentFindingsRecomds)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssessmentFindingsRecomds  $assessmentFindingsRecomds
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssessmentFindingsRecomds $assessmentFindingsRecomds)
    {
        //
    }
}
