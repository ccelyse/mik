<?php

namespace App\Http\Controllers;


use App\Agents;

use App\Category;
use App\Client;

use App\Country;
use App\ProductCategories;
use App\ProductProvider;
use App\Products;
use App\ProductsSTOs;
use App\ProviderContracts;

use App\Role;
use App\SystemQuotation;
use App\SystemQuotationDays;
use App\SystemQuoteDetails;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BackendController extends Controller

{
//    use Illuminate\Foundation\Testing\WithoutMiddleware;

    protected $user;

    public function __construct()
    {
//        $this->middleware('auth:api', ['except' => ['login']]);
        $this->middleware('jwt', ['except' => ['login']]);
    }

    /* create account */

    public function CreateAccount(Request $request){
        $User = new User();
        $User->email = $request['email'];
        $User->name = $request['name'];
        $User->password = bcrypt($request['password']);
        $User->save();

        return response()->json([
            'response_message' => "account successfully created",
        ]);


    }

    /*DASHBOARD COUNT*/

    public function DashboardCount(){
        $countusers = User::count();
        $countagent = Agents::count();

        return response()->json([
            'response_message' => "success",
            'response_status' =>200,
            'countusers' =>$countusers,
            'countagent' =>$countagent,
            'pendingQuotation' =>"",
            'approvedQuotation' =>"",

        ]);
    }
    /*SHOW ROLES*/

    public function Roles(){
        $listroles = Role::all();
        return $listroles->toArray();
    }


    public function ShowRoles(){
        $show = Role::all();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }

    /*LOGIN ROUTE FUNCTION*/

    public function login(){
        return view('backend.login');
    }

    /*SHOW ALL CREATED APP ACCOUNT */
    public function ShowSystemAccount(){
        $show = User::all();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }

    /*SHOW ACCOUNT BY EMAIL */
    public function ShowUserAccount(Request $request){
        $show = User::where('email',$request['email'])->get();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }

    /*UPDATE USER ACCOUNT*/
    public function UpdateUserAccount(Request $request){
        $update = User::where('id',$request['id'])->update(['name' => $request['name'],'email'=>$request['email']]);
        $arr = array('response_message' => 'Something goes to wrong. Please try again later', 'response_status' => false);
        if($update){
            $arr = array('response_message' => 'Successfully update account information', 'response_status' => true);
        }
        return Response()->json($arr);
    }

    /*LIST COUNTRIES */
    public function Countries(){
        $show = Country::all();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }

    /*ADD SYSTEM AGENT */
    public function AddAgent(Request $request){
    $all = $request->all();
    $add = Agents::create($all);

    $arr = array('response_message' => 'Something goes to wrong. Please try again later', 'response_status' => false);
    if($add){
        $arr = array('response_message' => 'Successfully created a Agents', 'response_status' => true);
    }
    return Response()->json($arr);
    }

    /*SHOW ALL AGENTS*/

    public function ShowAgent(){
        $show = Agents::join('apps_countries', 'apps_countries.id', '=', 'agents.agent_country')
            ->select('agents.*', 'apps_countries.country_name')
            ->get();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "You do not have any Agents yet",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }

    /*SHOW AGENT BY ID*/
    public function ShowAgentById(Request $request){
        $show = Agents::where('agents.id',$request['id'])
            ->join('apps_countries', 'apps_countries.id', '=', 'agents.agent_country')
            ->select('agents.*', 'apps_countries.country_name')
            ->get();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }

    /*UPDATE AGENT INFORMATION */
    public function UpdateAgent(Request $request){
        $id = $request['id_edit'];
        $update = Agents::find($id);
        $update->agent_name = $request['agent_name'];
        $update->agent_email = $request['agent_email'];
        $update->agent_phone = $request['agent_phone'];
        $update->agent_percentage = $request['agent_percentage'];
        $update->agent_country = $request['agent_country'];
        $update->agent_city = $request['agent_city'];
        $update->agent_state = $request['agent_state'];
        $update->agent_address = $request['agent_address'];
        $update->agent_notes = $request['agent_notes'];
        $update->save();

        return response()->json([
            'response_message' =>'successfully updated Agents information',
            'response_status' =>true
        ]);
    }
    /*DELETE AGENT ACCOUNT */
    public function DeleteAgent(Request $request){
        $id = $request['id'];
        $delete = Agents::find($id);
        $delete->delete();
        $arr = array('response_message' => 'Something goes to wrong. Please try again later', 'response_status' => false);
        if($delete){
            $arr = array('response_message' => 'Successfully deleted Agents', 'response_status' => true);
        }
        return Response()->json($arr);
    }




}
