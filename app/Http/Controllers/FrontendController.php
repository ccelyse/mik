<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontendController extends Controller
{

    public function ComingSoon(){
        return view('frontend.ComingSoon');
    }
    public function BlogPage(){
        return view('frontend.blog.blog');
    }
    public function BlogPost(){
        return view('frontend.blog.BlogPost');
    }
    public function AboutUs(){
        return view('frontend.AboutUs');
    }
    public function WhoWeAre(){
        return view('frontend.WhoWeAre');
    }
    public function HowWeDoIt(){
        return view('frontend.HowWeDoIt');
    }
}
