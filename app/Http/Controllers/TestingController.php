<?php

namespace App\Http\Controllers;

use App\BlogCategories;
use App\BlogPost;
use App\BlogTags;
use App\Category;
use App\Districts;
use App\HospitalInfo;
use App\Provinces;
use App\Role;
use App\SystemQuotationDays;
use App\Tags;
use Illuminate\Http\Request;
//use Tymon\JWTAuth\Contracts\Providers\Auth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
//use Maatwebsite\Excel\Excel;
use Excel;
use JWTAuth;
use phpDocumentor\Reflection\DocBlock\Tag;

class TestingController extends Controller
{
    public function auth(){
        return view('backend.auth');
    }
    public function dashboard(){
//        $userId = Auth::id();

        return view('backend.dashboard');
    }


    public function CreateAccount(){
        return view('backend.CreateAccount');
    }
    public function H_Categorization(){
        return view('backend.H_Categorization');
    }
    public function HospitalAdd(){
        return view('backend.HospitalAdd');
    }
    public function Health_Center_Add(){
        return view('backend.Health_Center_Add');
    }
    public function ShowHealthCenterUI(){
        return view('backend.ShowHealthCenterUI');
    }
    public function ShowHospitalInfoUI(){
        return view('backend.ShowHospitalInfoUI');
    }
    public function ShowHospitalInfoMore(){
        return view('backend.ShowHospitalInfoMore');
    }
//    public function AccreditationSurveyors(){
//        return view('backend.AccreditationSurveyors');
//    }
    public function AccreditxSurvey(){
        return view('backend.AccreditxSurvey');
    }
    public function RiskArea(){
        return view('backend.RiskArea');
    }
    public function Standard(){
        return view('backend.Standard');
    }
    public function Level(){
        return view('backend.Level');
    }
    public function Perform_finding(){
        return view('backend.Perform_finding');
    }
    public function AccreditationSurveyors(){
        return view('backend.AccreditationSurveyors');
    }




// Hospital Account

    public function HDashboard(){
//        $userId = Auth::id();
        return view('backend.Hdashboard');
    }
    public function SystemAccount(){
        return view('backend.SystemAccount');
    }
    public function ShowHospital(){
        return view('backend.ShowHospital');
    }

//    SystemSurveyors
    public function SystemClients(){
        return view('backend.SystemClients');
    }
    public function SystemProducts(){
        return view('backend.SystemProducts');
    }
    public function SystemContracts(){
        return view('backend.SystemContracts');
    }
    public function SystemQuotation(){
        return view('backend.SystemQuotation');
    }
    public function SystemQuotationDays(){
        return view('backend.SystemQuotationDays');
    }
    public function SystemQuotationDaysServices(){
        return view('backend.SystemQuotationDaysServices');
    }
    public function SystemProductCategories(){
        return view('backend.SystemProductCategories');
    }
    public function SystemProductProvider(){
        return view('backend.SystemProductProvider');
    }
    public function SystemAgents(){
        return view('backend.SystemAgents');
    }
    public function SystemPdf(){
        return view('backend.SystemPdf');
    }
    public function SurveyHospital(){
        return view('backend.SurveyHospital');
    }
    public function ShowHospitalInfoMoreS(){
        return view('backend.ShowHospitalInfoMoreS');
    }
    public function ShowHealthCenterS(){
        return view('backend.ShowHealthCenterS');
    }
    public function CheckNewDate(){
        $check_days = SystemQuotationDays::where('quotation_id','1')->get()->last();
        $date = strtotime("+1 day", strtotime($check_days->quotation_day));
        $new_date = date("Y-m-d", $date);


    }
    public function UploadExcelHospital(){
//        $user = JWTAuth::parseToken()->authenticate();
//        dd($user);
        if( Input::file('hospital_excel') ) {
            $path = Input::file('hospital_excel')->getRealPath();
        }
        else {
            return "Error";
        }
        $data = Excel::load($path)->get();

        if($data->count()){
            foreach ($data as $value) {
                $province_id = Provinces::where('name', 'like', '%' . $value['province_id'] . '%')->first();
                $district_id = Districts::where('district_name', 'like', '%' . $value['district_id'] . '%')->first();

                if(!empty($province_id) && !empty($district_id)) {
                    HospitalInfo::create(
                        [
                            'province_id' => $province_id->id,
                            'district_id' => $district_id->id,
                            'facility_name' => $value['facility_name'],
                            'hospital_cat_id' => $value['hospital_cat_id'],
                            'facility_phone' => $value['facility_phone'],
                            'facility_email' => $value['facility_email'],
                            'name_of_facility_head' => $value['name_of_facility_head'],
                            'phone_of_head_facility' => $value['phone_of_head_facility'],
                            'name_alternative_facility_head' => $value['name_alternative_facility_head'],
                            'phone_alternative_facility_head' => $value['phone_alternative_facility_head'],
                            'population_covered' => $value['population_covered'],
                            'total_beds' => $value['total_beds'],
                            'maternity_beds' => $value['maternity_beds'],
                            'operational_ambulances' => $value['operational_ambulances'],
                            'general_practitioner' => $value['general_practitioner'],
                            'specialists' => $value['specialists'],
                            'a1' => $value['a1'],
                            'a2' => $value['a2'],
                            'mid_wives' => $value['mid_wives'],
                            'lab_techs' => $value['lab_techs'],
                            'physio' => $value['physio'],
                            'anesthetists' => $value['anesthetists'],
                            'pharmacists' => $value['pharmacists'],
                            'dentists' => $value['dentists'],
//                            'user_id' => '',
                        ]
                    );
                }
            }
        }
        return back()->with('success','You have successfully upload hospital information');
    }
    public function AssessmentPeriod(){
        return view('backend.AssessmentPeriod');
    }
    public function SurveyReports(){
        return view('backend.SurveyReports');
    }
    public function AccreditationReport(){
        return view('backend.AccreditationReport');
    }
    public function AccreditationReportPdf(){
        return view('backend.AccreditationReportPdf');
    }
    public function HospitalProfile(){
        return view('backend.HospitalProfile');
    }

    public function Blog(){
        return view('backend.Blog');
    }
    /*Show category page*/
    public function Category(){
        $show = Category::all();
        return view('backend.Category')->with(['show'=>$show]);
    }

    /*Add NEW CATEGORY*/
    public function AddCategory(Request $request){
        $all = $request->all();
        $add = Category::create($all);
        return back()->with('success','You have successfully created a category');
    }

    /*Update CATEGORY*/
    public function UpdateCategory(Request $request){
        $all = $request->all();
        $id = $request['id'];
        $update = Category::find($id);
        $update->name = $request['name'];
        $update->description = $request['description'];
        $update->save();
        return back()->with('success','You have successfully updated category');
    }

    /*Delete CATEGORY*/
    public function DeleteCategory(Request $request){
        $id = $request['id'];
        $delete  = Category::find($id);
        $delete->delete();
        return back()->with('success','You have successfully deleted category');
    }

    /*Show Tags page*/
    public function Tags(){
        $show = Tags::all();
        return view('backend.Tags')->with(['show'=>$show]);
    }

    /*Add NEW Tags*/
    public function AddTags(Request $request){
        $all = $request->all();
        $add = Tags::create($all);
        return back()->with('success','You have successfully created a tag');
    }

    /*Update Tags*/
    public function UpdateTags(Request $request){
        $all = $request->all();
        $id = $request['id'];
        $update = Tags::find($id);
        $update->name = $request['name'];
        $update->description = $request['description'];
        $update->save();
        return back()->with('success','You have successfully updated tag');
    }

    /*Delete Tags*/
    public function DeleteTags(Request $request){
        $id = $request['id'];
        $delete  = Tags::find($id);
        $delete->delete();
        return back()->with('success','You have successfully deleted tag');
    }


    /*Show New blog page*/
    public function NewPost(){
        $listcategory = Category::all();
        $listtags = Tags::all();
        return view('backend.NewBlog')->with(['listcategory'=>$listcategory,'listtags'=>$listtags]);
    }

    /*Show add New blog page*/
    public function AddPost(Request $request){
        $all = $request->all();
        $add_post = new BlogPost();

        $image = $request->file('post_featured_image');
        $new_name = rand() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('/frontend/assets/images/820w'), $new_name);
        $add_post->post_featured_image = $new_name;
        $add_post->post_title = $request['post_title'];
        $add_post->post_details = $request['post_details'];
        $add_post->save();

        $last_id = $add_post->id;

        foreach ($request['category_id'] as $key => $value){
            $add_post_cat = new BlogCategories();
            $add_post_cat->category_id = $request['category_id'][$key];
            $add_post_cat->post_id = $last_id;
            $add_post_cat->save();
        }
        foreach ($request['tag_id'] as $key => $value){
            $add_gallery = new BlogTags();
            $add_gallery->tag_id = $request['tag_id'][$key];
            $add_gallery->post_id = $last_id;
            $add_gallery->save();
        }

        return back()->with('success','You have successfully published a new blog');
    }


    /*SHOW ALL BLOG POST*/
    public function AllBlog(){
        $all_blog = BlogPost::all();
        foreach ($all_blog as $post){
            $this->ShowCategories($post);
            $this->ShowTags($post);
            $post['Categories'] = $this->ShowCategories($post);
            $post['Tags'] = $this->ShowTags($post);
        }
//        dd($all_blog);
        return view('backend.AllBlog')->with(['all_blog'=>$all_blog]);
    }
    public function ShowCategories($post){
        $show = BlogCategories::where('post_id',$post->id)
            ->join('categories', 'categories.id', '=', 'blog_categories.category_id')
            ->select('blog_categories.*', 'categories.name')
            ->get();
        return $show->toArray();
    }
    public function ShowTags($post){
        $show = BlogTags::where('post_id',$post->id)
            ->join('tags', 'tags.id', '=', 'blog_tags.tag_id')
            ->select('blog_tags.*', 'tags.name')
            ->get();
        return $show->toArray();
    }

}
