<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model
{
    protected $table = "blog_posts";
    protected $fillable = ['id','post_title','post_details','post_featured_image'];
}
