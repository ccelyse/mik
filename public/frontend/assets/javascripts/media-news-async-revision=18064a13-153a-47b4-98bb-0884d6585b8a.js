var dataurl = $j(location).attr('href');

$j(document).ready(function() {
	
currWidth = viewport().width;
var catVal = $j('#contentwrapper ul.media-cat li:eq(0) a').attr('href');

getAsyncDataCat(catVal, ".async-content-wrapper");

/*------- Tabs click starts ----------*/  

$j(document).on("click", ".newstab ul li a", function(e) {
if(!($j(this).parent().hasClass('current')))
{
      thisSelec = $j(this).children('span').text();      
      $j('.newstab ul li a').parent('li').removeClass('current');
      $j(this).parent('li').addClass('current');
      getAsyncDataYear(thisSelec, ".async-content-wrapper");
}  //  End of if
e.preventDefault();
});

$j(document).on("click", ".filter-tab ul.media-cat li a", function(e) {   
if(!($j(this).parent().hasClass('current')))
{   
      $j('.filter-tab ul.media-cat li').removeClass('current');
      $j(this).parent('li').addClass('current');
      dataurl = $j(location).attr('href');
      catVal = $j(this).attr('href');      
      getAsyncDataCat(catVal, ".async-content-wrapper");
}  //  End of if
e.preventDefault();
});  //  End of click fn

});  // End of document ready

function getAsyncDataYear(year, containerName) {

dataurl = $j(location).attr('href');

	$j(containerName).html('<img class="loader" alt="Loading..." src="/~/media/Images/A/Asos-V2/css/loader.gif">');
    var currentcat = $j(".filter-tab ul li.current a").attr("href");
    
    if (dataurl.indexOf('?') < 0) {
          dataurl =  location.href + "?async=1&cat=" + currentcat +  "&year=" + year;
    }else {
      dataurl =  location.href + "&async=1&cat=" + currentcat +  "&year=" + year;
    }
    dataurl = encodeURI(dataurl);
    //alert(dataurl);
    $j(containerName).load(dataurl, function() {
news_loadMore_Pagination('.pagination', '.news-item', '.news-landing > .news-item:last', 'Load More');
if($j('.news-landing .news-item').length > 0)
{
$j('.news-landing').imagesLoaded(function() {
if(currWidth <= 768 )
{
fixPos(".news-landing .news-item", 2, ".news-landing", ".news-item");
//news_loadMore_Pagination('.pagination', '.news-item', '.news-landing > .news-item:last', 'Load More');
}else
{
fixPos(".news-landing .news-item", 3, ".news-landing", ".news-item");
//news_loadMore_Pagination('.pagination', '.news-item', '.news-landing > .news-item:last', 'Load More');
}
});
$j(window).resize();
}
else{
        $j('.news-landing').height('auto');
      }

$j(containerName).hide();
$j(containerName).fadeIn();

/* ------------------ File extension to UPPERCASE in title attribute ------------------ */
  $j("a.media-links").each(function () {
    var tmpTxt = $j(this).attr("title").split(",");
    if (tmpTxt.length > 1) {
      tmpTxt[0] = tmpTxt[0].toUpperCase();
    }
    $j(this).attr("title", tmpTxt.join());
  });
  $j("a.media-links>img").each(function () {
    var tmpTxt = $j(this).attr("alt").split(",");
    if (tmpTxt.length > 1) {
      tmpTxt[1] = tmpTxt[1].toUpperCase();
    }
    $j(this).attr("alt", tmpTxt.join());
  });
/* ------------------ <END> File extension to UPPERCASE in title attribute ------------ */

 }); //  End of load fn    
    
    $j(containerName).addClass('ready');
}
	
function getAsyncDataCat(catVal, containerName) {

	$j(containerName).html('<img class="loader" alt="Loading..." src="/~/media/Images/A/Asos-V2/css/loader.gif">');
    
    if (dataurl.indexOf('?') < 0) {
      dataurl =  location.href + "?async=1&cat=" + catVal + "&year=" + $j('#contentwrapper .newstab ul li.current span').text();
      
    } else {
      dataurl =  location.href + "&async=1&cat=" + catVal + "&year=" + $j('#contentwrapper .newstab ul li.current span').text();
      
    }
    dataurl = encodeURI(dataurl);
    //alert(dataurl);
    $j(containerName).load(dataurl, function() {
news_loadMore_Pagination('.pagination', '.news-item', '.news-landing > .news-item:last', 'Load More');
if($j('.news-landing .news-item').length > 0)
{
$j('.news-landing').imagesLoaded(function() {
if(currWidth <= 768 )
{
fixPos(".news-landing .news-item", 2, ".news-landing", ".news-item");
//news_loadMore_Pagination('.pagination', '.news-item', '.news-landing > .news-item:last', 'Load More');
}else
{
fixPos(".news-landing .news-item", 3, ".news-landing", ".news-item");
//news_loadMore_Pagination('.pagination', '.news-item', '.news-landing > .news-item:last', 'Load More');
}
});
$j(window).resize();
}
else{
        $j('.news-landing').height('auto');
      }

$j(containerName).hide();
$j(containerName).fadeIn();

/* ------------------ File extension to UPPERCASE in title attribute ------------------ */
  $j("a.media-links").each(function () {
    var tmpTxt = $j(this).attr("title").split(",");
    if (tmpTxt.length > 1) {
      tmpTxt[0] = tmpTxt[0].toUpperCase();
    }
    $j(this).attr("title", tmpTxt.join());
  });
  $j("a.media-links>img").each(function () {
    var tmpTxt = $j(this).attr("alt").split(",");
    if (tmpTxt.length > 1) {
      tmpTxt[1] = tmpTxt[1].toUpperCase();
    }
    $j(this).attr("alt", tmpTxt.join());
  });
/* ------------------ <END> File extension to UPPERCASE in title attribute ------------ */


}); //  End of load fn    
    
    $j(containerName).addClass('ready');
}

/*---------------------------Load More Starts----------------------------------*/
document.write('<style type="text/css">');
document.write('#loadmore{position:absolute; bottom:0px; width:100%; padding:20px 0;} #loadmore a{margin:0 auto; width:212px; border:2px solid #5aa395; font-size:1.6rem; text-transform:uppercase; text-align:center; padding:11px 0; display:block; text-decoration:none; cursor:pointer; color:#1e1e1e;} #loader,#loadedcontent{display:none; text-align:center;font-size:1.4em;line-height:20px;padding:10px 0 0;float:left;width:100%;}');
document.write('</style>');

function news_loadMore_Pagination(pagination_id, ajax_Page_id, append_id, load_Text) {
  if ($j(pagination_id).length > 0) {
    $j('<div id="loader" class="media-loader"><img class="loader" alt="Loading..." src="/~/media/Images/A/Asos-V2/css/loader.gif" /></div><div id="loadedcontent"></div><div class="loadmore-outer"><div id="loadmore" class="green-box-link"><p><a href="#"><strong><span data-hover="Load More">Load More</span></strong></a></p></div></div>').insertAfter(pagination_id);
    var nextPage_URL = $j('.media-sitesearchpagination li.pagerlink.pageloaded:last').next('.pagerlink').children('a').attr('href');
        
    //alert(nextPage_URL);
    $j(pagination_id).hide();
  }
  
  if (typeof nextPage_URL === 'undefined') {
    //alert("Finish loading");
    $j(pagination_id).siblings('#loadmore').hide();
  }
  $j(document).off('click', "#loadmore a");
  $j(document).on('click', "#loadmore a", function(e) {
    e.preventDefault();
    $j('.media-sitesearchpagination li.pagerlink.pageloaded:last').next('.pagerlink').addClass('pageloaded');
    $j('#loader').fadeIn();
    $j('#loadedcontent').html('');
    finalURL = encodeURI(nextPage_URL);
    
    $j("#loadedcontent").load(finalURL + ' ' + ajax_Page_id, function() {
      $j('#loader').fadeOut();
      
      $j($j('#loadedcontent').html()).insertAfter(append_id);
      $j('#loadedcontent').html('');
      
setTimeout(function(){
if($j('.news-landing .news-item').length > 0)
{
$j('.news-landing').imagesLoaded(function() {
if(currWidth <= 768 )
{
fixPos(".news-landing .news-item", 2, ".news-landing", ".news-item");
//news_loadMore_Pagination('.pagination', '.news-item', '.news-landing > .news-item:last', 'Load More');
}else
{
fixPos(".news-landing .news-item", 3, ".news-landing", ".news-item");
//news_loadMore_Pagination('.pagination', '.news-item', '.news-landing > .news-item:last', 'Load More');
}
});
$j(window).resize();
}
else{
        $j('.news-landing').height('auto');
      }

},500);

//});

/* ------------------ File extension to UPPERCASE in title attribute ------------------ */
  $j("a.media-links").each(function () {
    var tmpTxt = $j(this).attr("title").split(",");
    if (tmpTxt.length > 1) {
      tmpTxt[0] = tmpTxt[0].toUpperCase();
    }
    $j(this).attr("title", tmpTxt.join());
  });
  $j("a.media-links>img").each(function () {
    var tmpTxt = $j(this).attr("alt").split(",");
    if (tmpTxt.length > 1) {
      tmpTxt[1] = tmpTxt[1].toUpperCase();
    }
    $j(this).attr("alt", tmpTxt.join());
  });
/* ------------------ <END> File extension to UPPERCASE in title attribute ------------ */


});
    
    nextPage_URL = $j('.media-sitesearchpagination li.pagerlink.pageloaded:last').next('.pagerlink').children('a').attr('href');
	
    if (typeof nextPage_URL === 'undefined') {
      //alert("Finish loading");
      $j('#loadmore').hide();
    }
  });
}

/*----------------------------Load More ends-------------------------------------*/