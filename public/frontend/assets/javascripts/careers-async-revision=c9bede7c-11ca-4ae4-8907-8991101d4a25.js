var dataurl = $j(location).attr('href');
var vid;

$j(document).ready(function() {
  
  var catVal = $j('#contentwrapper ul.leadership-cat li:eq(0) a').attr('href');
  
  $j('#contentwrapper ul.leadership-cat li:eq(0)').addClass('current');
  $j('.leadership-landing .intro-text:eq(0)').addClass('current');
  
  $j(".leadership-wrapper .bod-item:odd").addClass("col-sm-7").removeClass("col-sm-5");
  $j(".leadership-wrapper .bod-item:odd").addClass("col-md-5").removeClass("col-md-4");
  
  /*$j('.bod-inner').hover(function() {
    $j(this).find('.black-spacer').stop(true, true).fadeToggle();
    $j(this).find('.bod-inner-cntnt .arw-btm').stop(true, true).slideToggle();
  });*/ //  End of hover fn
  /*
    fixPos(".leadership-wrapper .bod-item", 2, ".leadership-wrapper", ".bod-item");
*/
  /*--  leadership overlay starts  --*/
  
  $j(document).off("click", ".bod-inner");
  $j(document).on("click", ".bod-inner", function(e) {
    
    var bid = $j(this).attr('name-attr');
    dataurl = $j(location).attr('href');
    vid = $j(this).attr('vid-attr');
    
        
    if (dataurl.indexOf('?') < 0) {
      var burl = dataurl + "?ip3Rendering=Leadership Details Careers Async&async=1&bid=" + bid;
    } else {
      var burl = dataurl + "&ip3Rendering=Leadership Details Careers Async&async=1&bid=" + bid;
    }
    
    burl = encodeURI(burl);
    
    $j('.leadership-inr').load(burl, function() {
      if (currWidth >= 768) {
        $j('.leadership-container .rcol .rcol-inn').mCustomScrollbar({
          theme: "dark"
        });
      }
      
    });
    
    $j('.leadership-inr').animate({
      top: '0'
    }, 1000, "easeInOutCirc", function(){
$j('.overlay-controls').fadeIn('slow');
$j('body').addClass('no-scroll');
disableScrolling();
});
    
    $j('.para-pager').fadeOut();
    
    $j('.leadership-container span.icon-cs-page-ico').addClass('active');
    e.preventDefault();
    
  });
  
  
  
  
  $j(document).off("click", ".leadership-container span.icon-movie-ico");
    $j(document).on("click", ".leadership-container span.icon-movie-ico", function(e) {
      
      if (currWidth < 768) {
        $j('.leadership-container').addClass('mob-vid-played');
      }
      
      if (!$j(this).hasClass('active')) {
        $j(".leadership-vid-wrp").animate({
          top: '0'
        }, 1000, "easeInOutCirc");
        $j('.inner-bod-tabs span').removeClass('active');
        $j(this).addClass('active');
        $j('body').addClass('no-scroll');
		disableScrolling();
      
      setTimeout(function() {
        $j("<div id='videoPlayer1' class='inv_playerContainer' inv_videoId='"+vid+"' inv_playerType='singleplayer'></div>").insertBefore(".place-holder");
        
      }, 1500);
      
      return false;
       }
    });
    
    $j(document).off("click", ".leadership-container span.icon-cs-page-ico");
    $j(document).on("click", ".leadership-container span.icon-cs-page-ico", function(e) {

//$j('body').removeClass('no-scroll');

      if (!$j(this).hasClass('active')) {
        $j(".leadership-vid-wrp").animate({
          top: '-1000%'
        }, 1000, "easeInOutCirc",function(){
         
        });
        $j('.inner-bod-tabs span').removeClass('active');
        $j(this).addClass('active');        
      }
      
      if ($j('#videoPlayer1').length > 0) {
      
      $j('#videoPlayer1').empty();
      $j('#videoPlayer1').remove();
      
    }
      
      if (currWidth < 768) {
        $j('.leadership-container').removeClass('mob-vid-played');
      }
      
      return false;
      
    });
    
    $j(document).off("click", ".leadership-container span.mob-vid-close");
    $j(document).on("click", ".leadership-container span.mob-vid-close", function(e) {
      
      $j(".leadership-vid-wrp").animate({
        top: '-1000%'
      }, 1000, "easeInOutCirc",function(){
       
      });
      
      $j('.inner-bod-tabs span').removeClass('active');
      $j(this).addClass('active');
      $j('body').addClass('no-scroll');
	  disableScrolling();
      
     if ($j('#videoPlayer1').length > 0) {
      
      $j('#videoPlayer1').empty();
      $j('#videoPlayer1').remove();
      
    }
      
      if (currWidth < 768) {
        $j('.inner-bod-tabs span.icon-cs-page-ico').addClass('active');
        $j('.leadership-container').removeClass('mob-vid-played');
      }
      
      return false;
      
    });
  
  
  
  
  
  
  
  
  
  
  $j(document).on("click", ".bod-close", function(e) {
    $j('body').removeClass('no-scroll');
	enableScrolling();
    if (currWidth < 768) {
      $j('.leadership-container').removeClass('mob-vid-played');
    }
    $j('.para-pager').fadeIn();
    $j('.bod-overlay').animate({
      top: '-1000%'
    }, 1000, "easeInOutCirc");
    $j('.inner-bod-tabs span').removeClass('active');
    $j('.overlay-controls').fadeOut('slow');
        
   if ($j('#videoPlayer1').length > 0) {
      
      $j('#videoPlayer1').empty();
      $j('#videoPlayer1').remove();
      
    }
    
  });
  
  /*--  leadership overlay ends  --*/
   
}); // End of document ready