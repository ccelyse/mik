var dataurl = $j(location).attr('href');
var vid;

/*$j(window).load(function() {
fixPos(".leadership-wrapper .bod-item", 2, ".leadership-wrapper", ".bod-item");
});*/

$j(document).ready(function() {
  
  var catVal = $j('#contentwrapper ul.leadership-cat li:eq(0) a').attr('href');
  
  $j('#contentwrapper ul.leadership-cat li:eq(0)').addClass('current');
  $j('.leadership-landing .intro-text:eq(0)').addClass('current');
  
  $j(".leadership-wrapper .bod-item:odd").addClass("col-sm-7").removeClass("col-sm-5");
  $j(".leadership-wrapper .bod-item:odd").addClass("col-md-6").removeClass("col-md-5");
  $j('.leadership-landing .fullcol-con:eq(0)').addClass('current');
  /*$j('.bod-inner').hover(function() {
    $j(this).find('.black-spacer').stop(true, true).fadeToggle();
    $j(this).find('.bod-inner-cntnt .arw-btm').stop(true, true).slideToggle();
  });*/ //  End of hover fn
  
    /*fixPos(".leadership-wrapper .bod-item", 2, ".leadership-wrapper", ".bod-item");*/

  /*--  leadership overlay starts  --*/
  
  $j(document).off("click", ".bod-inner");
  $j(document).on("click", ".bod-inner", function(e) {
    
    var bid = $j(this).attr('name-attr');
    dataurl = $j(location).attr('href');
    vid = $j(this).attr('vid-attr');
    
    if (dataurl.indexOf('?') < 0) {
      var burl = dataurl + "?ip3Rendering=Leadership Details Async&async=1&bid=" + bid;
    } else {
      var burl = dataurl + "&ip3Rendering=Leadership Details Async&async=1&bid=" + bid;
    }
    
    burl = encodeURI(burl);
    
    $j('.leadership-inr').load(burl, function() {
      if (currWidth >= 768) {
        $j('.leadership-container .rcol .rcol-inn').mCustomScrollbar({
          theme: "dark"
        });
      }
      
    });
    
    $j('.leadership-inr').animate({
      top: '0'
    }, 1000, "easeInOutCirc", function(){ 

$j('.overlay-controls').fadeIn('slow');
    $j('body').addClass('no-scroll');
	disableScrolling();
    
    });

    $j('.leadership-container span.icon-cs-page-ico').addClass('active');
    e.preventDefault();
    
  });
  
  $j(document).on("click", ".bod-close", function(e) {

$j('body').removeClass('no-scroll');
enableScrolling();
    
    if (currWidth < 768) {
      $j('.leadership-container').removeClass('mob-vid-played');
    }
    
    $j('.bod-overlay').animate({
      top: '-1000%'
    }, 1000, "easeInOutCirc",function(){
    });

    $j('.inner-bod-tabs span').removeClass('active');
    $j('.overlay-controls').fadeOut('slow');
     
    if ($j('#videoPlayer1').length > 0) {
      
      $j('#videoPlayer1').empty();
      $j('#videoPlayer1').remove();
      
    }
    
  });
  
  /*--  leadership overlay ends  --*/
  
  /*------- Tabs click starts ----------*/
  
  $j(document).on("click", ".filter-tab ul.leadership-cat li a", function(e) {
    
    if (!($j(this).parent().hasClass('current'))) {
      $j('.filter-tab ul.leadership-cat li').removeClass('current');
      $j(this).parent('li').addClass('current');
      dataurl = $j(location).attr('href');
      catVal = $j(this).attr('href');
      getBodAsyncData(catVal, ".leadership-wrapper");
      
      $j('.leadership-landing .intro-text').removeClass('current');
      $j('.leadership-landing .intro-text.' + catVal).addClass('current');
      $j('.leadership-landing .fullcol-con').removeClass('current');
      $j('.leadership-landing .fullcol-con.' + catVal).addClass('current');
      
    } //  End of if
    
    e.preventDefault();
    
  }); //  End of click fn
  
  getBodAsyncData(catVal, ".leadership-wrapper");
  
}); // End of document ready

function getBodAsyncData(catVal, containerName) {
  
  $j(containerName).html('<img class="loader" alt="Loading..." src="/~/media/Images/A/Asos-V2/css/loader.gif">');
  
  if (dataurl.indexOf('?') < 0) {
    dataurl = dataurl + "?ip3Rendering=Leadership Async" + "&async=1&cat=" + catVal;
  } else {
    dataurl = dataurl + "&ip3Rendering=Leadership Async" + "&async=1&cat=" + catVal;
  }
  dataurl = encodeURI(dataurl);
  
  $j(containerName).load(dataurl, function() {
    
    leadership_loadMore_Pagination('.pagination', '.bod-item', '.leadership-wrapper > .bod-item:last', 'Load More');
    
    setTimeout(function() {
      
      $j(".leadership-wrapper .bod-item:odd").addClass("col-sm-7").removeClass("col-sm-5");
     // $j(".leadership-wrapper .bod-item:odd").addClass("col-md-5").removeClass("col-md-4");
     $j(".leadership-wrapper .bod-item:odd").addClass("col-md-6").removeClass("col-md-5");
      if($j('.bod-item').length>0)
      {
      
	  $j('.leadership-wrapper').imagesLoaded( function() {
      fixPos(".leadership-wrapper .bod-item", 2, ".leadership-wrapper", ".bod-item");
      });
$j(window).resize();
      }
      else{
        $j(".leadership-wrapper").height('auto');
      }
      /*--  Bod box hover height starts  --*/
      if (currWidth > 1030) {
        $j('.bod-item .bod-inner').each(function() {
          var bih = 0;
          bih = $j(this).height();
          nbih = (bih * 31) / 100;
          $j(this).find('.arw-btm').css('padding-bottom', nbih);
        });
      } else {
        $j('.bod-item .bod-inner').each(function() {
          var bih = 0;
          bih = $j(this).height();
          nbih = (bih * 10) / 100;
          $j(this).find('.arw-btm').css('padding-bottom', nbih);
        });
      }
      /*--  Bod box hover height ends  --*/
      
    }, 500);
    
    $j(containerName).hide();
    /*$j('.bod-inner').hover(function() {
      $j(this).find('img').attr('transform: scale(1.1)');
      $j(this).find('.black-spacer').stop(true, true).fadeToggle();
      $j(this).find('.bod-inner-cntnt .arw-btm').stop(true, true).slideToggle('slow');
    });*/
    
    $j(containerName).fadeIn();
    
    /*--  leadership overlay starts  --*/
    $j(document).off("click", ".bod-inner");
    $j(document).on("click", ".bod-inner", function(e) {
   
      var bid = $j(this).attr('name-attr');
      dataurl = $j(location).attr('href');
      vid = $j(this).attr('vid-attr');
      
           
      if (dataurl.indexOf('?') < 0) {
        var burl = dataurl + "?ip3Rendering=Leadership Details Async&async=1&bid=" + bid;
      } else {
        var burl = dataurl + "&ip3Rendering=Leadership Details Async&async=1&bid=" + bid;
      }
      
      burl = encodeURI(burl);
      
      $j('.leadership-inr').load(burl, function() {
        if (currWidth >= 768) {
          
          $j('.leadership-container .rcol .rcol-inn').mCustomScrollbar();
        }
      });
      
      $j('.leadership-inr').animate({
        top: '0'
	  }, 1000, "easeInOutCirc", function(){ 

$j('.overlay-controls').fadeIn('slow');
$j('body').addClass('no-scroll');
      
      });     

      $j('.leadership-container span.icon-cs-page-ico').addClass('active');
      
      e.preventDefault();
      
    });
    
    $j(document).off("click", ".leadership-container span.icon-movie-ico");
    $j(document).on("click", ".leadership-container span.icon-movie-ico", function(e) {
      
      if (currWidth < 768) {
        $j('.leadership-container').addClass('mob-vid-played');
      }
      
      if (!$j(this).hasClass('active')) {
        $j(".leadership-vid-wrp").animate({
          top: '0'
        }, 1000, "easeInOutCirc");
        $j('.inner-bod-tabs span').removeClass('active');
        $j(this).addClass('active');
        $j('body').addClass('no-scroll');
		disableScrolling();
      
      setTimeout(function() {
        $j("<div id='videoPlayer1' class='inv_playerContainer' inv_videoId='"+vid+"' inv_playerType='singleplayer'></div>").insertBefore(".place-holder");
    $j("#videoPlayer1").InvPlayer();
        
      }, 1500);
      
      return false;
       }
    });
    
    $j(document).off("click", ".leadership-container span.icon-cs-page-ico");
    $j(document).on("click", ".leadership-container span.icon-cs-page-ico", function(e) {

//$j('body').removeClass('no-scroll');
      
      if (!$j(this).hasClass('active')) {
        $j(".leadership-vid-wrp").animate({
          top: '-1000%'
        }, 1000, "easeInOutCirc",function(){
         
        });
        $j('.inner-bod-tabs span').removeClass('active');
        $j(this).addClass('active');        
      }
      
      if ($j('#videoPlayer1').length > 0) {
      
      $j('#videoPlayer1').empty();
      $j('#videoPlayer1').remove();
      
    }
      
      if (currWidth < 768) {
        $j('.leadership-container').removeClass('mob-vid-played');
      }
      
      return false;
      
    });
    
    $j(document).off("click", ".leadership-container span.mob-vid-close");
    $j(document).on("click", ".leadership-container span.mob-vid-close", function(e) {
      
      $j(".leadership-vid-wrp").animate({
        top: '-1000%'
      }, 1000, "easeInOutCirc",function(){
       
      });
      
      $j('.inner-bod-tabs span').removeClass('active');
      $j(this).addClass('active');
      $j('body').addClass('no-scroll');
	  disableScrolling();
      
     if ($j('#videoPlayer1').length > 0) {
      
      $j('#videoPlayer1').empty();
      $j('#videoPlayer1').remove();
      
    }
      
      if (currWidth < 768) {
        $j('.inner-bod-tabs span.icon-cs-page-ico').addClass('active');
        $j('.leadership-container').removeClass('mob-vid-played');
      }
      
      return false;
      
    });
    
    $j(document).off("click", ".bod-close");
    $j(document).on("click", ".bod-close", function(e) {
      
$j('body').removeClass('no-scroll');
enableScrolling();

      if (currWidth < 768) {
        $j('.leadership-container').removeClass('mob-vid-played');
      }
      
      $j('.bod-overlay').animate({
        top: '-1000%'
      }, 500, "easeInOutCirc",function(){
      
      $j('.leadership-inr').html('');      

      });

      $j('.inner-bod-tabs span').removeClass('active');
      $j('.overlay-controls').fadeOut('slow');      

     if ($j('#videoPlayer1').length > 0) {
      
      $j('#videoPlayer1').empty();
      $j('#videoPlayer1').remove();
      
    }
      
    });
    
    /*--  leadership overlay ends  --*/
    
  }); //  End of load fn    
  
  $j(containerName).addClass('ready');
  
} //  End of GetAsyncData fn

/*---------------------------Load More Starts----------------------------------*/
document.write('<style type="text/css">');
document.write('#loadmore{position:absolute; bottom:0px; width:100%; padding:20px 0;} #loadmore a{margin:0 auto; width:212px; border:2px solid #5aa395; font-size:1.6rem; text-transform:uppercase; text-align:center; padding:11px 0; display:block; text-decoration:none; cursor:pointer; color:#1e1e1e;} #loader,#loadedcontent{display:none; text-align:center;font-size:1.4em;line-height:20px;padding:10px 0 0;float:left;width:100%;}');
document.write('</style>');

function leadership_loadMore_Pagination(pagination_id, ajax_Page_id, append_id, load_Text) {
  if ($j(pagination_id).length > 0) {
    $j('<div id="loader" class="media-loader"><img class="loader" alt="Loading..." src="/~/media/Images/A/Asos-V2/css/loader.gif" /></div><div id="loadedcontent"></div><div class="loadmore-outer"><div id="loadmore" class="green-box-link"><p><a href="#"><strong><span data-hover="Load More">Load More</span></strong></a></p></div></div>').insertAfter(pagination_id);
    var nextPage_URL = $j('.media-sitesearchpagination li.pagerlink.pageloaded:last').next('.pagerlink').children('a').attr('href');
    
    $j(pagination_id).hide();
  }
  
  if (typeof nextPage_URL === 'undefined') {
    $j(pagination_id).siblings('#loadmore').hide();
  }
  $j(document).off('click', "#loadmore a");
  $j(document).on('click', "#loadmore a", function(e) {
    e.preventDefault();
    $j('.media-sitesearchpagination li.pagerlink.pageloaded:last').next('.pagerlink').addClass('pageloaded');
    $j('#loader').fadeIn();
    $j('#loadedcontent').html('');
    finalURL = encodeURI(nextPage_URL);
    
    $j("#loadedcontent").load(finalURL + ' ' + ajax_Page_id, function() {
      $j('#loader').fadeOut();
      
      $j($j('#loadedcontent').html()).insertAfter(append_id);
      $j('#loadedcontent').html('');
      
      setTimeout(function() {
        $j(".leadership-wrapper .bod-item:odd").addClass("col-sm-7").removeClass("col-sm-5");
        $j(".leadership-wrapper .bod-item:odd").addClass("col-md-6").removeClass("col-md-5");
       if($j('.bod-item').length>0)
      {
      
	  $j('.leadership-wrapper').imagesLoaded( function() {
      fixPos(".leadership-wrapper .bod-item", 2, ".leadership-wrapper", ".bod-item");
      });
$j(window).resize();
      }
      else{
        $j(".leadership-wrapper").height('auto');
      }
      }, 300);
      
      /*$j('.bod-inner').unbind('mouseenter mouseleave');
      $j('.bod-inner').hover(function() {
        $j(this).find('img').attr('transform: scale(1.1)');
        $j(this).find('.black-spacer').stop(true, true).fadeToggle();
        $j(this).find('.bod-inner-cntnt .arw-btm').stop(true, true).slideToggle('slow');
      });*/
      
      
      /*--  Bod box hover height starts  --*/
      if (currWidth > 1030) {
        $j('.bod-item .bod-inner').each(function() {
          var bih = 0;
          bih = $j(this).height();
          nbih = (bih * 31) / 100;
          $j(this).find('.arw-btm').css('padding-bottom', nbih);
        });
      } else {
        $j('.bod-item .bod-inner').each(function() {
          var bih = 0;
          bih = $j(this).height();
          nbih = (bih * 10) / 100;
          $j(this).find('.arw-btm').css('padding-bottom', nbih);
        });
      }
      /*--  Bod box hover height ends  --*/
      
    });
    
    nextPage_URL = $j('.media-sitesearchpagination li.pagerlink.pageloaded:last').next('.pagerlink').children('a').attr('href');
    
    if (typeof nextPage_URL === 'undefined') {
      $j('#loadmore').hide();
    }
  });
}

/*----------------------------Load More ends-------------------------------------*/

    
    
    