var currBrowserTitle;
var thisSelec, prevSelec = '';

$j(document).ready(function() {

/*------- Tabs click starts ----------*/    
    
    $j(document).on("click", ".newstab.asynctab ul li a", function(e) {
      thisSelec = $j(this);
      
      e.preventDefault();
      $j('.newstab.asynctab ul li a').parent('li').removeClass('current');
      $j(this).parent('li').addClass('current');
      getAsyncData(thisSelec, ".async-content-wrapper");
    });
    
/*------- Tabs click ends ----------*/
  
});

function getAsyncData(selector, containerName) {
  if(selector.length>2 || $j(selector).attr("href") === undefined){
    $j(containerName).hide();
    $j(containerName).removeClass('ready');
    
  }
  else{
    var thisSelec = selector;
    
    $j(containerName).html('<div class="loader-wrapper"><img class="loader" alt="Loader" src="/~/media/Images/A/Asos-V2/css/loader.gif"></div>');
    
    var dataurl = $j(selector).attr("href");
    if (dataurl.indexOf('?') < 0) {
      dataurl = dataurl + "?async=1";
    } else {
      dataurl = dataurl + "&async=1";
    }
    dataurl = encodeURI(dataurl);
    
    $j(containerName).load(dataurl, function() {
      
$j('.rad-responsive').each(function(count) {
            $j(this).find("tr").addClass("ignore-header");
            $j(this).InvResponsiveTable({
                prefix: 'inv',
                showTableHeader: false
            });
        });
        $j(".inv-responsive-data-td:empty").parent().hide();
      
    }); //  End of load fn
    
    
    $j(containerName).show();
    $j(containerName).addClass('ready');
  }
  
}