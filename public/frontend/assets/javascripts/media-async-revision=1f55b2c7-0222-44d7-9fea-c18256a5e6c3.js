var dataurl = $j(location).attr('href');

$j(document).ready(function() {

  
  
  var vid;
  
  $j(".photos-wrapper .photo-item:odd").addClass("col-sm-7").removeClass("col-sm-5");
  
  /*$j('.photo-inner').hover(function(){
    $j(this).find('.black-spacer').stop(true,true).fadeToggle();
    $j(this).find('.photo-inner-cntnt').stop(true,true).slideToggle();
  });*/
  
 /* if($j('.photos-wrapper .photo-item').length > 0)
  {
    fixPos(".photos-wrapper".photos-wrapper .photo-item", 2, ".photos-wrapper", ".photo-item");
  }
  */
  var catVal = $j('#contentwrapper ul.media-cat li:eq(0) a').attr('href');
  var catType = $j('#contentwrapper ul.media-types li:eq(0) a').attr('href');
  var op = "=";
  
  $j('#contentwrapper ul.media-cat li:eq(0), #contentwrapper ul.media-types li:eq(0)').addClass('current');
  $j('.media-gallery .intro-text:eq(0)').addClass('current');
  
  /*------- Tabs click starts ----------*/    
  $j(document).off("click", ".filter-tab ul.media-cat li a", function() {});
  $j(document).on("click", ".filter-tab ul.media-cat li a", function(e) {   
    
    if(!($j(this).parent().hasClass('current')))
    {
      $j('.filter-tab ul.media-cat li').removeClass('current');
      $j(this).parent('li').addClass('current');
      dataurl = $j(location).attr('href');
      catVal = $j(this).attr('href');

      if($j('.photo.'+catVal).attr('data-count') == 0 && $j('.filter-tab ul.media-types li.current a').text()=="Photos")
      {
        $j('.filter-tab ul.media-types li.current').hide();
        $j('.filter-tab ul.media-types li.videos').show();
        $j('.filter-tab ul.media-types li.videos a').trigger('click');
      }
      else
      {
        
        if($j('.photo.'+catVal).attr('data-count') == 0 && $j('.filter-tab ul.media-types li.current a').text() == "Videos")
        {
          $j('.filter-tab ul.media-types li:first-child').hide();
          $j('.filter-tab ul.media-types li.videos').show();
          getAsyncData(catVal, op, ".photos-wrapper");
        }
        else if($j('.video.'+catVal).attr('data-count') == 0 && $j('.filter-tab ul.media-types li.current a').text() == "Videos") 
        {
          $j('.filter-tab ul.media-types li.current').hide();
          $j('.filter-tab ul.media-types li.photos').show();
          $j('.filter-tab ul.media-types li.photos a').trigger('click');
        }
          else if($j('.video.'+catVal).attr('data-count') == 0 && $j('.filter-tab ul.media-types li.current a').text()=="Photos")
          {
            $j('.filter-tab ul.media-types li.videos').hide();
            $j('.filter-tab ul.media-types li.photos').show().css('border','none');
            getAsyncData(catVal, op, ".photos-wrapper");
          }
        
      }

      $j('.media-gallery .intro-text').removeClass('current');
      $j('.media-gallery .intro-text.'+catVal).addClass('current');
      
    }  //  End of if
    
    e.preventDefault();
    
  });  //  End of click fn
  
  $j(document).on("click", ".filter-tab ul.media-types li a", function(e) {  
    
    if(!($j(this).parent().hasClass('current')))
    {
      $j('.filter-tab ul.media-types li').removeClass('current');
      $j(this).parent('li').addClass('current');
      dataurl = $j(location).attr('href');
      catType = $j(this).attr('href');
      
      if(catType == "photos")
      {
        op = "=";
      }
      else if(catType == "videos")
      {
        op = "!=";
      }	  
      getAsyncData(catVal, op, ".photos-wrapper");
      
    }  //  End of if
    
    e.preventDefault();
    
  });  //  End of click fn
  
  
// HS: Start of filtering content type
  if($j('.photo.'+catVal).attr('data-count') == 0 && $j('.filter-tab ul.media-types li.current a').text()=="Photos")
  {
    $j('.filter-tab ul.media-types li.current').hide();
    $j('.filter-tab ul.media-types li.videos').show();
    $j('.filter-tab ul.media-types li.videos a').trigger('click');
  }
  else
  {
    
    if($j('.photo.'+catVal).attr('data-count') == 0 && $j('.filter-tab ul.media-types li.current a').text() == "Videos")
    {
      $j('.filter-tab ul.media-types li:first-child').hide();
      $j('.filter-tab ul.media-types li.videos').show();
      getAsyncData(catVal, op, ".photos-wrapper");
    }
    else if($j('.video.'+catVal).attr('data-count') == 0 && $j('.filter-tab ul.media-types li.current a').text() == "Videos") 
    {
      $j('.filter-tab ul.media-types li.current').hide();
      $j('.filter-tab ul.media-types li.photos').show();
      $j('.filter-tab ul.media-types li.photos a').trigger('click');
    }
      else if($j('.video.'+catVal).attr('data-count') == 0 && $j('.filter-tab ul.media-types li.current a').text()=="Photos")
      {
        $j('.filter-tab ul.media-types li.videos').hide();
        $j('.filter-tab ul.media-types li.photos').show().css('border','none');
        getAsyncData(catVal, op, ".photos-wrapper");
      }
    
  }
//HS: End of filtering content type

//  getAsyncData(catVal, op, ".photos-wrapper");
  
});  // End of document ready

function getAsyncData(catVal, op, containerName) {
  
  
  $j(containerName).html('<img class="loader" alt="Loading..." src="/~/media/Images/A/Asos-V2/css/loader.gif">');
  
  if (dataurl.indexOf('?') < 0) {
    dataurl = dataurl + "?async=1&cat=" + catVal + "&op=" + op;
  } else {
    dataurl = dataurl; //+ "&async=1&cat=" + catVal + "&op=" + op;
  }
  dataurl = encodeURI(dataurl);
  
  $j(containerName).load(dataurl, function() {
    
    //$j(containerName).hide();
    
    media_loadMore_Pagination('.pagination', '.photo-item', '.photos-wrapper > .photo-item:last', 'Load More');
    
    
    //setTimeout(function(){
    
      if($j('.photos-wrapper .photo-item').length > 0){
        $j('.photos-wrapper').imagesLoaded( function() {
        
          fixPos(".photos-wrapper .photo-item", 2, ".photos-wrapper", ".photo-item");
        });

$j(window).resize();

      }
      else{
        $j('.photos-wrapper').height('auto');
      }
      $j(".photos-wrapper .photo-item:odd").addClass("col-sm-7").removeClass("col-sm-5");
    //},500);
   
    
    $j(containerName).hide();
  /*  $j('.photo-inner').hover(function(){
      $j('.photo-inner').removeClass('photo-inner-hovered');
      $j(this).addClass('photo-inner-hovered');
      $j(this).find('img').attr('transform: scale(1.1)');
      $j(this).find('.black-spacer').stop(true,true).fadeToggle();
      $j(this).find('.photo-inner-cntnt').stop(true,true).slideToggle('slow');
    });
    */
    
    /*$j('.photo-inner').hover(function(){
      $j(this).addClass('photo-inner-hovered');
      $j(this).find('img').attr('transform: scale(1.1)');
      $j(this).find('.black-spacer').stop(true,true).show();
      $j(this).find('.photo-inner-cntnt').stop(true,true).slideDown('slow');
    },function(){
      $j('.photo-inner').find('.black-spacer').stop(true,true).fadeOut();
      $j('.photo-inner').find('.photo-inner-cntnt').stop(true,true).slideUp('slow');
    });
    
    
    $j(document).on('click touchstart', function () {
      $j('.photo-inner').find('img').attr('transform: scale(1)');
      $j('.photo-inner').find('.black-spacer').stop(true,true).fadeOut();
      $j('.photo-inner').find('.photo-inner-cntnt').stop(true,true).slideUp('slow');
    });*/
    
    
    $j(containerName).fadeIn();
    
    /*--  video overlay starts  --*/
    
 /*   $j(document).off('click', "a.vid");
    $j(document).on('click', "a.vid", function(e) {
      
      vid = $j(this).attr('href');
      
      $j("<div id='inv_playerContainer' class='inv_playerContainer'></div>").insertBefore(".video-container .vid-close");
      
      $j("#inv_playerContainer").embedCode({playerType:"singleplayer",videoId:vid,overlayPlay:true,autoPlay:true});
      
      if (!$j(this).hasClass('active')) {
        $j('a.vid').removeClass('active');          
        
        $j('.video-container').animate({
          top: '0'
        }, 1000, "easeInOutCirc").addClass('active');
        $j('a.vid').removeClass('active');
        
        $j(this).addClass('active');
        $j('body').addClass('no-scroll');
      }
      return false;
    });
    
    $j(document).off('click', ".vid-close");
    $j(document).on('click', ".vid-close", function(e) {
      $j('.video-container').animate({
        top: '-100%'
      }, 1000, "easeInOutCirc").removeClass('active');
      $j('a.vid').removeClass('active');
      $j('body').removeClass('no-scroll');
      
      if($j('#inv_playerContainer').length > 0)
      {
        $j("#inv_playerContainer").embedCode({stopVideo:true});
        $j('#inv_playerContainer').empty();
        $j('#inv_playerContainer').remove();
        
      }
      
    });*/
    
    /*--  Video overlay ends  --*/
    
  }); //  End of load fn    
  
  $j(containerName).addClass('ready');
  
}  //  End of GetAsyncData fn

/*---------------------------Load More Starts----------------------------------*/
document.write('<style type="text/css">');
document.write('#loadmore{position:absolute; bottom:0px; width:100%; padding:20px 0;} #loadmore a{margin:0 auto; width:212px; border:2px solid #5aa395; font-size:1.6rem; text-transform:uppercase; text-align:center; padding:11px 0; display:block; text-decoration:none; cursor:pointer; color:#1e1e1e;} #loader,#loadedcontent{display:none; text-align:center;font-size:1.4em;line-height:20px;padding:10px 0 0;float:left;width:100%;}');
document.write('</style>');

function media_loadMore_Pagination(pagination_id, ajax_Page_id, append_id, load_Text) {
  if ($j(pagination_id).length > 0) {
    $j('<div id="loader" class="media-loader"><img class="loader" alt="Loading..." src="/~/media/Images/A/Asos-V2/css/loader.gif" /></div><div id="loadedcontent"></div><div class="loadmore-outer"><div id="loadmore" class="green-box-link"><p><a href="#"><strong><span data-hover="Load More">Load More</span></strong></a></p></div></div>').insertAfter(pagination_id);
    var nextPage_URL = $j('.media-sitesearchpagination li.pagerlink.pageloaded:last').next('.pagerlink').children('a').attr('href');
    
    //alert(nextPage_URL);
    $j(pagination_id).hide();
  }
  
  if (typeof nextPage_URL === 'undefined') {
    //alert("Finish loading");
    $j(pagination_id).siblings('#loadmore').hide();
  }
  $j(document).off('click', "#loadmore a");
  $j(document).on('click', "#loadmore a", function(e) {
    e.preventDefault();
    $j('.media-sitesearchpagination li.pagerlink.pageloaded:last').next('.pagerlink').addClass('pageloaded');
    $j('#loader').fadeIn();
    $j('#loadedcontent').html('');
    finalURL = encodeURI(nextPage_URL);
    
    $j("#loadedcontent").load(finalURL + ' ' + ajax_Page_id, function() {
      $j('#loader').fadeOut();
      
      $j($j('#loadedcontent').html()).insertAfter(append_id);
      $j('#loadedcontent').html('');
      
      setTimeout(function(){
        $j(".photos-wrapper .photo-item:odd").addClass("col-sm-7").removeClass("col-sm-5");
        if($j('.photos-wrapper .photo-item').length > 0){
        $j('.photos-wrapper').imagesLoaded( function() {
        
        fixPos(".photos-wrapper .photo-item", 2, ".photos-wrapper", ".photo-item");
        });

$j(window).resize();

        }
        else{
        $j('.photos-wrapper').height('auto');
        }
      },500);
      
      /*$j('.photo-inner').unbind('mouseenter mouseleave');
      $j('.photo-inner').hover(function(){
        $j(this).find('img').attr('transform: scale(1.1)');
        $j(this).find('.black-spacer').stop(true,true).fadeToggle();
        $j(this).find('.photo-inner-cntnt').stop(true,true).slideToggle('slow');
      });*/
      
      /*$j('.photo-inner').hover(function(){
        $j(this).addClass('photo-inner-hovered');
        $j(this).find('img').attr('transform: scale(1.1)');
        $j(this).find('.black-spacer').stop(true,true).show();
        $j(this).find('.photo-inner-cntnt').stop(true,true).slideDown('slow');
      },function(){
        $j('.photo-inner').find('.black-spacer').stop(true,true).fadeOut();
        $j('.photo-inner').find('.photo-inner-cntnt').stop(true,true).slideUp('slow');
      });
      $j(document).on('click touchstart', function () {
        $j('.photo-inner').find('img').attr('transform: scale(1)');
        $j('.photo-inner').find('.black-spacer').stop(true,true).fadeOut();
        $j('.photo-inner').find('.photo-inner-cntnt').stop(true,true).slideUp('slow');
      });*/
      
      
      /*--  video overlay starts  --*/
      
     /* $j(document).off('click', "a.vid");
      $j(document).on('click', "a.vid", function(e) {
        
        vid = $j(this).attr('href');
        
        $j("<div id='inv_playerContainer' class='inv_playerContainer'></div>").insertBefore(".video-container .vid-close");
        
        $j("#inv_playerContainer").embedCode({playerType:"singleplayer",videoId:vid,overlayPlay:true,autoPlay:true});
        
        if (!$j(this).hasClass('active')) {
          $j('a.vid').removeClass('active');          
          
          $j('.video-container').animate({
            top: '0'
          }, 1000, "easeInOutCirc").addClass('active');
          $j('a.vid').removeClass('active');
          
          $j(this).addClass('active');
          $j('body').addClass('no-scroll');
        }
        return false;
      });
      
      $j(document).off('click', ".vid-close");
      $j(document).on('click', ".vid-close", function(e) {
        $j('.video-container').animate({
          top: '-100%'
        }, 1000, "easeInOutCirc").removeClass('active');
        $j('a.vid').removeClass('active');
        $j('body').removeClass('no-scroll');
        
        if($j('#inv_playerContainer').length > 0)
        {
          $j("#inv_playerContainer").embedCode({stopVideo:true});
          $j('#inv_playerContainer').empty();
          $j('#inv_playerContainer').remove();
          
        }
        
      });
      */
      /*--  Video overlay ends  --*/
      
    });
    
    nextPage_URL = $j('.media-sitesearchpagination li.pagerlink.pageloaded:last').next('.pagerlink').children('a').attr('href');
    
    
    if (typeof nextPage_URL === 'undefined') {
      //alert("Finish loading");
      $j('#loadmore').hide();
    }
  });
}

/*----------------------------Load More ends-------------------------------------*/