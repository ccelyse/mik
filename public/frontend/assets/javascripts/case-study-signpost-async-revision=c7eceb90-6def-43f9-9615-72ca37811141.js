var dataurl = $j(location).attr('href');
var vid;



$j(document).ready(function() {
  
  /*--  Case study overlay starts  --*/
  
  $j(document).on("click", ".case-study-item-inner", function(e) {
    
    var casestudyid = $j(this).attr('name-attr'); 
    dataurl = $j(location).attr('href');
    vid = $j(this).attr('vid-attr');
    var burl;
    
    
    if (dataurl.indexOf('?') < 0) {
      if ( $j(this).hasClass("video-item") ) {
        burl = dataurl + "?async=1&video=1&casestudyid=" + casestudyid;
      }else{
        burl = dataurl + "?async=1&casestudyid=" + casestudyid;
      }
      
      
      
    } else {
      
      if ( $j(this).hasClass("video-item") ) {
        burl = dataurl + "&async=1&video=1&casestudyid=" + casestudyid;
      }else{
        burl = dataurl + "&async=1&casestudyid=" + casestudyid;
      }
      
    }
    
   burl = encodeURI(burl);
    alert(burl);
    
    
    if ( $j(this).hasClass("video-item") ) {
      
      
      
      setTimeout(function() {
        
     $j("<div id='videoPlayer1' class='inv_playerContainer' inv_videoId='"+vid+"' inv_playerType='singleplayer'></div>").insertBefore(".place-holder");
    $j("#videoPlayer1").InvPlayer();
      }, 1500);
      $j('.leadership-vid-wrp').animate({
        top: '0'
      }, 1000, "easeInOutCirc");
      $j('.overlay-controls').fadeIn('slow');
      $j('.bod-close').addClass('vid-wrp-active');
      $j('body').addClass('no-scroll');
    }
    else
    {
      $j('.leadership-inr').load(burl,function(){
        
        $j('.leadership-container .rcol .rcol-inn').mCustomScrollbar({theme:"dark"});
        
      });
      $j('.leadership-inr').animate({
        top: '0'
      }, 1000, "easeInOutCirc");
      $j('.overlay-controls').fadeIn('slow');
      $j('body').addClass('no-scroll');
    }
    $j('.leadership-container span.icon-cs-page-ico').addClass('active');
    e.preventDefault();
    
  });
  
  $j(document).on("click", ".bod-close", function(e) {
    
    if($j(this).hasClass('vid-wrp-active'))
    {
      setthis = "leadership-vid-wrp";
      
    }
    else
    {
      setthis = "leadership-inr";
    }
    //console.log(setthis);
    $j('.bod-overlay.'+setthis).animate({
      top: '-1000%'
    }, 1000);
    $j('.inner-bod-tabs span').removeClass('active');
    $j('.overlay-controls').fadeOut('slow');
    $j('body').removeClass('no-scroll');
    $j('.bod-close').removeClass('vid-wrp-active');
   if ($j('#videoPlayer1').length > 0) {
      
      $j('#videoPlayer1').empty();
      $j('#videoPlayer1').remove();
      
    }
    
  });
  
  /*--  Case study overlay ends  --*/
  
  
}); // End of document ready
