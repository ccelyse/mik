var inv_mainConfiguration=
{
	//18-01-2017 Urvi : Allow File execution in local machine
	inv_local:false,

	inv_isIE8:((/MSIE (\d+\.\d+);/.test(navigator.userAgent))?new Number(RegExp.$1)==8:false),
	inv_videoJQuery:$j,

	inv_protocol:'https://', //'http://'  //'https://'  //'http://'
	inv_hostName:'viz.tools.investis.com/', //'visualisation.investis.com/'   //'viz.tools.investis.com/'    //'localhost/'   //arcelormittal.video.tf.investis.com/  //arcelomittal.tools.investis.com
	inv_playerPath:'video/videoPlayer-v.2.0-latest/',//'video/videoPlayer-v.2.0-test/'        //'video/videoPlayer-v.2.0/' 		//Work/AM/am_imagevideogallery/
	inv_videoClient:'asos',
/*	inv_videoClientFolder:'vrtest',*/
	inv_renderViewPath:'/tools/urlproxy/urlproxy.aspx?settingname=video', ///tools/urlproxy/urlproxy.aspx?settingname=video - asos proxy
	
	inv_urlVRProxy:'https://viz.tools.investis.com/video/videoPlayer-v.2.0-latest/vrtest/files/urlProxyFile.php?filePath=',
	
	/*inv_xmlPath:'',*/
	inv_cssPath:'css/videoplayer.css',
	/*inv_colorboxCssPath:'',*/
	
	inv_videoCodeReady:false,
	
	inv_facebookAppId:'',
	inv_gaType:'traditional',
	inv_gaAccount:'UA-40089914-1', //UA-40089914-1//UA-39226021-1
	inv_debugMode:false, //For debugging - Value:true/false - DefaultValue:false
	inv_startTime:new Date(),
	inv_disableBuiltInGATracking:false,
	
	inv_dateFormat:'yyyy-dd-MM',
	
	inv_modulesRequested:new Array(),
	inv_modulesLoaded:new Array(),
	inv_bypassModuleLoadLogic:false,
	
	inv_disableBuiltInDeeplinking:false
}
//18-01-2017 Urvi : Allow File execution in local machine -- start
if(window.location.host+"/"==inv_mainConfiguration.inv_hostName)
	inv_mainConfiguration.inv_local=true;

if(inv_mainConfiguration.inv_local)
{
	inv_mainConfiguration.inv_bypassModuleLoadLogic = false;
	inv_mainConfiguration.inv_gaAccount = 'UA-123-1';
	inv_mainConfiguration.inv_renderViewPath = "view/@viewFileName.html";
}
inv_mainConfiguration.inv_videoBaseURL = function()
{
	if(inv_mainConfiguration.inv_local)
		return '../';
	else
		return (inv_mainConfiguration.inv_protocol + inv_mainConfiguration.inv_hostName + inv_mainConfiguration.inv_playerPath);
}
//18-01-2017 Urvi : Allow File execution in local machine -- end
if(!inv_mainConfiguration.inv_isIE8)  	
{
	inv_mainConfiguration.inv_modules=
	{
		//18-01-2017 Urvi : Changed Centralized Files path
		paths: {
			libs: inv_mainConfiguration.inv_videoBaseURL() + "centralizedprojectfiles/js/libs/",
			models: inv_mainConfiguration.inv_videoBaseURL() + "centralizedprojectfiles/js/models/",
			controllers: inv_mainConfiguration.inv_videoBaseURL() + "centralizedprojectfiles/js/controllers/",
			clientDir: inv_mainConfiguration.inv_videoBaseURL() + inv_mainConfiguration.inv_videoClient + "/js/"
			//inv_mainConfiguration.inv_videoBaseURL() + inv_mainConfiguration.inv_videoClient
		},
		shim: {
			'clientDir/jquery.videoCode': 
			{
				deps: ['clientDir/typedarray','libs/matchMedia','clientDir/jquery.deviceDetection','libs/ga','models/jquery.gaCode','models/jquery.readXMLFile','libs/imagesloaded.min']
			},
			'controllers/jquery.videoDataCtrl': 
			{
				deps: ['models/jquery.brightcoveData','models/jquery.youtubeData']/*,'models/jquery.jsonVideoData',*/
			},
			'clientDir/jquery.singlePlayerCtrl': 
			{
				deps: ['clientDir/jquery.videoPlayerCtrl']
			},
			'clientDir/jquery.videoPlayerCtrl': 
			{
				deps: ['clientDir/jquery.vrPlayer','models/jquery.html5Player','models/jquery.youtubePlayer']
			},
			'clientDir/jquery.vrPlayer': 
			{
				deps: ['clientDir/three']
			},
			'clientDir/three': 
			{
				deps: ['clientDir/iphone-inline-video.browser','clientDir/fulltilt']
			},
			'controllers/jquery.videoControlsCtrl': 
			{
				deps: ['models/jquery.videoControls']
			},
			'controllers/jquery.soundCtrl': 
			{
				deps: ['models/jquery.soundCode','libs/jquery.mobile.custom.min']
			}
		}
	}
	inv_mainConfiguration.inv_videoJQuery(function()
	{		
		inv_mainConfiguration.inv_videoJQuery.getScript((inv_mainConfiguration.inv_bypassModuleLoadLogic==true?(inv_mainConfiguration.inv_modules.paths.clientDir + 'videoMerged.js'):(inv_mainConfiguration.inv_modules.paths.libs + 'globalScripts.js')) + '?' + inv_mainConfiguration.inv_videoJQuery.now() , function()
		{
			inv_mainConfiguration.inv_debugConsole("jquery is ready");
			//18-01-2017 Urvi : Allow File execution in local machine
			if(!inv_mainConfiguration.inv_local)
			{

			var inv_absoluteProxyFilePath=inv_mainConfiguration.inv_urlVRProxy + inv_mainConfiguration.inv_videoBaseURL() + inv_mainConfiguration.inv_videoClient;
			var inv_absoluteFilePath=inv_mainConfiguration.inv_videoBaseURL() + inv_mainConfiguration.inv_videoClient;
			
			inv_mainConfiguration.inv_cssPath = inv_absoluteFilePath + "/" + inv_mainConfiguration.inv_cssPath;
			}
			inv_mainConfiguration.inv_videoJQuery('<link rel="stylesheet" href="'+inv_mainConfiguration.inv_cssPath+'" type="text/css" />').appendTo("head").each(function() 
			{
				inv_mainConfiguration.inv_debugConsole("video css is ready");
			});
			/*inv_mainConfiguration.inv_videoJQuery('<link rel="stylesheet" href="'+inv_mainConfiguration.inv_colorboxCssPath+'" type="text/css" />').appendTo("head");*/

			inv_mainConfiguration.inv_resolveModuleNameAndLoad(['clientDir/jquery.videoCode','controllers/jquery.videoDataCtrl'],function()
			{
				inv_mainConfiguration.inv_videoDataContainer=inv_mainConfiguration.inv_videoJQuery("body").videoDataCtrl({videoDataReceived:function()
				{
					inv_mainConfiguration.inv_debugConsole("embed code and data js ready");
					inv_mainConfiguration.inv_videoCodeReady=true;			
				}
				,errorReceived:function(errString)
				{
				  	inv_mainConfiguration.inv_debugConsole('inv_videoDataContainer Error:'+errString);
				}});		
			});		
		});		
	});
}
(function($)
{
	$.fn.InvPlayer = function(params) 
	{
		var def = new $.Deferred();
		var me = $(this);
		var dataname='videoCode';
		var instance = me.data(dataname);
		if(!inv_mainConfiguration.inv_isIE8)  	
		{
			if(instance==undefined && !inv_mainConfiguration.inv_videoCodeReady)			
			{
				var checkvideoCodeReady = setInterval(function()
				{
					if(inv_mainConfiguration.inv_videoCodeReady)
					{
						clearInterval(checkvideoCodeReady);
						def.resolve(me.videoCode(params));
					}
				},500);	
			}
			else
				def.resolve(me.videoCode(params));
		}
		else
		{
			me.html("not supported");
			def.reject();
		}
		return def.promise(instance);
	};
})(inv_mainConfiguration.inv_videoJQuery);