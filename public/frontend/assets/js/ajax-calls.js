jQuery( document ).ready( function( $ ) {

	var archivo_ppp = 6;
	var page_archivo = 1;

  function breadcrumbs() {
    var text = "";

    var year = $( "#years option:selected" ).val();
    var tipo = $('.group-inline a.selected').attr('id');
    var taxonomy = $( "#categories option:selected" ).text();

    text = $( "#categories option:selected" ).text();

    if (taxonomy != "Seleccionar por tema") {
      text = taxonomy;
    } else {
      text = "archive";
    }

    if (typeof tipo !== "undefined") {
      text += " > "+tipo;  
    }

    if (year.length > 0) {
      text += " | "+year;  
    }
    return text;
  }
  
	$('#categories').on('change', function() {
	  
	  var taxonomy_id = this.value;
    var year = $( "#years option:selected" ).val();
    var tipo = $('.group-inline a.selected').attr('id');
    if (tipo == "all-types") {
      tipo = null;
    }
    page_archivo = 1;
    $('.content-newsroom .gutter-x1').html("");
    $('#pagination-button').hide();
    $('.content-newsroom #loading-message').show();
	  $.post(
      PT_Ajax.ajaxurl,
      {
        action:"ajax_category_filter",
        offset: ((page_archivo - 1) * archivo_ppp),
        ppp: archivo_ppp,
        taxonomy_id: taxonomy_id,
        year: year,
        tipo: tipo,
        nonce : PT_Ajax.nonce
      },
      function(response) {
      	if (response) {
          var regExp = new RegExp("block", "gi");
          var num = (response.match(regExp) || []).length;
        	$('.content-newsroom .gutter-x1').html(response);
          $('.content-newsroom #message').html("");
          $('.head-section .title').html(breadcrumbs());
          if (num >= archivo_ppp) {
            $('#pagination-button').show();  
          }
        } else {
          $('.head-section .title').html(breadcrumbs());
          $('.content-newsroom #message').html('No hay elementos para mostrar...');
        }
        $('.content-newsroom #loading-message').hide();
      }
    );
	})

  $('#years').on('change', function() {
    
    var year = this.value;
    var taxonomy_id = $( "#categories option:selected" ).val();
    var tipo = $('.group-inline a.selected').attr('id');
    if (tipo == "all-types") {
      tipo = null;
    }
    page_archivo = 1;
    $('.content-newsroom .gutter-x1').html("");
    $('#pagination-button').hide();
    $('.content-newsroom #loading-message').show();
    $.post(
      PT_Ajax.ajaxurl,
      {
        action:"ajax_category_filter",
        offset: ((page_archivo - 1) * archivo_ppp),
        ppp: archivo_ppp,
        taxonomy_id: taxonomy_id,
        year: year,
        tipo: tipo,
        nonce : PT_Ajax.nonce
      },
      function(response) {
        if (response) {
          var regExp = new RegExp("block", "gi");
          var num = (response.match(regExp) || []).length;
          $('.content-newsroom .gutter-x1').html(response);
          $('.content-newsroom #message').html("");
          $('.head-section .title').html(breadcrumbs());
          if (num >= archivo_ppp) {
            $('#pagination-button').show();  
          }
        } else {
          $('.head-section .title').html(breadcrumbs());
          $('.content-newsroom #message').html('No hay elementos para mostrar...');
        }
        $('.content-newsroom #loading-message').hide();
      }
    );
  })

  $('.actions-archive .group-inline a').on('click', function(e) {
    e.preventDefault();
    $('.actions-archive .group-inline a').removeClass('selected');
    $(this).addClass('selected');
    var tipo = $(this).attr('id');
    var year = $( "#years option:selected" ).val();
    var taxonomy_id = $( "#categories option:selected" ).val();
    
    if (tipo == "all-types") {
      tipo = null;
    }

    page_archivo = 1;
    $('.content-newsroom .gutter-x1').html("");
    $('#pagination-button').hide();
    $('.content-newsroom #loading-message').show();
    $.post(
      PT_Ajax.ajaxurl,
      {
        action:"ajax_category_filter",
        offset: ((page_archivo - 1) * archivo_ppp),
        ppp: archivo_ppp,
        taxonomy_id: taxonomy_id,
        year: year,
        tipo: tipo,
        nonce : PT_Ajax.nonce
      },
      function(response) {
        if (response) {
          var regExp = new RegExp("block", "gi");
          var num = (response.match(regExp) || []).length;
          $('.content-newsroom .gutter-x1').html(response);
          $('.content-newsroom #message').html("");
          $('.head-section .title').html(breadcrumbs());
          if (num >= archivo_ppp) {
            $('#pagination-button').show();  
          }
        } else {
          $('.head-section .title').html(breadcrumbs());
          $('.content-newsroom #message').html('No hay elementos para mostrar...');
        }
        $('.content-newsroom #loading-message').hide();
      }
    );
  });

  $('#pagination-button').on('click', function(e) {
    e.preventDefault();
    var tipo = $('.group-inline a.selected').attr('id');
    var year = $( "#years option:selected" ).val();
    var taxonomy_id = $( "#categories option:selected" ).val();
    $('#pagination-button').hide();  
    $('.content-newsroom #loading-message').show();

    $.post(
      PT_Ajax.ajaxurl,
      {
        action:"ajax_category_filter",
        offset: (page_archivo * archivo_ppp),
        ppp: archivo_ppp,
        taxonomy_id: taxonomy_id,
        year: year,
        tipo: tipo,
        nonce : PT_Ajax.nonce
      },
      function(response) {
        if (response) {
          var regExp = new RegExp("block", "gi");
          var num = (response.match(regExp) || []).length;
          page_archivo++;
          $('.content-newsroom .gutter-x1').append(response);
          $('.content-newsroom #message').html("");
          $('.head-section .title').html(breadcrumbs());
          if (num >= archivo_ppp) {
            $('#pagination-button').show();  
          } 
        } else {
          $('.head-section .title').html(breadcrumbs());
          $('.content-newsroom #message').html('No hay elementos para mostrar...');
        }
        $('.content-newsroom #loading-message').hide();
      }
    );
  });

});