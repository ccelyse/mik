(function($, window, document) {

  $('.igualar').matchHeight(); 
  objectFitImages();

  //slider 
    var slider_banner = $('.lista_imagenes.owl-carousel');
    slider_banner.owlCarousel({
      margin: 0,
      loop: true,
      autoHeight:true,
      items: 1,
      nav: true,
      dots: false,
      autoplay: false,
      mouseDrag: false,

    });

    var slider_banner = $('.modulo_imagen .owl-carousel');
    slider_banner.owlCarousel({
      margin: 0,
      loop: true,
      autoHeight:true,
      items: 1,
      nav: true,
      dots: false,
      autoplay: false,
      mouseDrag: false,
    });


//modal
  $('.modal-link').on('click', function(event) {
    event.preventDefault();
    $('body').css('overflow', 'hidden');
    var id = $(this).attr('data-modal');
    $('#'+id+'').addClass('modal-active');
    $('#'+id+'').next('.modal-mascara').addClass('modal-mascara-active');
    console.log(id);
  });
  $('.modal-close').on('click', function(event) {
    $('body').css('overflow', 'auto');
    $(this).parent().removeClass('modal-active');
    $(this).parent().next('.modal-mascara').removeClass('modal-mascara-active');
  });


  $(function() {

    if (window.matchMedia('(min-width: 400px)').matches) {

        $('.categoria_distribucion .label-pop1').on('hover', function() {
          $('.pop1').toggleClass('active');
        });

       $('.categoria_distribucion .label-pop2').on('hover', function() {
          $('.pop2').toggleClass('active');
        });
        
        $('.seccion-aroundtheworld .wrap-text strong').on('hover', function() {
          $('.pop').toggleClass('active');
        });
  
    } else {

        $('.categoria_distribucion .label-pop1').on('click', function() {
          $('.pop1').addClass('active');
        });
        $('.pop1').on('click', function() {
          $('.pop1').removeClass('active');
        });

       $('.categoria_distribucion .label-pop2').on('click', function() {
          $('.pop2').addClass('active');
        });
       $('.pop2').on('click', function() {
          $('.pop2').removeClass('active');
        });
        
        $('.seccion-aroundtheworld .wrap-text strong').on('click', function() {
          $('.pop').addClass('active');
        });
        $('.seccion-aroundtheworld .pop').on('click', function() {
          $('.pop').removeClass('active');
        });
     
    }


    $('.landingnav .menu-toggle').on('click', function() {
      $(this).toggleClass('is-active');
      $('#menulanding').toggleClass('toggle');
    });

    $('.header_corporativo .menu-toggle').on('click', function() {
      $(this).toggleClass('is-active');
      $('.header_corporativo').toggleClass('toggle');
    });

    
    $('.wpml-ls-legacy-dropdown-click').on('click', function() {
      $(this).toggleClass('active');
    });
   

    //---ANIMATED ON IMAGE BANNER LOAD
    $('#banner').imagesLoaded(function() {
      $('#banner').addClass('animate');
      //console.log('loaded');
    }); 



    //---ANIMATE NUMBERS
    // var $animateNums = $('.animate-num, .animate-num-slow');

    // setTimeout(function() {
    //   $animateNums.counterUp({
    //     time: 800
    //   });
    // }, 0);

     // Animate numbers
    $('.animate-num-slow').each(function() {
      var $this = $(this);
      var numero_data = $this.data('numero');
      $this.waypoint(function() {
        $this.animateNumbers(numero_data, true, 1200, "swing");
      },{offset: '50%'});
    });

    $('.scrolldetector').each(function() {

      var $this = $(this);

      var waypoint = new Waypoint({
        element: $this.get(),
        handler: function() {
          $this.addClass('animate');
        },
        offset: '10%'
      });

    });
    //---ANCLAS
    $('#menu-nav .sub-menu a').click(function(e) {
      e.preventDefault(); //evitar el eventos del enlace normal

      $('.menu-toggle').removeClass('is-active');
      $('#menulanding').removeClass('toggle');

      var strAncla = $(this).attr('href'); //id del ancla
      $('body,html').stop(true, true).animate({
        scrollTop: $(strAncla).offset().top - 60
      }, 1000);

    });
    styling_selects($('#categories'), 1);
    styling_selects($('#years'), 2);

  }); //---END READY


  function styling_selects($elm, id) {
    var $theselect = $elm;

    var htmlskin = '<div class="wrapselectlist" style="position: relative">' + '<div id="actoption_' + id + '" class="actoption">Selecciona uno</div>' + '<ul id="seleclist_' + id + '" class="seleclist" style="display:none;list-style: none;margin:0;position:absolute;z-index:10"></ul>' + '</div>';

    $theselect.after(htmlskin);

    var $seleclist = $('#seleclist_' + id);
    var $actoption = $('#actoption_' + id);

    //---duplicar lista de opciones
    $theselect.find('option').each(function() {
      $seleclist.append('<li class="' + $(this).attr('value') + '">' + $(this).text() + '</li>');
    });
    //---escribir primera opcion
    $actoption.text($seleclist.find('li').first().addClass('seleccionado').text());
    
    //console.log($seleclist.find('li').first().get(0));

    //---interacción dropdown
    $actoption.on('click', function() {
      $seleclist.slideToggle('fast');
      $actoption.toggleClass('up');
    });

    //---interacción seleccion
    $seleclist.on('click', 'li', function() {
      $('#' + $theselect.attr('id') + ' option:eq(' + $(this).index() + ')').prop('selected', true);
      $seleclist.find('li').removeClass('seleccionado');
      $(this).addClass('seleccionado');
      $actoption.text($(this).text());
      $seleclist.slideUp('fast');
      $actoption.removeClass('up');
      $elm.trigger('change');
    });

    //---una vez todo está listo ocultar real select/mostrar seudo select, ojo hacer fallback!
    $elm.attr('style', 'display:none');
  }


  // social share links
  $(function(){
    var twitterLinks = document.getElementsByClassName("twitter-sharer");
    var facebookLinks = document.getElementsByClassName("facebook-sharer");
    var linkedinLinks = document.getElementsByClassName("linkedin-sharer");

    var twitterShare = function(e) {
        e.preventDefault();
        var twitterWindow = window.open($('#twitter-sharer').attr('href'), 'twitter-popup', 'height=350,width=600');
        if(twitterWindow.focus) { twitterWindow.focus(); }
        return false;
    };

    var facebookShare = function(e) {
        e.preventDefault();
        var facebookWindow = window.open($('#facebook-sharer').attr('href'), 'facebook-popup', 'height=350,width=600');
        if(facebookWindow.focus) { facebookWindow.focus(); }
        return false;
    };

    var linkedinShare = function(e) {
        e.preventDefault();
        var linkedinWindow = window.open($('#linkedin-sharer').attr('href'), 'linkedin-popup', 'height=350,width=600');
        if(linkedinWindow.focus) { linkedinWindow.focus(); }
        return false;
    };

    for (var i = 0; i < twitterLinks.length; i++) {
      twitterLinks[i].addEventListener('click', twitterShare, false);
    }

    for (var i = 0; i < facebookLinks.length; i++) {
      facebookLinks[i].addEventListener('click', facebookShare, false);
    }

    for (var i = 0; i < linkedinLinks.length; i++) {
      linkedinLinks[i].addEventListener('click', linkedinShare, false);
    }
  })


  

}(window.jQuery, window, document));