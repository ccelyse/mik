var data  = sessionStorage.getItem('accessToken');
if(!data == data){
    window.location.href = "/";
}else{
    $(document).ready(function() {
        $.ajax({
            type: 'post',
            url: "../api/auth/me",
            dataType: 'json',
            beforeSend : function( xhr ) {
                xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
            },
            success: function (response) {
                $('#hospital_id').val(response.user.allhospital);
                $.ajax({
                    type: 'get',
                    url: "../api/auth/DashboardCount",
                    dataType: 'json',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'BEARER ' + sessionStorage.getItem('accessToken'));
                    },
                    success: function (responsecount) {
                        JSON.stringify(responsecount);
                        $('.img-profile').initial();
                        $('a[href$="#"]').addClass('active');
                        jQuery('#countusers').html('0'+responsecount.countusers);
                        jQuery('#countagent').html('0'+responsecount.countagent);
                        jQuery('#pendingQuotation').html('0'+responsecount.pendingQuotation);
                        jQuery('#approvedQuotation').html('0'+responsecount.approvedQuotation);
                        jQuery('#countstandard').html('0'+responsecount.countstandard);

                    },error: function (xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "../api/auth/ShowHospitalProfile",
                    data: {
                        id:response.user.allhospital,
                    },
                    dataType: 'json',
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader('Authorization', 'BEARER ' + sessionStorage.getItem('accessToken'));
                    },
                    success: function(responseProfile) {
                        JSON.stringify(responseProfile); //to string
                        // console.log(responseProfile[0].hospital_background);

                        // $('.note-editable').html(responseProfile[0].hospital_background);

                        $("#summernote").summernote("code", responseProfile[0].hospital_background);
                        $("#summernote2").summernote("code", responseProfile[0].mission);
                        $("#summernote3").summernote("code", responseProfile[0].vision);
                        $("#summernote4").summernote("code", responseProfile[0].core_values);
                        $("#summernote5").summernote("code", responseProfile[0].motto);
                        // $('#summernote').val(responseProfile[0].hospital_background);
                        // $('#summernote2').val(responseProfile[0].mission);
                        // $('#summernote3').val(responseProfile[0].vision);
                        // $('#summernote4').val(responseProfile[0].core_values);
                        // $('#summernote5').val(responseProfile[0].motto);

                    },
                    error: function(xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
            }, error: function (xhr, status, error) {
                if(xhr.status == 401){
                    window.location.href="/";
                }else{
                    console.log(xhr.responseText);
                }
            }
        });
    });
    $(document).on('click', '#addhospitalprofile', function() {
        $('#addhospitalprofile').html('Saving.....');
        var formData = $("#AddHospitalProfile").serialize();
        // console.log(formData);
        $.ajax({
            type: "POST",
            url: "../api/auth/AddHospitalProfile",
            data: formData,
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', 'BEARER ' + sessionStorage.getItem('accessToken'));
            },
            success: function(response) {
                JSON.stringify(response); //to string
                jQuery('#levelnoti').show();
                document.getElementById("levelnoti").style.display = "inherit";
                jQuery('#levelnoti').append('<p>' + response.response_message + '</p>');
                $('#addhospitalprofile').html('Updated');
                setTimeout(function() {
                    window.location.reload()
                }, 2000);

            },
            error: function(xhr, status, error) {
                console.log(xhr.responseText);
            }
        });

    });

}
