var data = sessionStorage.getItem('accessToken');
if (!data == data) {
    window.location.href = "/";
} else {

    $(function() {
        //on keypress
        $('#confirm_password').keyup(function(e) {
            //get values
            var password = $('#password').val();
            var confirm_password = $(this).val();

            //check the strings
            if (password == confirm_password) {
                //if both are same remove the error and allow to submit
                $('.passworderror').text('Password are matching');
            } else {
                //if not matching show error and not allow to submit
                $('.passworderror').text('Password not matching');
            }
        });

        //jquery form submit
    });
    $(document).ready(function () {
        $.ajax({
            type: 'get',
            url: "../api/auth/ShowHospitalInfoAll",
            dataType: 'json',
            beforeSend : function( xhr ) {
                xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
            },
            success: function (response) {
                JSON.stringify(response); //to string
                $.each(response, function (index, value) {
                    $('.allhospital').append('' +
                        '<option value=' + this.id +'>'+ this.facility_name +'</option>');

                });

            }, error: function (xhr, status, error) {
                jQuery('#login_error').show();
                document.getElementById("login_error").style.display = "inherit";
                jQuery('#login_error').append('<p>'+error+'</p>');
            }
//
        });
    });
}
$(document).on('click', '#create_account', function() {

    var name = document.getElementById("name").value;
    var password = document.getElementById("password").value;
    var email = document.getElementById("email").value;
    var confirm_password = document.getElementById("confirm_password").value;

    if (email.trim() == '' || name.trim() == '' || password.trim() == ''  || confirm_password.trim() == '') {
        jQuery('#login_error').show();
        document.getElementById("login_error").style.display = "inherit";
        jQuery('#login_error').append('<p>Please Fill All Required Field</p>');
    } else {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: "../api/auth/SignUp",
            data: {
                'name': name,
                'password': password,
                'email': email,
            },
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', 'BEARER ' + data);
            },
            success: function(response) {
                JSON.stringify(response);
                jQuery('#login_error').show();
                document.getElementById("updating_error").style.display = "inherit";
                jQuery('#login_error').append('<p>You have successfully created account</p>');
                setTimeout(function() {
                    window.location.reload()
                }, 2000);

            },
            error: function(xhr, status, error) {
//                    console.log(xhr.responseText);
                jQuery('#login_error').show();
                document.getElementById("login_error").style.display = "inherit";
                jQuery('#login_error').append('<p>' + error + '</p>');
            }
        });
    }
});
$(document).ready(function() {
    sendRequest();

    function sendRequest() {
        $.ajax({
            type: 'get',
            url: "../api/auth/ShowSystemAccount",
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', 'BEARER ' + data);
            },
            success: function(response) {
                $(function(){
                    var current = location.pathname;
                    $('#sidebarmenu a').each(function(){
                        var $this = $(this);
                        // if the current path is like this link, make it active
                        if($this.attr('href').indexOf(current) !== -1){
                            $this.addClass('active');
                        }
                    })
                });
                $.ajax({
                    type: 'get',
                    url: "../api/auth/DashboardCount",
                    dataType: 'json',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'BEARER ' + data);
                    },
                    success: function (response) {
                        JSON.stringify(response); //to string

                        jQuery('#countusers').html('0'+response.countusers);
                        jQuery('#countagent').html('0'+response.countagent);
                        jQuery('#pendingQuotation').html('0'+response.pendingQuotation);
                        jQuery('#approvedQuotation').html('0'+response.approvedQuotation);
                        jQuery('#countstandard').html('0'+response.countstandard);

                    }, error: function (xhr, status, error) {
                        jQuery('#login_error').show();
                        document.getElementById("login_error").style.display = "inherit";
                        jQuery('#login_error').append('<p>' + error + '</p>');
                    }
                    //
                });
                $('.img-profile').initial();
                if (response.response_status == 400) {
                    $('#SystemInfo').append('');
                } else {
                    var $i = 1;
                    $.each(response, function (key, value) {
                        $("#SystemInfo").append("" +
                            "<tr><td class='text-center'>" + $i++ +"</td>" +
                            '<td>' + this.name + '</td>' +
                            '<td>' + this.email + '</td>' +
                            '<td>' + this.updated_at + '</td>' +
                            '<td><button type="button" class="btn btn-success btn-circle action_btn edit-modal" data-toggle="modal" data-id="' + this.id + '" data-target="#exampleModal"> <i class="fas fa-edit"></i> </button></td>' +
                            '<td><button type="button" class="btn btn-danger btn-circle action_btn delete" id="' + this.id + '"> <i class="fas fa-trash"></i> </button></td>' +
                            '</tr>');
                    });
                    /*
                    |--------------------------------------------
                    | Load Data Table
                    |--------------------------------------------
                    */
                    var table = $('#provincesTable').DataTable();
                }
            },
            error: function(xhr, status, error) {
                jQuery('#login_error').show();
                document.getElementById("login_error").style.display = "inherit";
                jQuery('#login_error').append('<p>' + error + '</p>');
            }
        });
    }
});
$(document).on('click', '.delete', function() {
    var id = $(this).attr("id");
    if (confirm("Are you sure you want to delete this records?")) {
        $.ajax({
            type: 'post',
            url: "../api/auth/DeleteAccount",
            dataType: 'json',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', 'BEARER ' + data);
            },
            data: {
                id: id
            },
            success: function(response) {
                JSON.stringify(response); //to string
                jQuery('#updating_error').show();
                document.getElementById("updating_error").style.display = "inherit";
                jQuery('#updating_error').append('<p>' + response.response_message + '</p>');
                setTimeout(function() {
                    window.location.reload()
                }, 2000);
            },
            error: function(xhr, status, error) {
//                    jQuery('#login_error').show();
//                    document.getElementById("login_error").style.display = "inherit";
//                    jQuery('#login_error').append('<p>' + response.response_message + '</p>');
                console.log(xhr.responseText);
            }
        });
    }
});
$(document).on('click', '.edit-modal', function() {
    var id = $(this).data('id');
    $.ajax({
        type: 'get',
        url: '../api/auth/UserInfo/' + id +'',
        dataType: 'json',
        beforeSend : function( xhr ) {
            xhr.setRequestHeader( 'Authorization', 'BEARER ' + data );
        },
        success: function (response) {
            JSON.stringify(response);
            console.log(response);

            $('.form-horizontal').show();
            $('#id_edit').val(response[0].id);
            $('#name_edit').val(response[0].name);
            $('#email_edit').val(response[0].email);

            $.each(response, function (index, value) {

                $('.User_role').append('' +
                    '<option value="'+ this.role +'" selected>'+ this.role +'</option>');

                $('#hospital_id').append('' +
                    '<option value="'+ this.allhospital +'">'+ this.facility_name +'</option>');
            });

        }, error: function (xhr, status, error) {
            jQuery('#login_error').show();
            document.getElementById("login_error").style.display = "inherit";
            jQuery('#login_error').append('<p>' + response.response_message + '</p>');
            console.log(xhr.responseText);
        }
    });
});
